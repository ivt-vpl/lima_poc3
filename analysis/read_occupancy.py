import sys, gzip, re
import pandas as pd
import numpy as np

# Path to events file as input parameters
input_path = sys.argv[1]
output_path = sys.argv[2]

PATTERN = r'time="([0-9.]+)" type="space availability" occupancyType="(parking|pickupDropoff)" availability="([0-9]+)" link="(.+)"'
data = []

previous_progress = 0

with gzip.open(input_path) as f:
    for line in f:
        if b"space availability" in line:
            match = re.search(PATTERN, line.decode("utf-8"))

            if match:
                time, occupancy_type, availability, link = match.group(1), match.group(2), match.group(3), match.group(4)
                time, availability = float(time), int(availability)

                data.append((time, link, occupancy_type, availability))

                progress = int(100.0 *  time / (30.0 * 3600))

                if progress > previous_progress:
                    print("Progress: %d%%" % progress)

                previous_progress = progress

df = pd.DataFrame.from_records(data, columns = ["time", "link", "type", "availability"])
df.to_csv(output_path, sep = ";", index = None)
