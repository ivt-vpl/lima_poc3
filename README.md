# LIMA PoC 3: Funktionalität

Im Folgenden wird beschrieben, wie im Rahmen des Projekts "LIMA PoC 3" MATSim und das `av` Paket
erweitert wurden, um räumliche Effekte automatisierter Fahrzeuge darzustellen. Zudem wurde die
"discrete mode choice" Erweiterung des IVT refactored, welche nun in einem eigenen, in sich 
geschlossenen Paket zur Verfügung steht. 

## Automatisierte Fahrzeuge: Platzbedarf und intermodale Reisen

Der Code zur Erweiterung automatisierter Fahrzeuge ist in diesem Repository enthalten. Er erfüllt zwei Aufgaben:

- Integration mit SwissRailRaptor um first/last-mile Reisen zu ermöglichen, bei denen automatisierte Fahrzeuge Kunden zu bestimmten ÖV-Haltestellen bringen oder sie von diesen abholen
- Platzbedarf automatisierter Fahrzeuge beim Parken und beim Auflesen/Absetzen von Kunden

Die bestehenden Funktionalitäten (Tür-zu-Tür Service) als Einzelfahrt (ohne pooling) ist weiterhin vorhanden. Funktionalitäten zur Simulation von car pooling sind in diesem Projekt nicht vorgesehen
### Einrichtung

Zur Einrichtung der AV-Funktionalität sind zwei Schritte notwendig. Zunächst muss die Konfiguration in MATSim eingebunden und angepasst werden, danach müssen verschiedene Module in MATSim integriert werden. Dies geschieht allerdings jeweils automatisch durch je einen Aufruf im Code.

#### Konfiguration

Prinzipiell kann die gesamte Funktionalität durch eine neuen `ConfigGroup` definiert werden. Diese kann auf den bekannten Wegen in MATSim integriert werden:

```java
Config config = ConfigUtils.loadConfig("...", new LimaAvConfigGroup());
/// oder
config.addModule(new LimaAvConfigGroup());
```

Die Einstellungen in dieser `ConfigGroup` können entweder in der Configdatei des Szenarios angepasst werden, oder direkt im Code. Die folgenden Optionen stehen zur Verfügung:

```xml
	<module name="lima_av" >
		<param name="feederLinksStopAttribute" value="avFeederLinkIds" />
		<param name="fleetSize" value="1000" />
		<param name="interactionLinkAttribute" value="isAvailableForAVInteraction" />
		<!-- Possible values: Nearest, CapacityAwareWithinRadius -->
		<param name="interactionPointFinder" value="CapacityAwareWithinRadius" />
		<!-- Possible values: Constant, Lima -->
		<param name="interactionTimeEstimator" value="Constant" />
		<!-- Possible values: Constant, Lima -->
		<param name="spatialCapacity" value="Constant" />
		<param name="useAv" value="true" />
		<parameterset type="InteractionPointFinder:CapacityAwareWithinRadius" >
			<param name="searchRadius" value="150.0" />
		</parameterset>
		<parameterset type="InteractionTime:Constant" >
			<param name="dropoffTime_s" value="300.0" />
			<param name="pickupTime_s" value="300.0" />
		</parameterset>
		<parameterset type="InteractionTime:Lima" >
			<param name="defaultWaitingTime_s" value="300.0" />
			<param name="estimationEndTime" value="22:00:00" />
			<param name="estimationInterval" value="00:05:00" />
			<param name="estimationStartTime" value="05:00:00" />
			<param name="horizon" value="10" />
			<param name="linkAttribute" value="avWaitingTimeGroup" />
		</parameterset>
		<parameterset type="SpatialCapacity:Constant" >
			<param name="parkingCapacity" value="1000" />
			<param name="pickupDropoffCapacity" value="1000" />
		</parameterset>
		<parameterset type="SpatialCapacity:Lima" >
			<param name="parkingCapacityPath" value="null" />
			<param name="pickupDropoffCapacityPath" value="null" />
		</parameterset>
		<parameterset type="scoring" >
			<param name="marginalUtilityOfDirectWaitingTime" value="0.0" />
			<param name="marginalUtilityOfFeederWaitingTime" value="0.0" />
			<param name="subpopulation" value="null" />
		</parameterset>
		<parameterset type="pricing" >
			<param name="useCostCalculator" value="false" />
			<param name="scenarioScalingFactor" value="0.1" />
			<param name="horizon" value="10" />
		</parameterset>
	</module>
```

Die Option `fleetSize` gibt die Zahl automatisieter Fahrzeuge an, die im System generiert werden. 

Mit der Option `interactionTimeEstimator` kann ausgewählt werden, die Interaktionen mit automatisiertern Fahrzeugen vorausberechnet werden. Dies betrifft hauptsächlich die Wartezeit auf ein Fahrzeug. Eine Variante ist `Constant`, welche noch genauer im `parameterset` weiter unten mit dem Namen "InteractionTime:Constant" konfiguriert werden kann. Hier wird lediglich eine fixe Zeit für das Aufsammeln und das Absetzen der Kunden angenommen. Diese Werte beeinflussen hauptsächlich das Routing, insbesondere im Fall von First/Last mile, da die Wartezeit ("pickup time") bestimmt, welche nächste Zugverbindung genutzt werden kann. Natürlich wird diese Zeit im finalen Szenario nicht immer gleich sein, sondern räumlich und zeitlich verteilt. Darum gibt es eine zweite Implementierung der `InteractionTime`, welche wiederum in der Option `interactionTimeEstimator` durch Angabe des Wertes `Lima` genutzt werden kann. Die genauere Konfiguration erfolgt im "InteractionTime:Lima" `parameterset`. Dort kann eingestellt werden, in wie vielen zeitlichen Abschnitten Abholzeiten gemessen und gemittelt werden. Sollten Wartezeiten vor oder nach den angegebenen Zeiten angefordert werden, so werden die jeweiligen Randzeiten genutzt. Räumlich müssen für diesen Schätzer alle Links, für die Wartezeiten gemessen werden mit einem Attribut versehen werden. Die Option `linkAttribute` bestimmt dabei, wie dieses Attribut heisst. Das Attribut wird als String angegeben und kann z.B. die Werte "group1", "group2", ... annahmen. Alle Links innerhalb einer Gruppe werden dabei zusammen gemittelt um Wartezeiten zu berechnen. Da Wartezeiten zwischen zwei Iterationen durchaus schwanken können, erfolgt zudem eine Mittelung über mehrere Iterationen. Wie viele dies sind, wird über den Parameter `horizon` definiert. Zusammenfassend ist also festzuhalten: Mit dem `Lima` Schätzer werden Wartezeiten in räumlichen Gruppen von Links und in spezifizierten Zeitbins gemittelt, und zwar über mehere Iterationen. All dies lässt sich über dieses `ConfigGroup` einstellen.

Der nächste Punkt ist der Platzbedarf. Dabei gibt es wiederum zwei Alternativen: `Constant` und `Lima`. In der einfachen `Constant` Variante lässt sich im `parameterset` "SpatialCapacity:Constant" einstellen, wie viel Platzkapazität *pro Link* vorhanden ist. Es kann einerseits die `parkingCapacity`, andererseits die `pickupDropoffCapacity` eingstellt werden. Die `Lima` Variante ist flexibler: Hier werden statt fixen Werten zwei Pfade angegeben. Diese sollten jeweils auf eine CSV-Datei zeigen mit dem folgenden Format:

```csv
link_id;3600;7200;10800;14400;...
link1;20;20;10;10;...
```

> Anm.: Vermutlich würde es Sinn machen, hier nicht einzelne Links aufzulisten, sondern Gruppen. Diese Gruppen würden dann wiederum als Attribut der Links definiert werden. Ich denke aber nicht, dass es grosse Unterschiede für die Performance macht. Falls doch, wäre dies hier ein erster Punkt zum ansetzen.

Die erste Zelle in der Datei ist prinzipiell ohne Bedeutung, alle folgenden Zellen in der ersten Zeile geben an, bis zu welchem Zeitpunkt (in Sekunden nach Mitternacht) ein bestimmter Kapazitätswert für einen Link Gültigkeit hat. Die Werte für die einzelnen Links werden dann Zeile für Zeile angegeben. Die erste Zelle gibt dabei immer die ID des Links an, während alle weiteren Zellen die Kapazitätswerte enthalten.

Ein weiterer wichtiger Punkt zur Konfiguration der automatisierten Fahrzeuge ist das "scoring" `parameterset`. Dieses kann mehrfach existieren, denn in ihm wird über das `subpopulation` Attribut definiert, welche Agenten betroffen sind. Für diese kann dann definiert werden wie im Scoring die Wartezeit auf ein automatisiertes Fahrzeug wahrgenommen wird, und zwar unterschieden danach, ob sie das automatiserte Fahrzeug als Feeder-Service für den ÖV nutzen, oder als Direktfahrt.

Die Parameter `feederLinksStopAttribute` und `interactionLinkAttribute` werden weiter unten erläutert. Sie bestimmen, welcher Attributname in den Haltestellen genutzt wird, um zu definieren, welche Links als Zugangs- und Abgangslinks dienen, sowie welcher Attributname für einzelne Links verwendet wird, um zu definieren, ob dort Interaktionen mit Kunden möglich sind. Zudem bestimmt `interactionPointFinder` wie Links zum Ein- und Aussteigen für eine Fahrt bestimmt werden.

Schlussendlich kann eingestellt werden, ob automatisierte Fahrzeuge überhaupt verwendet werden sollen. Dies geschieht über die Option `useAv`, welche entweder auf `true` oder `false` gestellt werden kann.

#### Einbindung der Module

Die Module werden eingebunden, sobald im Run Script der `Controler` erstellt wird:

```java
LimaPoC3Configurator.configure(controler);
```

Diese Funktion fügt diverse Module (z.B. `AVModule`, `DvrpModule`, ...) dem Controller hinzu. Dabei wird zudem überprüft, ob die Konfigurationsdatei konsistent ist. Wurde z.B. `useAv` eingeschaltet, wird diese Funktion überprüfen, ob der `av_direct` Modus tatsächlich in `SubtourModeChoice` eingebunden wurde. Ist dies nicht der Fall, wird eine Warnung produziert. Auf der anderen Seite, wurde z.B. `useAv` ausgeschaltet, so wird überprüft, ob dennoch `av_feeder` als ein Zugangsmodus zum ÖV im SwissRailRaptor definiert ist. Sollte dies der Fall sein, wird wiederum eine Warnung produziert. 

Die Idee dahinter ist, das das PoC3 Modul keine "magische Anpassung" der Konfiguration vornehmen soll. Die einzige direkte Konfiguration ist die spezifische `LimaConfigGroup` wie oben beschrieben. Alle weiteren notwendigen Anpassungen der Konfigurationsdatei werden nur *überprüft* und *gemeldet*, aber nicht selbst vorgenommen. In diesem Sinne kann die PoC 3 Erweiterung einfach wie in obiger Zeile eingebunden werden und mit jedem "Run" wird eine neue Warnung produziert, welche Konfiguration noch angepasst werden muss, bis alles konsistent ist. Um dies zu erleichtern, hier eine kurze Übersicht:

- Die `ConfigGroup` des SwissRailRaptor muss existieren.
- `av_feeder` und `av_direct` müssen `modeParams` in `planCalcScore` haben, die das Scoring der Reisen definieren
- `av interaction` muss einen `activityParams` Block in `planCalcScore` erhalten (normalerweise mit `scoringThisActivityAtAll == false`)
- Die `LimaConfigGroup` muss einen `scoring` Block für *jede* Subpopulation enthalten, die in `planCalcScore` definiert ist

- *Falls* intermodale Reisen erwünscht sind, muss `av_feeder` in der SwissRailRaptor Konfiguration als Zugangsmodus definiert sein
- *Falls* direkte Reisen erwünscht sind, muss `av_direct` im `subtourModeChoice` in `availableModes` hinzugefügt werden

Zusätzlich muss dafür gesorgt sein, dass wenn automatisierte Fahrzeuge aktiviert sind, diese tatsächlich genutzt werden können und nicht durch Platzbeschränkungen überall verboten sind. Dazu mehr im nächsten Abschnitt.

### Simulation

Im folgenden sollen die einzelnen Simulationskomponenten für automatisierte Fahrzeuge beschrieben werden. Zunäst wird dabei die Simulation der Platzbeschränkung erläutert, danach die Interaktion mit den Kunden.

#### Platzbeschränkungen

Automatisierte Fahrzeuge dürfen in PoC 3 nur auf den Netzwerk-Links fahren, auf denen der Verkehrsmodus `av` registriert ist, d.h.

```xml
<link ... modes="...,av" />
```

Kunden dürfen ausschliesslich auf solchen Links aufgelesen und abgesetzt werden, welche das Attribut `isAvailableForAVInteraction == true` besitzen, z.B.:

```xml
<link  ...>
   <attributes>
      <attribute name="isAvailableForAVInteraction" value="true" />
      <!-- ... -->
   </attributes>
</link>
```

Wird irgendwo ein Link definiert, der `isAvailableForAVInteraction == true` hat, aber nicht den Verkehrsmodus `av`, so wird ein Fehler ausgegeben. Das heisst, durch die reste Einstellung lässt sich angeben, wo Fahrzeug erlaubt sind (also z.B. nur auf HLS oder in speziell präparierten Gebieten) und mit dem zweiten Attribut lässt sich einstellen, wo Personen aufgesammelt werden können (also z.B. nicht auf Autobahnen oder Strassen mit baulicher Trennung bzw. ohne Zugangswege für Fussgänger).

Auf Links, auf denen Interaktionen durchgeführt werden können, gibt es nun eine Platzbeschränkung für "Pickup und Dropoff". Wie diese konfiguriert wird, wurde oben bereits beschrieben. Die Dynamik verläuft dann folgendermassen: Solange genüngend Platz zur Verfügung steht, kann ein Fahrzeug in die "virtuelle Haltebucht" einfahren. Dabei wird einfach pro Fahrzeug die vorhandene Kapazität um 1 Fahrzeug verringert. Sobald das Fahrzeug die Haltebuch wieder verlässt, wird die Kapazität zurückgewonnen. Will ein Fahrzeug jedoch einfahren, ohne dass Platz vorhanden ist, muss dieses vor der Bucht warten. Dies bedeutet explizit, dass es im Link verweilt und somit alle folgenden Fahrzeuge blockiert. An gefragten Haltepunkten kann also mit dieser Dynamik eine realistische Staubildung abgebildet werden. Sobald ein anderes Fahrzeug herausfährt, kann das neue Fahrzeug dann einfahren.

Ähnlich funktioniert die Dynamik für das Parken, allerdings wird schlichtweg in einer anderen "Haltebucht" gezählt. Das heisst, will ein Fahrzeug parken, so wird einfach ein anderer Zähler heruntergezählt, als für "Pickup und Dropoff". Wie weiss die Link-Dynamik allerdings, ob ein Fahrzeug parken oder einen Kunden aufsammeln will? Dies wird den jeweiligen Simulationskomponenten nun mitgeteilt, in dem jedes dynamische (automatisierte) Fahrzeug eine *intention* mit sich trägt, die gelesen wird, sobald es auf einen Link auffährt.

Dieses *intention* wird durch den Flottensteuerungsalgorithmus (*Dispatcher*) eingestellt. Der standardmässige heuristische Dispatcher des `av`-Pakets wurde daher folgendermassen angepasst:

- Ist ein Fahrzeug auf dem Weg einen Kunden aufzusammeln oder abzusetzen, wird seine *intention* auf "Pickup/Dropoff" gesetzt. Das heisst, wenn es im Link ankommt, wird es mit der entsprechenden Warteschlange interagieren.
- Wurde ein Kunde abgesetzt, würde der Dispatcher das Fahrzeug bisher einfach stehen lassen. In PoC 3 *muss* dieses nun weiterbewegt werden, und wenn es auch nur ein Parkplatz auf dem gleichen Link ist. Hierzu schaut der Dispatcher in der Umgebung des aktuellen Links nach, wo Parkplätze verfügbar sind und fährt zum nächsten freien Platz. Dabei wird seine *intention* auf "Parken" gesetzt. Das heisst dort angekommen wird es mit der "Parkplatzwarteschlange" interagieren. Dies geschieht allerdings erst nach dem nächsten Dispatching Aufruf, d.h. wenn das Fahrzeug direkt wieder für einen anderen Kunden benötigt wird, so geht es nicht auf die Fahrt zum Parkplatz, sondern direkt zum Kunden.

#### Interaktionen mit Reisenden

Wie bereits erläutert, sind Interaktionen mit Reisenden nur auf bestimmten Links möglich. Wie sieht dies allerdings auf deren Sicht aus? Wird eine Reise mit dem direkten Service (intern `av_direct`) geroutet, so gibt es zwei Varianten: Ist in der Konfiguraton `Nearest` als `interactionPointFinder` ausgewählt, so wird jeweils der  nächstmögliche Link gesucht, auf dem eine Interaktion mit einem AV möglich ist. Ist `CapacityAwareWithinRadius` ausgewählt, so werden alle Links in einem bestimmten Suchradius gefiltert und von diesen wird dann ein Link zufällig gewichtet mit der Platzkapazität zum Ein- und Aussteigen gewählt. Der Suchradius kann im `parameterset` `InterationPointFinder:CapacityAwareWithinRadius` im `searchRadius` Parameter eingestellt werden. Stimmen nach dieser Auswahl jeweils Start- und Pickup-Link bzw. Ziel- und Dropoff-Link der Fahrt nicht überein, werden im Routing zusätzliche Laufwege vor bzw. nach der Fahrt mit dem automatisierten Fahrzeug eingefügt. Diese werden durch "av interaction" Aktivitäten, die normalerweise nicht gescort werden sollten, verbunden. Ein Beispielplan sähe also so aus:

```xml
<activity link="linkStart" type="home" endTime="08:00:00" />
<leg mode="access_walk" />
<activity link="linkPickup" type="av interaction" />
<leg mode="av_direct" />
<activity link="linkDropoff" type="av interaction" />
<leg mode="egress_walk" />
<activity link="linkEnd" type="work" />
```

Das Routing wird komplexer für den Feeder (`av_feeder`) Modus. Bei diesem wird das AV-Routing intern vom SwissRailRaptor aufgerufen. Auf beiden Seiten einer Reise mit dem ÖV ist also eine Abfolge von `legs` und `activities` wie im obigen Beispiel möglich. Standardmässig sieht die Dynamik so aus, dass SwissRailRaptor den Link der Einstiegs- und Ausstiegsstationen angibt. Ist, für das Beispiel des Zugangsweges, also die Zugangsstation bereits auf einem Link, auf dem Interaktionen mit automatsierten Fahrzeugen möglich sind, so fährt das AV direkt diesen Link an. Ist dies allerdings nicht der Fall, so wird, wie oben beschrieben, der nächstmögliche Link für die Interaktion ausgewählt und der Agent erhält einen zusätzlichen Weg zu Fuss zwischen dem Absetzpunkt und der Haltestation. Analog funktioniert dies für den Abgangsweg.

Für Haltestellen können allerdings zusätzliche Links definiert werden, auf denen Interaktionen *für diese Haltstelle* möglich sind, obwohl dies sonst nicht erlaubt ist. Diese Links sind also exklusiv für Ein- und Aussteiger an der jeweiligen Haltestelle reserviert. Welche Links das sind, kann in der Schedule Datei von MATSim definiert werden. Dort kann jede `stopFacility` das Attribute `avFeederLinkIds` erhalten, welches, durch Kommata getrennt, die Links angibt, welche als Zugangs- und Abgangslinks dienen sollen:

```xml
<stopFacility ...>
   <attributes>
      <attribute name="avFeederLinkIds" class="java.lang.String">link1,link2,link3</attribute>
   </attributes>
</stopFacility>
```

Es ist wiederum zu beachten, dass diese Links auch den `av` *mode* im Netzwerk besitzen müssen, andernfalls wird direkt beim Laden der Daten ein Fehler produziert, der die entsprechenden fehlerhaften Einstellungen zeigt. 

Wie wird also bestimmt, welcher Zugangs- und Abgangslink für eine bestimmte ÖV-Reise genutzt wird. Hierfür ist es zunächst wichtig zu erwähnen, dass, sobald eine ÖV-Haltestelle dieses Attribut besitzt, *immer* einer dieser Links ausgewählt wird. Das heisst, dass diese Links nicht nur zusätzlich für diese Station genutzt werden *können*, sondern sogar *müssen*. Momentan wird dabei einer der Links zufällig ausgewählt, jedes mal, wenn eine ÖV-Reise geroutet wird. Die Idee dahinter ist, dass das MATSim Scoring ein Gleichgewicht der Pläne herstellen wird, sodass sich die Agenten abhängig von der Auslastung der Station und der einzelnen Zugangspunkte sinnvoll verteilen.

Die Bewertung jeglicher Wege mit automatisierten Fahrzeugen - sei es direkt oder als first/last mile Service - erfolgt also durch das Scoring in MATSim. Die Parameter kommen dabei aus der `planCalcScore` Konfiguration, so wie es standardmässig in MATSim der Fall ist. Dies betrifft hauptsächlich die Berechnung der distanzabhängigen (monetären) Kosten, sowie die generalisierten Kosten der Reisezeit. Leider enthalten diese Parameter standardmässig nur einen Beta-Wert für die Wartezeit für den ÖV. Aus diesem Grund definiert die `LimaConfigGroup` zusätzliche Faktoren, die die Bewertung der Wartezeiten für die automatisierten Angebote bestimmen. 

Für intermodale Reisen mit dem Feeder-Service kann weiterhin über die Funktionalität des SwissRailRaptor eingestellt werden, welche Haltestellen dafür zur Verfügung stehen. Beispielsweise lassen sich so nur grössere Haltestellen filtern. Dazu können die standardmässigen Filter-Attribute des SwissRailRaptor für Zu- und Abgangsmodi eingestellt werden. In jedem Fall ist in LIMA PoC3 der SwissRailRaptor so modifiziert, dass für die AV-Feeder Reisen nur unter den jeweils nächsten Stationen pro Linie gesucht wird. D.h. man könnte zwar mit dem Feeder mehrere Haltestellen auf einer Linie erreichen, jedoch wird nur die nächstgelegene als Tatsächliche Anlaufstelle in Betracht gezogen. Dies dient der Reduktion der Rechenzeit, da sonst ein Routing zu allen Stationen im in der SwissRailRaptor-Config definierten Suchradius vonnöten wäre.

Zusätzlich wurde allerdings nun definiert, dass mit dem `av_feeder` nur `TransitStop`s angefahren haben, die mindestens einen Link in den `avFeederLinkIds` haben.

## Flussdynamik: Differenzierte Bestimmung der Flusseffizienz

Für die Simulation automatisierter Fahrzeuge in PoC 3 ist eine Anforderung, dass dieses andere Flusseigenschaften aufweisen als konventionelle Fahrzeuge. Standardmässig kann die in MATSim einerseits durch eine Skalierung aller Kapazitäten im Netzwerk abgebildet werden ("makroskopischer Ansatz"), während auf der anderen Seite pro Fahrzeug eine *flow efficiency* definiert werden kann. Diese entspricht weitgehend dem Konzept der *PCU* (*passenger car unit*). Dabei werden für ein Standardfahrzeug Kosten im Sinne von Strassenkapazität definiert. Eine *PCU* von *2.0* bedeutet dann, dass ein anderes Fahreug doppelt so viel Kapazität "verbraucht" als das Standardfahrzeug. Im Falle automatisierter Fahrzeuge ist also davon auszugehen, dass die *PCU* kleiner ist als die eines standardmässigen, konventionellen Fahrzeugs. Es ist somit möglich, innerhalb des gleichen Zeitfensters, mehr der automatisierten Fahrzeuge durch einen Link fahren zu lassen, als konventionelle Fahrzeuge. Dieser *PCU*-Wert wird allerdings in MATSim standardmässig *pro Fahrzeug* bestimmt. Für LIMA Poc 3 ist eine weitere Differenzierung erforderlich: es soll zusätzlich eine Rolle spielen, auf welchem Strassentyp die Fahrzeuge unterwegs sind.

### Konfiguration

Ähnlich zu den Komponenten der Angebotsdynamik wie oben beschrieben, kann die `FlowEfficiency` über zwei Aufrufe definiert werden. Zunächst muss die entsprechende `ConfigGroup` hinzugefügt werden:

```java
Config config = ConfigUtils.loadConfig("...", new LimaFlowefficiencyConfigGroup());
/// oder
config.addModule(new LimaFlowefficiencyConfigGroup());
```

Die Einstellungsmöglichkeiten sehen wie folgt aus:

```xml
<module name="lima_flow_efficiency" >
	<param name="constantFlowEfficiency" value="1.5" />
	<param name="linkAttribute" value="null" />
	<param name="useFlowEfficiency" value="true" />
	<param name="automatedVehicleTypeIds" value="" />
</module>
```

Die Option `useFlowEfficiency` definiert, ob das Model überhaupt genutzt werden soll (oder ob alle Fahrzeuge die gleiche, standardmässige PCU aufweisen). Wir das Modul genutzt, so definiert `constantFlowEfficiency`, welcher Wert den automatisierten Fahrzeugen zugewiesen wird. Dabei ist beachten, dass hier immer von *flow efficiency* gesprochen wird, das das Inverse der *PCU* ist. Zusätzlich kann über `linkAttribute` definiert werden, dass die *flow efficiency* über ein bestimmtes Link-Attribut bestimmt wird. Die Bestimmung des Wertes erfolgt dann wie folgt:

- Wenn `linkAttribute == null`
  - Alle AVs haben die `constantFlowEfficiency` auf allen Links
- Wenn `linkAttribute != null`
  - Ist das Attribut für einen bestimmten Link gegeben, nutzen alle AVs auf diesem Link diesen Wert
  - Ist das Attribute nicht vorhanden, nutzen alle AVs auf diesem Link die `constantFlowEfficiency`
  
Die gleiche *flow efficiency* kann optional nicht nur für geteilte AVs (`av_feeder` und `av_direct`) genutzt werden, sondern auch für alle anderen `VehicleType`s, die per Parameter in `automatedVehicleTypeIds` definiert sind. Dazu müssen die jeweiligen IDs als komma-getrennte Liste angegeben werden.
  
### Einbindung des Moduls

Das Modul wird mit einem einfachen Aufruf auf dem `Controler` eingebunden. Dies ist der gleiche wie zuvor:

```java
LimaPoC3Configurator.configure(controler);
```

Analog wie für die Angebotsdynamik bestimmt die Option in der Konfigurationsdatei `useFlowEfficiency`, ob die Module hinzugefügt werden, oder nicht. 

## Individuelle Parameter für SwissRailRaptor

Im Laufe des Projekts wurde festgestellt, dass der SwissRailraptor für das Routing standardmässig *nicht* die individuellen Scoring-Parameter für die *Subpopulations* nutzt. Dies kann aktiviert werden, indem das folgende Model (nach dem SwissRailRaptor) im Controller registriert wird:

```java
controler.addOverridingModule(new IndividualSRRParametersModule());
```

Die entsprechende Funktionalität wurde darüber hinaus bereits in den SwissRailRaptor übernommen, allerdings nur in der Entwicklungsversion, nicht in den aktuellen stabilen Release des Pakets. Die Funktionalität ist aber deckungsgleich mit der, die hier für den aktuellen Status über das PoC 3 Modul eingebunden wird.

## Discrete Mode Choice

Im Laufe des Projekts wurde der Discrete Mode Choice Ansatz des IVT für MATSim paketiert und [auf github zur Verfügung gestellt](https://github.com/matsim-eth/discrete-mode-choice). Es ist dabei ein vielseitig verwendbares Framework entstanden, mit welchem unterschiedliche Ansätze in der Verkehrsmittelwahl verfolgt werden können.

Während Verkehrsmittelwahlmodelle direkt verwendet werden können (was aber vermutlich eine Rekalibrierung für LIMA erfordern würde), ist es auch möglich, die Erweiterung als "importance sampler" zu nutzen. Dabei erzeugt die Komponente die gleichen Pläne, die der Standardansatz "SubtourModeChoice" in MATSim erzeugen würde, allerdings werden Pläne, die anhand des Scorings der einzelnen Etappen vielversprechen erscheinen, bevorzugt erstellt. Somit werden dem MATSim Scoring nach wie vor zufällig ausgewählte Pläne angeboten, die dann verworfen oder optimiert werden. Allerdings sind die vorgeschlagenen Pläne hier nicht komplett zufällig, sondern basieren auf einer a priori Auswahl.

Im LIMA Projekt wurden bereits einige Voreinstellungen zur Nutzung von DMC getroffen. Zunächst muss wiederum die entsprechende `ConfigGroup` eingebunden werden:

```java
Config config = ConfigUtils.loadConfig("...", new LimaModeChoiceConfigGroup());
/// oder
config.addModule(new LimaModeChoiceConfigGroup());
```

Die Konfigurationsoptionen sind relativ simpel:

```xml
<module name="lima_mode_choice" >
	<param name="operatingAreaLinkAttribute" value="avOperatingArea" />
	<param name="useDiscreteModeChoice" value="true" />
	<param name="useImportanceSampling" value="true" />
</module>
```

Mit der Option `useDiscreteModeChoice` wird bestimmt, ob DMC überhaupt genutzt werden soll, oder ob einfach wie bisher `SubtourModeChoice` verwendet wird. DMC lässt sich dann in zwei Varianten nutzen: Entweder als eine direkte Replizierung von `SubtourModeChoice`, oder also *importance sampler*, wenn `useImportanceSampling == true`. In jedem Fall kann, sobald DMC aktiviert ist, gefiltert werden, welche Reisen tatsächlich mit einem automatisierten Fahrzeug durchgeführt werden können. Wird `operatingAreaLinkAttribute` gesetzt, so können Fahrten nur von solchen Links starten, die das entsprechende Attribut besitzen und es auf "true" gesetzt ist. Dies sollte den Auswahlprozess zusätzlich beschleunigen, da keine Fahrten in Regionen getätigt werden können, die gar nicht bedient werden. 

Der Controller wird wie gehabt über den `LimaPoC3Configurator` angepasst:

```java
LimaPoC3Configurator.configure(controler);
```

## Beispiel

Die Klasse `RunLimaPoC3Sandbox` im LIMA PoC 3 Repository bietet eine Übersicht über alle Sachen, die eingestellt werden müssen, um die bereitgestellte funktionalität zu nutzen. Dabei wird von einem standard MATSim Szenario ausgegangen und die einzelnen Anpassungen an die Konfiguration werden Stück für Stück gezeigt. Dies wurde ebenfalls mit dem Testszenario "Sioux Falls" erfolgreich getestet.






