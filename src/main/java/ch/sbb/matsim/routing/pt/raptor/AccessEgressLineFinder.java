package ch.sbb.matsim.routing.pt.raptor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

public class AccessEgressLineFinder {
	private final Logger logger = Logger.getLogger(AccessEgressLineFinder.class);
	private final Collection<QuadTree<TransitStopFacility>> indices = new LinkedList<>();

	public AccessEgressLineFinder(Network network, TransitSchedule schedule) {
		double[] dimensions = NetworkUtils.getBoundingBox(network.getNodes().values());

		for (TransitLine transitLine : schedule.getTransitLines().values()) {
			QuadTree<TransitStopFacility> lineIndex = new QuadTree<>(dimensions[0], dimensions[1], dimensions[2],
					dimensions[3]);

			for (TransitRoute transitRoute : transitLine.getRoutes().values()) {
				for (TransitRouteStop stop : transitRoute.getStops()) {
					TransitStopFacility stopFacility = stop.getStopFacility();
					String avFeederLinks = (String) stopFacility.getAttributes().getAttribute("avFeederLinkIds");

					if (avFeederLinks != null && avFeederLinks.trim().length() > 0) {
						lineIndex.put(stopFacility.getCoord().getX(), stopFacility.getCoord().getY(), stopFacility);
					}
				}
			}

			logger.info(String.format("Found %d stops for line %s", lineIndex.size(), transitLine.getId().toString()));

			if (lineIndex.size() > 0) {
				indices.add(lineIndex);
			}
		}
	}

	public Collection<TransitStopFacility> findStops(Coord location) {
		Collection<TransitStopFacility> result = new ArrayList<>(indices.size());

		for (QuadTree<TransitStopFacility> index : indices) {
			result.add(index.getClosest(location.getX(), location.getY()));
		}

		return result;
	}
}
