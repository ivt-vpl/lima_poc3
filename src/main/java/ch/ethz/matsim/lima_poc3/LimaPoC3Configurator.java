package ch.ethz.matsim.lima_poc3;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;

import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.discrete_mode_choice.modules.ConstraintModule;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceConfigurator;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceModule;
import ch.ethz.matsim.discrete_mode_choice.modules.EstimatorModule;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.discrete_mode_choice.modules.config.LinkAttributeConstraintConfigGroup;
import ch.ethz.matsim.lima_poc3.av.IntermodalAVModule;
import ch.ethz.matsim.lima_poc3.av.LimaAvModule;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.pricing.AVPricingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.dmc.LimaDiscreteModeChoiceModule;
import ch.ethz.matsim.lima_poc3.dmc.LimaModeChoiceConfigGroup;
import ch.ethz.matsim.lima_poc3.flow_efficiency.LimaFlowEfficiencyModule;
import ch.ethz.matsim.lima_poc3.flow_efficiency.config.LimaFlowEfficiencyConfigGroup;
import ch.ethz.matsim.lima_poc3.srr.LimaSRRParametersModule;
import ch.sbb.matsim.routing.pt.raptor.AccessEgressLineFinder;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptor;

public class LimaPoC3Configurator {
	public static void configure(Controler controler) {
		configure(controler, true);
	}

	public static void configure(Controler controler, boolean performChecks) {
		Config config = controler.getConfig();
		LimaAvConfigGroup avConfig = (LimaAvConfigGroup) config.getModules().get(LimaAvConfigGroup.GROUP_NAME);

		if (avConfig != null && avConfig.isUseAv()) {
			if (performChecks) {
				LimaPoC3Checker.checkAv(config);
			}

			controler.addOverridingModule(new AbstractModule() {
				@Override
				public void install() {
					AVConfigGroup avConfigGroup = new AVConfigGroup();
					avConfigGroup.setParallelRouters(getConfig().global().getNumberOfThreads());
					bind(AVConfigGroup.class).toInstance(avConfigGroup);
				}
			});

			controler.addOverridingModule(new DvrpTravelTimeModule());
			controler.addOverridingModule(new AVModule());
			controler.addOverridingModule(new IntermodalAVModule());
			controler.addOverridingModule(new LimaAvModule());
			controler.addOverridingModule(new AVPricingModule(avConfig.getPricingConfig()));
			controler.addOverridingModule(new LimaSRRParametersModule());

			controler.addOverridingModule(new LimaPoC3QSimModule(true, true));
		} else if (performChecks) {
			LimaPoC3Checker.checkNoAv(config);
		}

		LimaFlowEfficiencyConfigGroup flowEfficiencyConfig = (LimaFlowEfficiencyConfigGroup) config.getModules()
				.get(LimaFlowEfficiencyConfigGroup.GROUP_NAME);

		if (flowEfficiencyConfig != null && flowEfficiencyConfig.getUseFlowEfficiency()) {
			if (performChecks) {
				LimaPoC3Checker.checkFlowEfficiency(config);
			}

			controler.addOverridingModule(new LimaFlowEfficiencyModule());
		}

		LimaModeChoiceConfigGroup modeChoiceConfig = (LimaModeChoiceConfigGroup) config.getModules()
				.get(LimaModeChoiceConfigGroup.GROUP_NAME);

		if (modeChoiceConfig != null && modeChoiceConfig.getUseDiscreteModeChoice()) {
			if (modeChoiceConfig.getUseImportanceSampling()) {
				DiscreteModeChoiceConfigurator.configureAsImportanceSampler(config);

				DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
						.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);
				dmcConfig.setTourEstimator(EstimatorModule.CUMULATIVE);
				dmcConfig.setTripEstimator(LimaDiscreteModeChoiceModule.ESTIMATOR_NAME);
			} else {
				DiscreteModeChoiceConfigurator.configureAsSubtourModeChoiceReplacement(config);
			}

			DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
					.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);

			if (modeChoiceConfig.getOperatingAreaLinkAttribute() != null) {
				LinkAttributeConstraintConfigGroup linkAttributeConfig = dmcConfig
						.getLinkAttributeConstraintConfigGroup();
				linkAttributeConfig.setAttributeName(modeChoiceConfig.getOperatingAreaLinkAttribute());
				linkAttributeConfig.setAttributeValue("true");
				linkAttributeConfig.setConstrainedModes(Arrays.asList(AVDirectRoutingModule.AV_DIRECT_MODE));

				Collection<String> tripConstraints = new HashSet<>();
				tripConstraints.add(ConstraintModule.LINK_ATTRIBUTE);
				dmcConfig.setTripConstraints(tripConstraints);

				Collection<String> tourConstraints = new HashSet<>(dmcConfig.getTourConstraints());
				tourConstraints.add(ConstraintModule.FROM_TRIP_BASED);
				dmcConfig.setTourConstraints(tourConstraints);
			}

			Set<String> cachedModes = new HashSet<>(dmcConfig.getCachedModes());
			cachedModes.add(AVDirectRoutingModule.AV_DIRECT_MODE);
			dmcConfig.setCachedModes(cachedModes);

			if (modeChoiceConfig.getFixRide()) {
				Collection<String> tourConstraints = new HashSet<>(dmcConfig.getTourConstraints());
				tourConstraints.add(ConstraintModule.FROM_TRIP_BASED);
				dmcConfig.setTourConstraints(tourConstraints);

				Collection<String> tripConstraints = Arrays.asList(LimaDiscreteModeChoiceModule.FIX_RIDE_CONSTRAINT);
				dmcConfig.setTripConstraints(tripConstraints);
			}

			controler.addOverridingModule(new DiscreteModeChoiceModule());
			controler.addOverridingModule(new LimaDiscreteModeChoiceModule());
		}

		if (avConfig != null && avConfig.getFilterFeederStationsByLine()) {
			SwissRailRaptor.accessEgressLineFinder = new AccessEgressLineFinder(controler.getScenario().getNetwork(),
					controler.getScenario().getTransitSchedule());
		}
	}

}
