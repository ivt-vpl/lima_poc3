package ch.ethz.matsim.lima_poc3.srr;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.matsim.api.core.v01.population.Person;
import org.matsim.core.config.Config;
import org.matsim.core.scoring.functions.ScoringParametersForPerson;

import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.ethz.matsim.lima_poc3.av.scoring.LIMAScoringParameterForPerson;
import ch.ethz.matsim.lima_poc3.av.scoring.LIMAScoringParameters;
import ch.sbb.matsim.routing.pt.raptor.RaptorParameters;

@Singleton
public class LimaRaptorParametersForPerson extends IndividualRaptorParametersForPerson {
	private final LIMAScoringParameterForPerson scoringParameters;
	private final ScoringParametersForPerson parametersForPerson;

	@Inject
	public LimaRaptorParametersForPerson(Config config, ScoringParametersForPerson parametersForPerson,
			LIMAScoringParameterForPerson scoringParameters) {
		super(config, parametersForPerson);
		this.scoringParameters = scoringParameters;
		this.parametersForPerson = parametersForPerson;
	}

	@Override
	public RaptorParameters getRaptorParameters(Person person) {
		RaptorParameters delegate = super.getRaptorParameters(person);
		LimaRaptorParameters limaParameters = new LimaRaptorParameters(delegate);
		LIMAScoringParameters limaScoringParameters = scoringParameters.getScoringParameters(person);

		double marginalUtilityOfFeederWaitingTime_s = limaScoringParameters.marginalUtilityOfFeederWaitingTime_s;
		limaParameters.setMarginalUtilityOfFeederWaitingTime_s(marginalUtilityOfFeederWaitingTime_s);

		double monetaryDistanceCostRateFeeder = parametersForPerson.getScoringParameters(person).modeParams
				.get(AVFeederRoutingModule.AV_FEEDER_MODE).monetaryDistanceCostRate;
		limaParameters.setMonetaryDistanceCostRateFeeder(monetaryDistanceCostRateFeeder);

		for (Map.Entry<String, Double> entry : limaScoringParameters.routingTransferUtilities.entrySet()) {
			limaParameters.setTransferUtility_utl(entry.getKey(), entry.getValue());
		}

		limaParameters.setDirectPriceFactor(limaScoringParameters.directPriceFactor);
		limaParameters.setFeederPriceFactor(limaScoringParameters.feederPriceFactor);

		double marginalUtilityOfDistanceFeeder = parametersForPerson.getScoringParameters(person).modeParams
				.get(AVFeederRoutingModule.AV_FEEDER_MODE).marginalUtilityOfDistance_m;
		limaParameters.setMarginalUtilityOfDistanceFeeder(marginalUtilityOfDistanceFeeder);

		double marginalUtilityOfDistanceDirect = parametersForPerson.getScoringParameters(person).modeParams
				.get(AVDirectRoutingModule.AV_DIRECT_MODE).marginalUtilityOfDistance_m;
		limaParameters.setMarginalUtilityOfDistanceDirect(marginalUtilityOfDistanceDirect);

		return limaParameters;
	}
}
