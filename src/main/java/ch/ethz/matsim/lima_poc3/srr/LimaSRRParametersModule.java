package ch.ethz.matsim.lima_poc3.srr;

import org.matsim.core.controler.AbstractModule;

import ch.sbb.matsim.routing.pt.raptor.RaptorParametersForPerson;

public class LimaSRRParametersModule extends AbstractModule {
	@Override
	public void install() {
		bind(RaptorParametersForPerson.class).to(LimaRaptorParametersForPerson.class);
	}
}
