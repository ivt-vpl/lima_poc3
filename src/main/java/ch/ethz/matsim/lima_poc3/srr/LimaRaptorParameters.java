package ch.ethz.matsim.lima_poc3.srr;

import java.util.HashMap;
import java.util.Map;

import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;
import ch.sbb.matsim.routing.pt.raptor.RaptorParameters;

public class LimaRaptorParameters extends RaptorParameters {
	public LimaRaptorParameters(RaptorParameters delegate) {
		super(delegate.getConfig());
		this.delegate = delegate;
	}

	private final RaptorParameters delegate;

	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	public double getBeelineWalkSpeed() {
		return delegate.getBeelineWalkSpeed();
	}

	public SwissRailRaptorConfigGroup getConfig() {
		return delegate.getConfig();
	}

	public double getExtensionRadius() {
		return delegate.getExtensionRadius();
	}

	public double getMarginalUtilityOfTravelTime_utl_s(String mode) {
		return delegate.getMarginalUtilityOfTravelTime_utl_s(mode);
	}

	public double getMarginalUtilityOfWaitingPt_utl_s() {
		return delegate.getMarginalUtilityOfWaitingPt_utl_s();
	}

	public double getSearchRadius() {
		return delegate.getSearchRadius();
	}

	public double getTransferPenaltyFixCostPerTransfer() {
		return delegate.getTransferPenaltyFixCostPerTransfer();
	}

	public double getTransferPenaltyTravelTimeToCostFactor() {
		return delegate.getTransferPenaltyTravelTimeToCostFactor();
	}

	public int hashCode() {
		return delegate.hashCode();
	}

	public void setBeelineWalkSpeed(double beelineWalkSpeed) {
		delegate.setBeelineWalkSpeed(beelineWalkSpeed);
	}

	public void setExtensionRadius(double extensionRadius) {
		delegate.setExtensionRadius(extensionRadius);
	}

	public void setMarginalUtilityOfTravelTime_utl_s(String mode, double marginalUtilityOfTravelTime_utl_s) {
		delegate.setMarginalUtilityOfTravelTime_utl_s(mode, marginalUtilityOfTravelTime_utl_s);
	}

	public void setMarginalUtilityOfWaitingPt_utl_s(double marginalUtilityOfWaitingPt_utl_s) {
		delegate.setMarginalUtilityOfWaitingPt_utl_s(marginalUtilityOfWaitingPt_utl_s);
	}

	public void setSearchRadius(double searchRadius) {
		delegate.setSearchRadius(searchRadius);
	}

	public void setTransferPenaltyFixCostPerTransfer(double transferPenaltyFixCostPerTransfer) {
		delegate.setTransferPenaltyFixCostPerTransfer(transferPenaltyFixCostPerTransfer);
	}

	public void setTransferPenaltyTravelTimeToCostFactor(double transferPenaltyTravelTimeToCostFactor) {
		delegate.setTransferPenaltyTravelTimeToCostFactor(transferPenaltyTravelTimeToCostFactor);
	}

	public String toString() {
		return delegate.toString();
	}

	private double marginalUtilityOfFeederWaitingTime_s = 0.0;

	public double getMarginalUtilityOfFeederWaitingTime_s() {
		return marginalUtilityOfFeederWaitingTime_s;
	}

	public void setMarginalUtilityOfFeederWaitingTime_s(double marginalUtilityOfFeederWaitingTime_s) {
		this.marginalUtilityOfFeederWaitingTime_s = marginalUtilityOfFeederWaitingTime_s;
	}

	private double marginalUtilityOfMoney = 0.0;

	public double getMarginalUtilityOfMoney() {
		return marginalUtilityOfMoney;
	}

	public void setMarginalUtilityOfMoney(double marginalUtilityOfMoney) {
		this.marginalUtilityOfMoney = marginalUtilityOfMoney;
	}

	private double monetaryDistanceCostRateFeeder = 0.0;

	public double getMonetaryDistanceCostRateFeeder() {
		return monetaryDistanceCostRateFeeder;
	}

	public void setMonetaryDistanceCostRateFeeder(double monetaryDistanceCostRateFeeder) {
		this.monetaryDistanceCostRateFeeder = monetaryDistanceCostRateFeeder;
	}

	private Map<String, Double> transferPenalties = new HashMap<>();

	public double getTransferUtility_utl(String mode) {
		return transferPenalties.getOrDefault(mode, 0.0);
	}

	public void setTransferUtility_utl(String mode, double utility) {
		transferPenalties.put(mode, utility);
	}

	private double directPriceFactor;
	private double feederPriceFactor;

	public double getDirectPriceFactor() {
		return directPriceFactor;
	}

	public void setDirectPriceFactor(double directPriceFactor) {
		this.directPriceFactor = directPriceFactor;
	}

	public double getFeederPriceFactor() {
		return feederPriceFactor;
	}

	public void setFeederPriceFactor(double feederPriceFactor) {
		this.feederPriceFactor = feederPriceFactor;
	}

	private double marginalUtilityOfDistanceFeeder = 0.0;

	public double getMarginalUtilityOfDistanceFeeder() {
		return marginalUtilityOfDistanceFeeder;
	}

	public void setMarginalUtilityOfDistanceFeeder(double marginalUtilityOfDistanceFeeder) {
		this.marginalUtilityOfDistanceFeeder = marginalUtilityOfDistanceFeeder;
	}

	private double marginalUtilityOfDistanceDirect = 0.0;

	public double getMarginalUtilityOfDistanceDirect() {
		return marginalUtilityOfDistanceDirect;
	}

	public void setMarginalUtilityOfDistanceDirect(double marginalUtilityOfDistanceDirect) {
		this.marginalUtilityOfDistanceDirect = marginalUtilityOfDistanceDirect;
	}
}
