package ch.ethz.matsim.lima_poc3.misc.convergence.variables;

import java.util.Map;

public interface CategoricalVariable extends ConvergenceVariable {
	Map<String, Double> getValues();
}
