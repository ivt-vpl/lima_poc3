package ch.ethz.matsim.lima_poc3.misc.sandbox.analysis;

import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.api.core.v01.events.PersonLeavesVehicleEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.PersonEntersVehicleEventHandler;
import org.matsim.api.core.v01.events.handler.PersonLeavesVehicleEventHandler;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.vehicles.Vehicle;

import ch.ethz.matsim.lima_poc3.misc.convergence.variables.CategoricalVariable;

public class AVDistanceVariable implements CategoricalVariable, LinkEnterEventHandler, PersonEntersVehicleEventHandler,
		PersonLeavesVehicleEventHandler {
	private final Network network;

	private final Map<Id<Vehicle>, Id<Person>> passengers = new HashMap<>();

	private double passengerDistance = 0.0;
	private double emptyDistance = 0.0;
	private int lastIteration = -1;

	private final Map<String, Double> state = new HashMap<>();

	public AVDistanceVariable(Network network) {
		this.network = network;

		this.state.put("passenger", 0.0);
		this.state.put("empty", 0.0);
	}

	@Override
	public void update(int iteration) {
		if (iteration > lastIteration) {
			state.put("passenger", passengerDistance);
			state.put("empty", emptyDistance);

			passengerDistance = 0.0;
			emptyDistance = 0.0;
			passengers.clear();

			lastIteration = iteration;
		}
	}

	@Override
	public String getName() {
		return "av_distance";
	}

	@Override
	public Map<String, Double> getValues() {
		return state;
	}

	@Override
	public void handleEvent(PersonEntersVehicleEvent event) {
		if (event.getVehicleId().toString().startsWith("av_")) {
			if (!event.getPersonId().toString().startsWith("av_")) {
				passengers.put(event.getVehicleId(), event.getPersonId());
			}
		}
	}

	@Override
	public void handleEvent(PersonLeavesVehicleEvent event) {
		if (event.getVehicleId().toString().startsWith("av_")) {
			passengers.remove(event.getVehicleId());
		}
	}

	@Override
	public void handleEvent(LinkEnterEvent event) {
		if (event.getVehicleId().toString().startsWith("av_")) {
			if (passengers.containsKey(event.getVehicleId())) {
				passengerDistance += network.getLinks().get(event.getLinkId()).getLength();
			} else {
				emptyDistance += network.getLinks().get(event.getLinkId()).getLength();
			}
		}
	}
}
