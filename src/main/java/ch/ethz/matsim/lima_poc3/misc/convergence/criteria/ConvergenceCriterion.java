package ch.ethz.matsim.lima_poc3.misc.convergence.criteria;

import java.util.Map;

public interface ConvergenceCriterion {
	void update(int iteration);

	boolean isConverged();

	Map<String, Double> getValues();
	
	String getName();
}
