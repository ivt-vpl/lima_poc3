package ch.ethz.matsim.lima_poc3.misc.sandbox;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ActivityParams;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import com.google.inject.Key;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVUtils;
import ch.ethz.matsim.lima_poc3.av.IntermodalAVModule;
import ch.ethz.matsim.lima_poc3.av.components.SpatiallyConstrainedPopulationDensityGenerator;
import ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking.ParkingSingleHeuristicDispatcher;
import ch.ethz.matsim.lima_poc3.av.components.dispatcher.unblocking.UnblockingSingleHeuristicDispatcher;
import ch.ethz.matsim.lima_poc3.av.config.ConstantInteractionTimeParameterSet;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.config.LimaInteractionTimeParameterSet;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.NearestInteractionLinkData;
import ch.ethz.matsim.lima_poc3.av.space.ConstantSpatialCapacity;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;
import ch.ethz.matsim.lima_poc3.av.space.VehicleInteractionHandler;
import ch.ethz.matsim.lima_poc3.srr.LimaSRRParametersModule;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup.IntermodalAccessEgressParameterSet;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorModule;

public class SandboxUtils {
	static public void updateScenarioWithAVs(Scenario scenario) {
		DvrpConfigGroup dvrpConfig = new DvrpConfigGroup();
		dvrpConfig.setNetworkMode("av");

		scenario.getConfig().addModule(dvrpConfig);
		scenario.getConfig().addModule(new AVConfigGroup());
		scenario.getConfig().addModule(new LimaAvConfigGroup());
		scenario.getConfig().addModule(new ConstantInteractionTimeParameterSet());
		scenario.getConfig().addModule(new LimaInteractionTimeParameterSet());

		for (Link link : scenario.getNetwork().getLinks().values()) {
			if (link.getAllowedModes().contains("car")) {
				Set<String> modes = new HashSet<>(link.getAllowedModes());
				modes.add("av");
				link.setAllowedModes(modes);
			}
		}

		ModeParams modeParams = scenario.getConfig().planCalcScore().getOrCreateModeParams("av");
		modeParams.setMarginalUtilityOfTraveling(-2.5);
		modeParams.setConstant(-0.005);
		modeParams.setMarginalUtilityOfDistance(0.0);
		modeParams.setMonetaryDistanceRate(0.0);

		scenario.getConfig().subtourModeChoice().setModes(new String[] { "car", "pt", "walk", "av" });
	}

	static public void updateScenarioWithIntermodalAVs(Scenario scenario) {
		SwissRailRaptorConfigGroup raptorConfig = new SwissRailRaptorConfigGroup();
		scenario.getConfig().addModule(raptorConfig);

		IntermodalAccessEgressParameterSet intermodalParams = new IntermodalAccessEgressParameterSet();
		intermodalParams.setMode(TransportMode.walk);
		intermodalParams.setRadius(1000.0);
		raptorConfig.addIntermodalAccessEgress(intermodalParams);

		intermodalParams = new IntermodalAccessEgressParameterSet();
		intermodalParams.setMode(AVFeederRoutingModule.AV_FEEDER_MODE);
		intermodalParams.setRadius(5000);
		raptorConfig.addIntermodalAccessEgress(intermodalParams);

		raptorConfig.setUseIntermodalAccessEgress(true);

		for (Person person : scenario.getPopulation().getPersons().values()) {
			for (Plan plan : person.getPlans()) {
				for (PlanElement element : plan.getPlanElements()) {
					if (element instanceof Leg) {
						Leg leg = (Leg) element;

						if (leg.getMode().equals(AVModule.AV_MODE)) {
							leg.setMode(AVDirectRoutingModule.AV_DIRECT_MODE);
						}
					}
				}
			}
		}

		Config config = scenario.getConfig();

		// We need this, otherwise AVScoringFunction will complain
		ModeParams modeParams = config.planCalcScore().getOrCreateModeParams(AVModule.AV_MODE);

		modeParams = config.planCalcScore().getOrCreateModeParams(AVDirectRoutingModule.AV_DIRECT_MODE);
		modeParams.setConstant(-0.005);
		modeParams.setMarginalUtilityOfTraveling(-2.5);
		modeParams.setMarginalUtilityOfDistance(0.0);
		modeParams.setMonetaryDistanceRate(0.0);

		modeParams = config.planCalcScore().getOrCreateModeParams(AVFeederRoutingModule.AV_FEEDER_MODE);
		modeParams.setConstant(0.0);
		modeParams.setMarginalUtilityOfTraveling(-2.5);
		modeParams.setMarginalUtilityOfDistance(0.0);
		modeParams.setMonetaryDistanceRate(0.0);

		ActivityParams activityParams = new ActivityParams("av interaction");
		activityParams.setScoringThisActivityAtAll(false);
		config.planCalcScore().addActivityParams(activityParams);

		for (String mode : Arrays.asList(TransportMode.access_walk, TransportMode.egress_walk,
				TransportMode.transit_walk)) {
			modeParams = config.planCalcScore().getOrCreateModeParams(mode);
			modeParams.setConstant(0.0);
			modeParams.setMarginalUtilityOfTraveling(
					config.planCalcScore().getModes().get(TransportMode.walk).getMarginalUtilityOfTraveling());
			modeParams.setMarginalUtilityOfDistance(0.0);
			modeParams.setMonetaryDistanceRate(0.0);
		}

		config.planCalcScore().setMarginalUtlOfWaitingPt_utils_hr(0.0);

		config.subtourModeChoice().setModes(new String[] { "car", "pt", "walk", AVDirectRoutingModule.AV_DIRECT_MODE });

		for (TransitStopFacility facility : scenario.getTransitSchedule().getFacilities().values()) {
			facility.getAttributes().putAttribute("avFeederLinkIds", facility.getLinkId().toString());
		}
	}

	static public void updateControllerWithAVs(Controler controler, boolean useParkingDispatcher) {
		controler.addOverridingModule(new DvrpTravelTimeModule());
		controler.addOverridingModule(new AVModule());
		VehicleInteractionHandler.INSTANCE.enableLogger();

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				bind(NearestInteractionLinkData.class).asEagerSingleton();

				AVUtils.registerGeneratorFactory(binder(), "SpatiallyConstrainedPopulationDensity",
						SpatiallyConstrainedPopulationDensityGenerator.Factory.class);

				AVUtils.registerDispatcherFactory(binder(), "UnblockingSingleHeuristicDispatcher",
						UnblockingSingleHeuristicDispatcher.Factory.class);

				AVUtils.registerDispatcherFactory(binder(), "ParkingDispatcher",
						ParkingSingleHeuristicDispatcher.Factory.class);

				bind(Key.get(SpatialCapacity.class, Names.named("parking")))
						.toInstance(new ConstantSpatialCapacity(1000L));
				bind(Key.get(SpatialCapacity.class, Names.named("pickupDropoff")))
						.toInstance(new ConstantSpatialCapacity(1000L));

				AVConfig config = new AVConfig();

				String dispatcher = useParkingDispatcher ? "ParkingDispatcher" : "SingleHeuristic";

				AVOperatorConfig operator = config.createOperatorConfig(AVModule.AV_MODE);
				operator.createDispatcherConfig(dispatcher);
				operator.createGeneratorConfig("SpatiallyConstrainedPopulationDensity").setNumberOfVehicles(100);
				operator.createPriceStructureConfig();

				bind(AVConfig.class).toInstance(config);
				bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
						.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();
			}

		});
	}

	static public void updateControllerWithRaptor(Controler controler, boolean individualParameters) {
		controler.addOverridingModule(new SwissRailRaptorModule());

		if (individualParameters) {
			controler.addOverridingModule(new LimaSRRParametersModule());
		}
	}

	static public void updateControllerWithIntermodalAVs(Controler controler) {
		controler.addOverridingModule(new IntermodalAVModule());
	}
}
