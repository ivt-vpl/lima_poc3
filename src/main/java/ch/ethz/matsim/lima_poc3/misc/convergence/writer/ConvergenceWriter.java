package ch.ethz.matsim.lima_poc3.misc.convergence.writer;

import java.io.IOException;

public interface ConvergenceWriter {
	void write(int iteration) throws IOException;
}
