package ch.ethz.matsim.lima_poc3.misc.sandbox;

import java.io.IOException;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;

import com.google.inject.Key;
import com.google.inject.name.Names;

import ch.ethz.idsc.amodeus.matsim.mod.AmodeusDispatcherModule;
import ch.ethz.idsc.amodeus.matsim.mod.AmodeusModule;
import ch.ethz.idsc.amodeus.matsim.mod.AmodeusVehicleGeneratorModule;
import ch.ethz.idsc.amodeus.matsim.mod.AmodeusVehicleToVSGeneratorModule;
import ch.ethz.idsc.amodeus.net.DatabaseModule;
import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.lima_poc3.LimaPoC3QSimModule;
import ch.ethz.matsim.lima_poc3.av.amodeus.LIMAAmodeusModule;

public class RunSandboxWithAmodeus {
	static public void main(String[] args) throws IOException {
		SandboxFactory factory = new SandboxFactory();

		// COMMENT OUT FOR A CLEAN START
		factory.setPopulation("output_raptor/output_plans.xml.gz");

		Scenario scenario = factory.create();

		scenario.getConfig().controler().setOutputDirectory("output_with_amodeus");

		DvrpConfigGroup dvrpConfig = new DvrpConfigGroup();
		dvrpConfig.setNetworkMode("car");

		Config config = scenario.getConfig();
		config.addModule(dvrpConfig);
		config.addModule(new AVConfigGroup());

		config.controler().setLastIteration(200);

		ModeParams modeParams = config.planCalcScore().getOrCreateModeParams("av");
		modeParams.setMarginalUtilityOfTraveling(-2.5);
		modeParams.setConstant(-0.005);
		modeParams.setMarginalUtilityOfDistance(0.0);
		modeParams.setMonetaryDistanceRate(0.0);

		config.subtourModeChoice().setModes(new String[] { "car", "pt", "walk", "av" });

		Controler controler = new Controler(scenario);
		controler.addOverridingModule(new DvrpTravelTimeModule());

		controler.addOverridingModule(new AVModule());
		controler.addOverridingModule(new AmodeusModule());
		controler.addOverridingModule(new AmodeusDispatcherModule());
		controler.addOverridingModule(new AmodeusVehicleGeneratorModule());
		controler.addOverridingModule(new AmodeusVehicleToVSGeneratorModule());
		controler.addOverridingModule(new DatabaseModule());

		int numberOfVehicles = 100;
		int numberOfVirtualNodes = 4;
		int travelDataInterval = 300;

		controler.addOverridingModule(new LimaPoC3QSimModule(true, false));
		controler.addOverridingModule(new LIMAAmodeusModule( //
				numberOfVirtualNodes, numberOfVehicles, travelDataInterval));

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				AVConfig config = new AVConfig();

				AVOperatorConfig operator = config.createOperatorConfig("av");
				operator.createDispatcherConfig("AdaptiveRealTimeRebalancingPolicy");
				operator.createGeneratorConfig("PopulationDensity").setNumberOfVehicles(numberOfVehicles);
				operator.createPriceStructureConfig();

				bind(AVConfig.class).toInstance(config);
				bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
							.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();
				bind(Key.get(Network.class, Names.named(AVModule.AV_MODE)))
						.to(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)));
			}

		});

		controler.run();
	}
}
