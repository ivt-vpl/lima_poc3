package ch.ethz.matsim.lima_poc3.misc.convergence.criteria;

public abstract class AbstractCriterion implements ConvergenceCriterion {
	private final String name;

	public AbstractCriterion(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}
}
