package ch.ethz.matsim.lima_poc3.misc.convergence.variables;

public interface ConvergenceVariable {
	void update(int iteration);

	String getName();
}
