package ch.ethz.matsim.lima_poc3.misc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.core.utils.geometry.geotools.MGC;
import org.matsim.core.utils.gis.PointFeatureFactory;
import org.matsim.core.utils.gis.ShapeFileWriter;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

import com.vividsolutions.jts.geom.Coordinate;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;

public class ConvertNetworkToShapefile {
	static public void main(String[] args) throws ConfigurationException, IOException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.requireOptions("network-path", "output-path") //
				.build();

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(cmd.getOptionStrict("network-path"));

		CoordinateReferenceSystem crs = MGC.getCRS("EPSG:2056");

		Collection<SimpleFeature> features = new ArrayList<>(network.getLinks().size());
		PointFeatureFactory featureFactory = new PointFeatureFactory.Builder() //
				.setCrs(crs) //
				.setName("links") //
				.addAttribute("link_id", String.class) //
				.addAttribute("group_id", String.class) //
				.create();

		for (Link link : network.getLinks().values()) {
			Coord startCoord = link.getFromNode().getCoord();
			Coord endCoord = link.getToNode().getCoord();

			Coord displayCoord = CoordUtils.plus(startCoord,
					CoordUtils.scalarMult(0.5, CoordUtils.minus(endCoord, startCoord)));

			SimpleFeature feature = featureFactory.createPoint(new Coordinate(displayCoord.getX(), displayCoord.getY()),
					new Object[] { //
							link.getId().toString(), //
							(String) link.getAttributes().getAttribute("av_waiting_area_id"), //
					}, null);
			features.add(feature);
		}

		ShapeFileWriter.writeGeometries(features, cmd.getOptionStrict("output-path"));
	}
}
