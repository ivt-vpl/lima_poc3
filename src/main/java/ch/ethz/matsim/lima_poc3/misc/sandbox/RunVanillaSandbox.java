package ch.ethz.matsim.lima_poc3.misc.sandbox;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.controler.Controler;

public class RunVanillaSandbox {
	static public void main(String[] args) {
		SandboxFactory factory = new SandboxFactory();
		Scenario scenario = factory.create();

		scenario.getConfig().controler().setLastIteration(100);
		scenario.getConfig().controler().setOutputDirectory("output_vanilla");

		Controler controler = new Controler(scenario);
		controler.run();
	}
}
