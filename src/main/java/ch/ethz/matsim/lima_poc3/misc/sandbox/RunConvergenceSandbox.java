package ch.ethz.matsim.lima_poc3.misc.sandbox;

import java.util.Arrays;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.controler.Controler;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.lima_poc3.misc.convergence.ConvergenceModule;
import ch.ethz.matsim.lima_poc3.misc.convergence.criteria.StandardDeviationOfMeanCriterion;
import ch.ethz.matsim.lima_poc3.misc.convergence.criteria.TCriterion;
import ch.ethz.matsim.lima_poc3.misc.convergence.variables.CategoryVariable;
import ch.ethz.matsim.lima_poc3.misc.convergence.variables.repository.LegCountVariable;
import ch.ethz.matsim.lima_poc3.misc.convergence.variables.repository.TravelTimeVariable;

public class RunConvergenceSandbox {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args) //
				.allowOptions("seed", "iterations", "output", "flow-capacity") //
				.build();

		SandboxFactory factory = new SandboxFactory();
		Scenario scenario = factory.create();

		scenario.getConfig().global().setRandomSeed(cmd.getOption("seed").map(Long::parseLong).orElse(1234L));
		scenario.getConfig().controler()
				.setLastIteration(cmd.getOption("iterations").map(Integer::parseInt).orElse(400));
		scenario.getConfig().controler().setOutputDirectory(cmd.getOption("output").orElse("output_convergence"));
		scenario.getConfig().qsim()
				.setFlowCapFactor(cmd.getOption("flow-capacity").map(Double::parseDouble).orElse(1.0));

		Controler controler = new Controler(scenario);
		controler.addOverridingModule(new ConvergenceModule() {
			public void installCriteria() {
				TravelTimeVariable carTravelTimeVariable = new TravelTimeVariable("car");
				addEventHandlerBinding().toInstance(carTravelTimeVariable);
				addVariable(carTravelTimeVariable);

				LegCountVariable legCountVariable = new LegCountVariable(Arrays.asList("car", "pt", "walk"));
				addEventHandlerBinding().toInstance(legCountVariable);
				addVariable(legCountVariable);

				CategoryVariable ptLegCountVariable = new CategoryVariable(legCountVariable, "pt");
				addVariable(ptLegCountVariable);

				StandardDeviationOfMeanCriterion stdMeanCriterion = new StandardDeviationOfMeanCriterion("cstd",
						ptLegCountVariable, 0.01);
				addCriterion(stdMeanCriterion);

				StandardDeviationOfMeanCriterion stdMeanCriterion100 = new StandardDeviationOfMeanCriterion("cstd100",
						ptLegCountVariable, 0.01, 100);
				addCriterion(stdMeanCriterion100);

				TCriterion tCriterion = new TCriterion("ct", ptLegCountVariable, 0.01);
				addCriterion(tCriterion);
			}
		});

		controler.run();
	}
}
