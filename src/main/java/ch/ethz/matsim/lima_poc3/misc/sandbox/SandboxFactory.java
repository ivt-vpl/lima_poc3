package ch.ethz.matsim.lima_poc3.misc.sandbox;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.network.NetworkFactory;
import org.matsim.api.core.v01.network.Node;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.api.core.v01.population.PopulationFactory;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ActivityParams;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.config.groups.StrategyConfigGroup.StrategySettings;
import org.matsim.core.controler.OutputDirectoryHierarchy.OverwriteFileSetting;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.population.io.PopulationReader;
import org.matsim.core.population.routes.LinkNetworkRouteFactory;
import org.matsim.core.population.routes.NetworkRoute;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.facilities.ActivityFacility;
import org.matsim.pt.transitSchedule.api.Departure;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitScheduleFactory;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleCapacity;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehiclesFactory;

import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;

public class SandboxFactory {
	private final static long SANDBOX_VERSION = 1;

	private Optional<String> populationPath = Optional.empty();
	private double ptHeadway = 300.0;
	private boolean enableModeChoice = true;

	public void setPopulation(String path) {
		populationPath = Optional.of(path);
	}

	public void setEnableModeChoice(boolean enableModeChoice) {
		this.enableModeChoice = enableModeChoice;
	}

	public void setHeadway(double ptHeadway) {
		this.ptHeadway = ptHeadway;
	}

	public Scenario create() {
		String versionString = String.format("Sandbox v%d", SANDBOX_VERSION);

		Config config = ConfigUtils.createConfig();

		config.controler().setOverwriteFileSetting(OverwriteFileSetting.deleteDirectoryIfExists);
		config.controler().setLastIteration(10);
		config.qsim().setEndTime(30.0 * 3600.0);

		config.strategy().setMaxAgentPlanMemorySize(3);

		ActivityParams params = new ActivityParams("generic");
		params.setScoringThisActivityAtAll(false);
		params.setTypicalDuration(1.0);
		config.planCalcScore().addActivityParams(params);

		for (String mode : Arrays.asList("car", "pt", "walk")) {
			ModeParams modeParams = config.planCalcScore().getModes().get(mode);
			modeParams.setConstant(0.0);
			modeParams.setMarginalUtilityOfDistance(0.0);
			modeParams.setMarginalUtilityOfTraveling(0.0);
			modeParams.setMonetaryDistanceRate(0.0);
		}

		config.planCalcScore().setMarginalUtilityOfMoney(0.0);
		config.planCalcScore().setMarginalUtlOfWaiting_utils_hr(0.0);
		config.planCalcScore().setMarginalUtlOfWaitingPt_utils_hr(0.0);
		config.planCalcScore().setUtilityOfLineSwitch(0.0);
		config.planCalcScore().setPerforming_utils_hr(0.0);

		config.planCalcScore().getModes().get("car").setMarginalUtilityOfTraveling(-6.0);
		config.planCalcScore().getModes().get("pt").setMarginalUtilityOfTraveling(-0.0);
		config.planCalcScore().getModes().get("walk").setMarginalUtilityOfTraveling(-12.0);

		config.planCalcScore().getModes().get("car").setConstant(-0.003);
		config.planCalcScore().getModes().get("pt").setConstant(-0.002);
		config.planCalcScore().getModes().get("walk").setConstant(-0.001);

		config.planCalcScore().getModes().remove("bike");
		config.planCalcScore().getModes().remove("other");
		config.planCalcScore().getModes().remove("ride");

		StrategySettings strategy = new StrategySettings();
		strategy.setStrategyName("ChangeExpBeta");
		strategy.setWeight(0.7);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("ReRoute");
		strategy.setWeight(0.15);
		config.strategy().addStrategySettings(strategy);

		strategy = new StrategySettings();
		strategy.setStrategyName("SubtourModeChoice");
		strategy.setWeight(enableModeChoice ? 0.15 : 0.0);
		config.strategy().addStrategySettings(strategy);

		config.subtourModeChoice().setModes(new String[] { "car", "pt", "walk" });
		config.subtourModeChoice().setChainBasedModes(new String[] { "car" });

		config.transit().setUseTransit(true);

		Scenario scenario = ScenarioUtils.createScenario(config);

		int numberOfRows = 14;
		int numberOfColumns = 5;
		int numberOfAgents = 1000;

		double linkLength = 1000;
		double linkCapacity = 50.0;

		double ptTravelTime = 600.0;

		double ptFirstDeparture = 5.0 * 3600.0;
		double ptLastDepartureAfter = 24.0 * 3600.0;

		double densityStd = 100.0;

		double departureTimeMean = 8.0 * 3600.0;
		double departureTimeStd = 3600.0;

		double activityTimeMean = 8.0 * 3600.0;
		double activityTimeStd = 3600.0;
		double commuteTime = 900.0;

		List<Node> nodes = new LinkedList<>();

		Network network = scenario.getNetwork();
		NetworkFactory networkFactory = network.getFactory();

		for (int i = 0; i < numberOfColumns; i++) {
			for (int j = 0; j < numberOfRows; j++) {
				Id<Node> nodeId = Id.createNodeId(String.format("node_%d_%d", i, j));
				Coord nodeCoord = new Coord(i * linkLength, j * linkLength);
				nodes.add(networkFactory.createNode(nodeId, nodeCoord));
			}
		}

		nodes.forEach(network::addNode);

		List<Link> links = new LinkedList<>();

		for (int i = 0; i < numberOfColumns; i++) {
			for (int j = 0; j < numberOfRows; j++) {
				if (i < numberOfColumns - 1) {
					Id<Node> nodeIdA = Id.createNodeId(String.format("node_%d_%d", i, j));
					Id<Node> nodeIdB = Id.createNodeId(String.format("node_%d_%d", i + 1, j));
					Id<Link> linkIdAB = Id.createLinkId(String.format("link_%d_%d_to_%d_%d", i, j, i + 1, j));
					Id<Link> linkIdBA = Id.createLinkId(String.format("link_%d_%d_to_%d_%d", i + 1, j, i, j));

					Node nodeA = network.getNodes().get(nodeIdA);
					Node nodeB = network.getNodes().get(nodeIdB);

					links.add(networkFactory.createLink(linkIdAB, nodeA, nodeB));
					links.add(networkFactory.createLink(linkIdBA, nodeB, nodeA));
				}

				if (j < numberOfRows - 1) {
					Id<Node> nodeIdA = Id.createNodeId(String.format("node_%d_%d", i, j));
					Id<Node> nodeIdB = Id.createNodeId(String.format("node_%d_%d", i, j + 1));
					Id<Link> linkIdAB = Id.createLinkId(String.format("link_%d_%d_to_%d_%d", i, j, i, j + 1));
					Id<Link> linkIdBA = Id.createLinkId(String.format("link_%d_%d_to_%d_%d", i, j + 1, i, j));

					Node nodeA = network.getNodes().get(nodeIdA);
					Node nodeB = network.getNodes().get(nodeIdB);

					links.add(networkFactory.createLink(linkIdAB, nodeA, nodeB));
					links.add(networkFactory.createLink(linkIdBA, nodeB, nodeA));
				}
			}
		}

		for (Link link : links) {
			link.setLength(linkLength);
			link.setAllowedModes(new HashSet<>(Collections.singleton("car")));
			link.setCapacity(linkCapacity);
			link.setFreespeed(13.8);
			link.setNumberOfLanes(1.0);
		}

		config.plansCalcRoute().getModeRoutingParams().get("walk").setBeelineDistanceFactor(1.05);
		config.plansCalcRoute().getModeRoutingParams().get("walk").setTeleportedModeSpeed(1.34);

		links.forEach(network::addLink);

		for (int i = 1; i < numberOfColumns - 1; i++) {
			for (int j = 5; j < 9; j++) {
				network.removeNode(Id.createNodeId(String.format("node_%d_%d", i, j)));
			}
		}

		new NetworkCleaner().run(network);

		Node ptNodeA = network.getNodes().get(Id.createNodeId("node_3_9"));
		Node ptNodeB = network.getNodes().get(Id.createNodeId("node_1_4"));
		Link ptLinkA = networkFactory.createLink(Id.createLinkId("link_pt_ab"), ptNodeA, ptNodeB);
		Link ptLinkB = networkFactory.createLink(Id.createLinkId("link_pt_ba"), ptNodeB, ptNodeA);
		network.addLink(ptLinkA);
		network.addLink(ptLinkB);

		for (Link link : Arrays.asList(ptLinkA, ptLinkB)) {
			link.setCapacity(9999.0);
			link.setAllowedModes(Collections.singleton("rail"));
			link.setLength(CoordUtils.calcEuclideanDistance(ptNodeA.getCoord(), ptNodeB.getCoord()));
			link.setFreespeed(Math.max(link.getLength() / ptTravelTime, 13.8));
			link.setNumberOfLanes(1.0);
		}

		TransitScheduleFactory transitFactory = scenario.getTransitSchedule().getFactory();
		VehiclesFactory vehicleFactory = scenario.getTransitVehicles().getFactory();

		Link stopLinkA = network.getLinks().get(Id.createLinkId("link_2_11_to_3_11"));
		TransitStopFacility stopFacilityA = transitFactory
				.createTransitStopFacility(Id.create("stopA", TransitStopFacility.class), stopLinkA.getCoord(), false);
		stopFacilityA.setLinkId(stopLinkA.getId());

		Link stopLinkB = network.getLinks().get(Id.createLinkId("link_1_2_to_0_2"));
		TransitStopFacility stopFacilityB = transitFactory
				.createTransitStopFacility(Id.create("stopB", TransitStopFacility.class), stopLinkB.getCoord(), false);
		stopFacilityB.setLinkId(stopLinkB.getId());

		List<TransitRouteStop> stops = new LinkedList<>();
		stops.add(transitFactory.createTransitRouteStop(stopFacilityA, 0.0, 0.0));
		stops.add(transitFactory.createTransitRouteStop(stopFacilityB, ptTravelTime, ptTravelTime));
		stops.add(transitFactory.createTransitRouteStop(stopFacilityA, ptTravelTime * 2.0, ptTravelTime * 2.0));

		Id<Link> startEndLinkId = Id.createLinkId("link_2_11_to_3_11");
		List<Id<Link>> routeLinkIds = Arrays.asList(Id.createLinkId("link_3_11_to_3_10"),
				Id.createLinkId("link_3_10_to_3_9"), Id.createLinkId("link_pt_ab"), Id.createLinkId("link_1_4_to_1_3"),
				Id.createLinkId("link_1_3_to_1_2"), Id.createLinkId("link_1_2_to_0_2"),
				Id.createLinkId("link_0_2_to_1_2"), Id.createLinkId("link_1_2_to_1_3"),
				Id.createLinkId("link_1_3_to_1_4"), Id.createLinkId("link_pt_ba"), Id.createLinkId("link_3_9_to_3_10"),
				Id.createLinkId("link_3_10_to_3_11"), Id.createLinkId("link_3_11_to_2_11"));

		NetworkRoute route = (NetworkRoute) new LinkNetworkRouteFactory().createRoute(startEndLinkId, startEndLinkId);
		route.setLinkIds(startEndLinkId, routeLinkIds, startEndLinkId);

		TransitRoute routeA = transitFactory.createTransitRoute(Id.create("LineA", TransitRoute.class), route, stops,
				"rail");

		double departureTime = ptFirstDeparture;
		int index = 0;

		VehicleCapacity vehicleCapacity = vehicleFactory.createVehicleCapacity();
		vehicleCapacity.setSeats(1000);
		vehicleCapacity.setStandingRoom(1000);

		VehicleType vehicleType = vehicleFactory.createVehicleType(Id.create("train", VehicleType.class));
		vehicleType.setCapacity(vehicleCapacity);

		scenario.getTransitVehicles().addVehicleType(vehicleType);

		while (departureTime <= ptLastDepartureAfter) {
			Departure departure = transitFactory.createDeparture(Id.create("dep" + index, Departure.class),
					departureTime);
			routeA.addDeparture(departure);

			Vehicle vehicle = vehicleFactory.createVehicle(Id.createVehicleId(String.format("vehicle_%d", index)),
					vehicleType);
			scenario.getTransitVehicles().addVehicle(vehicle);

			departure.setVehicleId(vehicle.getId());

			index++;
			departureTime += ptHeadway;
		}

		TransitLine lineA = transitFactory.createTransitLine(Id.create("LineA", TransitLine.class));
		lineA.addRoute(routeA);

		scenario.getTransitSchedule().addTransitLine(lineA);
		scenario.getTransitSchedule().addStopFacility(stopFacilityA);
		scenario.getTransitSchedule().addStopFacility(stopFacilityB);

		List<Link> homeCenters = Arrays.asList(network.getLinks().get(Id.createLinkId("link_3_12_to_2_12")),
				network.getLinks().get(Id.createLinkId("link_4_2_to_3_2")));

		List<Link> workCenters = Arrays.asList(network.getLinks().get(Id.createLinkId("link_2_10_to_2_11")),
				network.getLinks().get(Id.createLinkId("link_1_3_to_1_2")));

		List<Double> homeDensities = new LinkedList<>();
		List<Double> workDensities = new LinkedList<>();

		links.clear();

		for (Link link : network.getLinks().values()) {
			if (link.getAllowedModes().contains("car")) {
				links.add(link);
			}
		}

		for (Link link : links) {
			double homeDensity = 0.0;
			double workDensity = 0.0;

			for (Link center : homeCenters) {
				homeDensity += normalDensity(link.getCoord(), center.getCoord(), densityStd);
			}

			for (Link center : workCenters) {
				workDensity += normalDensity(link.getCoord(), center.getCoord(), densityStd);
			}

			homeDensities.add(homeDensity);
			workDensities.add(workDensity);
		}

		double homeSum = homeDensities.stream().mapToDouble(d -> d).sum();
		double workSum = homeDensities.stream().mapToDouble(d -> d).sum();

		List<Double> homeCDF = new LinkedList<>();
		List<Double> workCDF = new LinkedList<>();

		double homeCurrent = 0.0;
		double workCurrent = 0.0;

		for (int i = 0; i < homeDensities.size(); i++) {
			homeCurrent += homeDensities.get(i);
			workCurrent += workDensities.get(i);

			homeCDF.add(homeCurrent / homeSum);
			workCDF.add(workCurrent / workSum);
		}

		Random random = new Random(0);

		if (populationPath.isPresent()) {
			scenario.getPopulation().getFactory().getRouteFactories().setRouteFactory(AVRoute.class,
					new AVRouteFactory());

			new PopulationReader(scenario).readFile(populationPath.get());

			if (!scenario.getPopulation().getName().equals(versionString)) {
				throw new IllegalStateException("Potentially incompatible population version");
			}
		} else {
			Population population = scenario.getPopulation();
			PopulationFactory populationFactory = population.getFactory();

			for (int i = 0; i < numberOfAgents; i++) {
				double workSample = random.nextDouble();
				double homeSample = random.nextDouble();

				int homeIndex = (int) homeCDF.stream().filter(c -> homeSample < c).count() - 1;
				int workIndex = (int) workCDF.stream().filter(c -> workSample < c).count() - 1;

				Link homeLink = links.get(homeIndex);
				Link workLink = links.get(workIndex);

				double morningDepartureTime = random.nextGaussian() * departureTimeStd + departureTimeMean;
				double activityTime = random.nextGaussian() * activityTimeStd + activityTimeMean;

				Plan plan = populationFactory.createPlan();

				Activity activity = populationFactory.createActivityFromLinkId("generic", homeLink.getId());
				activity.setFacilityId(Id.create(homeLink.getId(), ActivityFacility.class));
				activity.setCoord(homeLink.getCoord());
				activity.setEndTime(morningDepartureTime);
				plan.addActivity(activity);

				Leg leg = populationFactory.createLeg("car");
				plan.addLeg(leg);

				activity = populationFactory.createActivityFromLinkId("generic", workLink.getId());
				activity.setFacilityId(Id.create(workLink.getId(), ActivityFacility.class));
				activity.setCoord(workLink.getCoord());
				activity.setEndTime(morningDepartureTime + activityTime + commuteTime);
				plan.addActivity(activity);

				leg = populationFactory.createLeg("car");
				plan.addLeg(leg);

				activity = populationFactory.createActivityFromLinkId("generic", homeLink.getId());
				activity.setFacilityId(Id.create(homeLink.getId(), ActivityFacility.class));
				activity.setCoord(homeLink.getCoord());
				plan.addActivity(activity);

				Person person = populationFactory.createPerson(Id.createPersonId(String.format("person_%d", i)));
				person.addPlan(plan);

				population.addPerson(person);
			}

			population.setName(versionString);
		}

		ActivityFacilities facilities = scenario.getActivityFacilities();

		for (Link link : network.getLinks().values()) {
			ActivityFacility facility = facilities.getFactory().createActivityFacility(
					Id.create(link.getId(), ActivityFacility.class), link.getCoord(), link.getId());
			facilities.addActivityFacility(facility);
		}

		return scenario;
	}

	private double normalDensity(Coord coord, Coord mean, double std) {
		double density = Math.pow(coord.getX() - mean.getX(), 2.0) + Math.pow(coord.getY() - mean.getY(), 2.0);
		density /= -2.0 * Math.pow(std, 2.0);
		density = Math.exp(density);
		density /= (2.0 * Math.PI * Math.pow(std, 2.0));
		return density;
	}
}
