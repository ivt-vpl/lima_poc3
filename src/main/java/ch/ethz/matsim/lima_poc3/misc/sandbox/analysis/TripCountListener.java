package ch.ethz.matsim.lima_poc3.misc.sandbox.analysis;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.handler.PersonArrivalEventHandler;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.ShutdownListener;

public class TripCountListener implements PersonArrivalEventHandler, IterationEndsListener, ShutdownListener {
	private final Map<String, ConvergenceWriter> convergenceWriters = new HashMap<>();
	private final Map<String, BufferedWriter> writers = new HashMap<>();
	private final Map<String, Integer> numberOfLegs = new HashMap<>();
	private final Set<String> modes;

	public TripCountListener(OutputDirectoryHierarchy output, Set<String> modes) {
		this.modes = modes;

		try {
			for (String mode : modes) {
				String outputPath = output.getOutputFilename(String.format("%s_count.csv", mode));
				BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath)));
				ConvergenceWriter convergenceWriter = new ConvergenceWriter(writer);

				writers.put(mode, writer);
				convergenceWriters.put(mode, convergenceWriter);
				numberOfLegs.put(mode, 0);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void handleEvent(PersonArrivalEvent event) {
		for (String mode : modes) {
			if (event.getLegMode().equals(mode)) {
				numberOfLegs.put(mode, numberOfLegs.get(mode) + 1);
			}
		}
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		try {
			for (String mode : modes) {
				convergenceWriters.get(mode).addValue((double) numberOfLegs.get(mode));
				numberOfLegs.put(mode, 0);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void notifyShutdown(ShutdownEvent event) {
		try {
			for (String mode : modes) {
				writers.get(mode).close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
