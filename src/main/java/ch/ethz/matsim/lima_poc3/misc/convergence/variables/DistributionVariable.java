package ch.ethz.matsim.lima_poc3.misc.convergence.variables;

import java.util.Collection;

public interface DistributionVariable extends ConvergenceVariable {
	Collection<Double> getValues();
}
