package ch.ethz.matsim.lima_poc3.misc.sandbox.analysis;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.OutputDirectoryHierarchy;

import com.google.inject.Provides;
import com.google.inject.Singleton;

public class TravelTimeListenerModule extends AbstractModule {
	private final Set<String> modes;

	public TravelTimeListenerModule(String... modes) {
		this.modes = new HashSet<>(Arrays.asList(modes));
	}

	@Override
	public void install() {
		addEventHandlerBinding().to(TravelTimeListener.class);
		addControlerListenerBinding().to(TravelTimeListener.class);
	}

	@Provides
	@Singleton
	public TravelTimeListener provideTripCountListener(OutputDirectoryHierarchy output) {
		return new TravelTimeListener(output, modes);
	}
}
