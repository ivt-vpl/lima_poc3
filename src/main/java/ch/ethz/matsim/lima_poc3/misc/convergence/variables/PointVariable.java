package ch.ethz.matsim.lima_poc3.misc.convergence.variables;

public interface PointVariable extends ConvergenceVariable {
	double getValue();
}
