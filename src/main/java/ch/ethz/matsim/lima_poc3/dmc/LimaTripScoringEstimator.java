package ch.ethz.matsim.lima_poc3.dmc;

import java.util.Collection;
import java.util.List;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.router.TripRouter;
import org.matsim.core.scoring.functions.ModeUtilityParameters;
import org.matsim.core.scoring.functions.ScoringParameters;
import org.matsim.core.scoring.functions.ScoringParametersForPerson;
import org.matsim.core.utils.misc.Time;
import org.matsim.facilities.ActivityFacilities;
import org.matsim.pt.routes.ExperimentalTransitRoute;

import ch.ethz.matsim.discrete_mode_choice.components.estimators.AbstractTripRouterEstimator;
import ch.ethz.matsim.discrete_mode_choice.components.estimators.MATSimTripCandidate;
import ch.ethz.matsim.discrete_mode_choice.components.utils.PTWaitingTimeEstimator;
import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.discrete_mode_choice.model.trip_based.candidates.TripCandidate;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.ExtendedAVRoute;
import ch.ethz.matsim.lima_poc3.av.scoring.LIMAScoringParameterForPerson;
import ch.ethz.matsim.lima_poc3.av.scoring.LIMAScoringParameters;

public class LimaTripScoringEstimator extends AbstractTripRouterEstimator {
	private final ScoringParametersForPerson scoringParametersForPerson;
	private final LIMAScoringParameterForPerson limaScoringParameterForPerson;
	private final PTWaitingTimeEstimator waitingTimeEstimator;
	private final Collection<String> ptLegModes;

	public LimaTripScoringEstimator(ActivityFacilities facilities, TripRouter tripRouter,
			PTWaitingTimeEstimator waitingTimeEstimator, ScoringParametersForPerson scoringParametersForPerson,
			Collection<String> ptModes, LIMAScoringParameterForPerson limaScoringParametersForPerson) {
		super(tripRouter, facilities);
		this.waitingTimeEstimator = waitingTimeEstimator;
		this.scoringParametersForPerson = scoringParametersForPerson;
		this.limaScoringParameterForPerson = limaScoringParametersForPerson;
		this.ptLegModes = ptModes;
	}

	protected double estimateLegUtility(Person person, ScoringParameters parameters, Leg leg) {
		String mode = leg.getMode();

		ModeUtilityParameters modeParams = parameters.modeParams.get(mode);

		if (modeParams == null) {
			if (mode.contains(TransportMode.walk)) {
				modeParams = parameters.modeParams.get(TransportMode.walk);
			} else {
				throw new IllegalStateException("No scoring parameter exist for: " + mode);
			}
		}

		double utility = modeParams.constant;
		utility += modeParams.marginalUtilityOfTraveling_s * leg.getTravelTime();
		utility += modeParams.marginalUtilityOfDistance_m * leg.getRoute().getDistance();
		utility += parameters.marginalUtilityOfMoney * modeParams.monetaryDistanceCostRate
				* leg.getRoute().getDistance();

		if (mode.contains("av")) {
			LIMAScoringParameters limaParameters = limaScoringParameterForPerson.getScoringParameters(person);

			ExtendedAVRoute avRoute = (ExtendedAVRoute) leg.getRoute();
			double waitingTime = avRoute.getPickupTime();

			utility -= modeParams.marginalUtilityOfTraveling_s * waitingTime;

			if (mode.equals(AVDirectRoutingModule.AV_DIRECT_MODE)) {
				utility += limaParameters.marginalUtilityOfDirectWaitingTime_s * waitingTime;
			} else if (mode.equals(AVFeederRoutingModule.AV_FEEDER_MODE)) {
				utility += limaParameters.marginalUtilityOfFeederWaitingTime_s * waitingTime;
			} else {
				throw new IllegalStateException();
			}
		}

		return utility;
	}

	protected double estimateStandardTrip(Person person, List<? extends PlanElement> elements) {
		ScoringParameters parameters = scoringParametersForPerson.getScoringParameters(person);
		double utility = 0.0;

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				utility += estimateLegUtility(person, parameters, (Leg) element);
			}
		}

		return utility;
	}

	protected double estimatePtTrip(Person person, List<? extends PlanElement> elements, double departureTime) {
		ScoringParameters parameters = scoringParametersForPerson.getScoringParameters(person);
		double utility = estimateStandardTrip(person, elements);

		int numberOfVehicularLegs = 0;
		double time = departureTime;

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;

				if (ptLegModes.contains(leg.getMode())) {
					ExperimentalTransitRoute route = (ExperimentalTransitRoute) leg.getRoute();

					double waitingTime = waitingTimeEstimator.estimateWaitingTime(time, route);
					utility += parameters.marginalUtilityOfWaitingPt_s * waitingTime;
					utility -= parameters.modeParams.get(leg.getMode()).marginalUtilityOfTraveling_s * waitingTime;

					numberOfVehicularLegs++;
				}

				time += leg.getTravelTime();
			}
		}

		if (numberOfVehicularLegs > 0) {
			utility += parameters.utilityOfLineSwitch * (numberOfVehicularLegs - 1);
		}

		return utility;
	}

	protected double estimateTripUtility(Person person, String tripMode, DiscreteModeChoiceTrip trip,
			List<? extends PlanElement> elements) {
		double utility = 0.0;

		if (tripMode.equals(TransportMode.pt)) {
			utility += estimatePtTrip(person, elements, trip.getDepartureTime());
		} else {
			utility += estimateStandardTrip(person, elements);
		}

		return utility;
	}

	@Override
	protected TripCandidate estimateTripCandidate(Person person, String mode, DiscreteModeChoiceTrip trip,
			List<TripCandidate> previousTrips, List<? extends PlanElement> elements) {
		double utility = estimateTripUtility(person, mode, trip, elements);
		double time = trip.getDepartureTime();

		for (PlanElement element : elements) {
			if (element instanceof Leg) {
				Leg leg = (Leg) element;

				if (!Time.isUndefinedTime(leg.getDepartureTime())) {
					time = leg.getDepartureTime();
				}

				time += leg.getTravelTime();
			}
		}

		double travelTime = time - trip.getDepartureTime();
		return new MATSimTripCandidate(utility, mode, elements, travelTime);
	}
}
