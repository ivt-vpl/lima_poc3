package ch.ethz.matsim.lima_poc3.dmc;

import java.util.Collection;
import java.util.List;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Person;

import ch.ethz.matsim.discrete_mode_choice.model.DiscreteModeChoiceTrip;
import ch.ethz.matsim.discrete_mode_choice.model.constraints.AbstractTripConstraint;
import ch.ethz.matsim.discrete_mode_choice.model.trip_based.TripConstraint;
import ch.ethz.matsim.discrete_mode_choice.model.trip_based.TripConstraintFactory;

public class FixRideConstraint extends AbstractTripConstraint {
	@Override
	public boolean validateBeforeEstimation(DiscreteModeChoiceTrip trip, String mode, List<String> previousModes) {
		if (trip.getInitialMode().equals(TransportMode.ride)) {
			if (!mode.equals(TransportMode.ride)) {
				// If we currently have ride, but the new mode is not ride, forbid this change!
				return false;
			}
		}

		if (mode.equals(TransportMode.ride)) {
			if (!trip.getInitialMode().equals(TransportMode.ride)) {
				// We want to change to ride, but initial mode is not ride, so forbid this
				// change!
				return false;
			}
		}

		return true;
	}

	static public class Factory implements TripConstraintFactory {
		@Override
		public TripConstraint createConstraint(Person person, List<DiscreteModeChoiceTrip> planTrips,
				Collection<String> availableModes) {
			return new FixRideConstraint();
		}
	}
}
