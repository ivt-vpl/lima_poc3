package ch.ethz.matsim.lima_poc3.dmc;

import org.matsim.core.config.ReflectiveConfigGroup;

public class LimaModeChoiceConfigGroup extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "lima_mode_choice";

	public static final String USE_DISCRETE_MODE_CHOICE = "useDiscreteModeChoice";
	public static final String USE_IMPORTANCE_SAMPLING = "useImportanceSampling";
	public static final String OPERATING_AREA_LINK_ATTRIBUTE = "operatingAreaLinkAttribute";
	public static final String FIX_RIDE = "fixRide";

	private boolean useDiscreteModeChoice = false;
	private boolean useImportanceSampling = false;
	private String operatingAreaLinkAttribute = "avOperatingArea";
	private boolean fixRide = false;

	public LimaModeChoiceConfigGroup() {
		super(GROUP_NAME);
	}

	@StringGetter(USE_DISCRETE_MODE_CHOICE)
	public boolean getUseDiscreteModeChoice() {
		return useDiscreteModeChoice;
	}

	@StringSetter(USE_DISCRETE_MODE_CHOICE)
	public void setUseDiscreteModeChoice(boolean useDiscreteModeChoice) {
		this.useDiscreteModeChoice = useDiscreteModeChoice;
	}

	@StringGetter(USE_IMPORTANCE_SAMPLING)
	public boolean getUseImportanceSampling() {
		return useImportanceSampling;
	}

	@StringSetter(USE_IMPORTANCE_SAMPLING)
	public void setUseImportanceSampling(boolean useImportanceSampling) {
		this.useImportanceSampling = useImportanceSampling;
	}

	@StringGetter(OPERATING_AREA_LINK_ATTRIBUTE)
	public String getOperatingAreaLinkAttribute() {
		return operatingAreaLinkAttribute;
	}

	@StringSetter(OPERATING_AREA_LINK_ATTRIBUTE)
	public void setOperatingAreaLinkAttribute(String operatingAreaLinkAttribute) {
		this.operatingAreaLinkAttribute = operatingAreaLinkAttribute;
	}

	@StringGetter(FIX_RIDE)
	public boolean getFixRide() {
		return fixRide;
	}

	@StringSetter(FIX_RIDE)
	public void setFixRide(boolean fixRide) {
		this.fixRide = fixRide;
	}
}
