package ch.ethz.matsim.lima_poc3.flow_efficiency.config;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Id;
import org.matsim.core.config.ReflectiveConfigGroup;
import org.matsim.vehicles.VehicleType;

public class LimaFlowEfficiencyConfigGroup extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "lima_flow_efficiency";

	public final static String USE_FLOW_EFFICIENCY = "useFlowEfficiency";
	public final static String LINK_ATTRIBUTE = "linkAttribute";
	public final static String CONSTANT_FLOW_EFFICIENCY = "constantFlowEfficiency";
	public final static String AUTOMATED_VEHICLE_TYPE_IDS = "automatedVehicleTypeIds";

	private boolean useFlowEfficiency = true;
	private String linkAttribute = null;
	private double constantFlowEfficiency = 1.5;
	private Collection<Id<VehicleType>> automatedVehicleTypeIds = Collections.emptySet();

	public LimaFlowEfficiencyConfigGroup() {
		super(GROUP_NAME);
	}

	@StringGetter(USE_FLOW_EFFICIENCY)
	public boolean getUseFlowEfficiency() {
		return useFlowEfficiency;
	}

	@StringSetter(USE_FLOW_EFFICIENCY)
	public void setUseFlowEfficiency(boolean useFlowEfficiency) {
		this.useFlowEfficiency = useFlowEfficiency;
	}

	@StringGetter(LINK_ATTRIBUTE)
	public String getLinkAttribute() {
		return linkAttribute;
	}

	@StringSetter(LINK_ATTRIBUTE)
	public void setLinkAttribute(String linkAttribute) {
		this.linkAttribute = linkAttribute;
	}

	@StringGetter(CONSTANT_FLOW_EFFICIENCY)
	public double getConstantFlowEfficiency() {
		return constantFlowEfficiency;
	}

	@StringSetter(CONSTANT_FLOW_EFFICIENCY)
	public void setConstantFlowEfficiency(double constantFlowEfficiency) {
		this.constantFlowEfficiency = constantFlowEfficiency;
	}

	public Collection<Id<VehicleType>> getAutomatedVehicleTypeIds() {
		return automatedVehicleTypeIds;
	}

	public void setAutomatedVehicleTypeIds(Collection<Id<VehicleType>> automatedVehicleTypeIds) {
		this.automatedVehicleTypeIds = automatedVehicleTypeIds;
	}

	public String getAutomatedVehicleTypeIdsAsString() {
		return String.join(",", automatedVehicleTypeIds.stream().map(Id::toString).collect(Collectors.toSet()));
	}

	public void setAutomatedVehicleTypeIdsAsString(String automatedVehicleTypeIds) {
		this.automatedVehicleTypeIds = Arrays.asList(automatedVehicleTypeIds.split(",")).stream().map(String::trim)
				.map(s -> Id.create(s, VehicleType.class)).collect(Collectors.toSet());
	}
}
