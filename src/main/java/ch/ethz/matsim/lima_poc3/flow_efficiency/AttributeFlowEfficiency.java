package ch.ethz.matsim.lima_poc3.flow_efficiency;

import java.util.Collection;

import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleType;

public class AttributeFlowEfficiency implements FlowEfficiency {
	private final Collection<VehicleType> automatedVehicleTypes;
	private final String linkAttribute;
	private final double defaultFlowEfficiency;

	public AttributeFlowEfficiency(Collection<VehicleType> automatedVehicleTypes, String linkAttribute,
			double defaultFlowEfficiency) {
		this.linkAttribute = linkAttribute;
		this.automatedVehicleTypes = automatedVehicleTypes;
		this.defaultFlowEfficiency = defaultFlowEfficiency;
	}

	@Override
	public double getFlowEfficiency(Vehicle vehicle, Link link) {
		Double flowEfficiency = (Double) link.getAttributes().getAttribute(linkAttribute);

		if (automatedVehicleTypes.contains(vehicle.getType())) {
			if (flowEfficiency == null) {
				return defaultFlowEfficiency;
			} else {
				return flowEfficiency;
			}
		} else {
			return vehicle.getType().getFlowEfficiencyFactor();
		}
	}
}
