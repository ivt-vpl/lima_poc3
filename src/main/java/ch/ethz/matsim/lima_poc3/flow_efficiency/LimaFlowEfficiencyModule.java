package ch.ethz.matsim.lima_poc3.flow_efficiency;

import java.util.Collection;
import java.util.HashSet;

import org.matsim.api.core.v01.Id;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.StartupListener;
import org.matsim.vehicles.VehicleCapacity;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.Vehicles;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.lima_poc3.flow_efficiency.config.LimaFlowEfficiencyConfigGroup;

public class LimaFlowEfficiencyModule extends AbstractModule {
	@Override
	public void install() {
		LimaFlowEfficiencyConfigGroup config = (LimaFlowEfficiencyConfigGroup) getConfig().getModules()
				.get(LimaFlowEfficiencyConfigGroup.GROUP_NAME);

		if (config.getUseFlowEfficiency()) {
			String linkAttribute = config.getLinkAttribute();

			if (linkAttribute == null) {
				bind(FlowEfficiency.class).to(ConstantFlowEfficiency.class);
				addControlerListenerBinding().to(FlowEfficiencyUpdater.class);
			} else {
				bind(FlowEfficiency.class).to(AttributeFlowEfficiency.class);
				addControlerListenerBinding().to(FlowEfficiencyUpdater.class);
			}

			bind(Key.get(VehicleType.class, Names.named(AVModule.AV_MODE)))
					.to(Key.get(VehicleType.class, Names.named("shared")));
		}
	}

	static private class FlowEfficiencyUpdater implements StartupListener {
		private final FlowEfficiency flowEfficiency;

		public FlowEfficiencyUpdater(FlowEfficiency flowEfficiency) {
			this.flowEfficiency = flowEfficiency;
		}

		@Override
		public void notifyStartup(StartupEvent event) {
			FlowEfficiency.INSTANCE.setInstance(flowEfficiency);
		}
	}

	@Provides
	@Singleton
	public FlowEfficiencyUpdater provideFlowEfficiencyUpdater(FlowEfficiency flowEfficiency) {
		return new FlowEfficiencyUpdater(flowEfficiency);
	}

	@Provides
	@Named("automated")
	public Collection<VehicleType> provideAutomatedVehicleTypes(LimaFlowEfficiencyConfigGroup config, Vehicles vehicles,
			@Named("shared") VehicleType sharedVehicleType) {
		Collection<VehicleType> automatedVehicleTypes = new HashSet<>();
		automatedVehicleTypes.add(sharedVehicleType);

		for (Id<VehicleType> id : config.getAutomatedVehicleTypeIds()) {
			automatedVehicleTypes.add(vehicles.getVehicleTypes().get(id));
		}

		return automatedVehicleTypes;
	}

	@Provides
	@Singleton
	public ConstantFlowEfficiency provideConstantFlowEfficiency(
			@Named("automated") Collection<VehicleType> automatedVehicleTypes, LimaFlowEfficiencyConfigGroup config) {
		return new ConstantFlowEfficiency(automatedVehicleTypes, config.getConstantFlowEfficiency());
	}

	@Provides
	@Singleton
	public AttributeFlowEfficiency provideLimaFlowEfficiency(
			@Named("automated") Collection<VehicleType> automatedVehicleTypes, LimaFlowEfficiencyConfigGroup config) {
		return new AttributeFlowEfficiency(automatedVehicleTypes, config.getLinkAttribute(),
				config.getConstantFlowEfficiency());
	}

	@Provides
	@Singleton
	@Named("shared")
	public VehicleType provideSharedVehicleType(Vehicles vehicles) {
		Id<VehicleType> id = Id.create("shared", VehicleType.class);

		if (vehicles.getVehicleTypes().containsKey(id)) {
			return vehicles.getVehicleTypes().get(id);
		} else {
			VehicleType vehicleType = vehicles.getFactory().createVehicleType(id);
			vehicles.addVehicleType(vehicleType);

			VehicleCapacity vehicleCapacity = vehicles.getFactory().createVehicleCapacity();
			vehicleCapacity.setSeats(4);
			vehicleType.setCapacity(vehicleCapacity);

			return vehicleType;
		}
	}
}
