package ch.ethz.matsim.lima_poc3.flow_efficiency;

import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;

public interface FlowEfficiency {
	double getFlowEfficiency(Vehicle vehicle, Link link);

	public static class DefaultFlowEfficiency implements FlowEfficiency {
		@Override
		public double getFlowEfficiency(Vehicle vehicle, Link link) {
			return vehicle.getType().getFlowEfficiencyFactor();
		}
	}

	public static class FlowEfficiencyInstance implements FlowEfficiency {
		private FlowEfficiency instance;

		public FlowEfficiencyInstance() {
			this.instance = new DefaultFlowEfficiency();
		}

		@Override
		public double getFlowEfficiency(Vehicle vehicle, Link link) {
			return instance.getFlowEfficiency(vehicle, link);
		}

		public void setInstance(FlowEfficiency instance) {
			this.instance = instance;
		}
	}

	static public FlowEfficiencyInstance INSTANCE = new FlowEfficiencyInstance();
}
