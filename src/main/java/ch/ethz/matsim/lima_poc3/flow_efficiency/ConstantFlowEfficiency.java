package ch.ethz.matsim.lima_poc3.flow_efficiency;

import java.util.Collection;

import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleType;

public class ConstantFlowEfficiency implements FlowEfficiency {
	private final Collection<VehicleType> automatedVehicleTypes;
	private final double flowEfficiency;

	public ConstantFlowEfficiency(Collection<VehicleType> automatedVehicleTypes, double flowEfficiency) {
		this.flowEfficiency = flowEfficiency;
		this.automatedVehicleTypes = automatedVehicleTypes;
	}

	@Override
	public double getFlowEfficiency(Vehicle vehicle, Link link) {
		return automatedVehicleTypes.contains(vehicle.getType()) ? flowEfficiency
				: vehicle.getType().getFlowEfficiencyFactor();
	}
}
