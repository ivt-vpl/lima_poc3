package ch.ethz.matsim.lima_poc3.av.space;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.GenericEvent;
import org.matsim.api.core.v01.network.Link;

public class SpaceAvailabilityEvent extends GenericEvent {
	public static final String TYPE = "space availability";

	private String occupancyType;
	private final Id<Link> linkId;
	private long availability;

	public SpaceAvailabilityEvent(double time, String occupancyType, Id<Link> linkId, long availability) {
		super(TYPE, time);

		this.linkId = linkId;
		this.availability = availability;
		this.occupancyType = occupancyType;

		this.getAttributes().put("occupancyType", occupancyType);
		this.getAttributes().put("availability", String.valueOf(availability));
		this.getAttributes().put("link", linkId.toString());
	}

	public Id<Link> getLinkId() {
		return linkId;
	}

	public long getAvailability() {
		return availability;
	}

	public String getOccupancyType() {
		return occupancyType;
	}
}
