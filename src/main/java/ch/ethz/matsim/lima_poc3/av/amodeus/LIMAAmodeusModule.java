package ch.ethz.matsim.lima_poc3.av.amodeus;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Properties;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Population;
import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.idsc.amodeus.data.ReferenceFrame;
import ch.ethz.idsc.amodeus.lp.LPCreator;
import ch.ethz.idsc.amodeus.lp.LPSolver;
import ch.ethz.idsc.amodeus.net.MatsimAmodeusDatabase;
import ch.ethz.idsc.amodeus.options.LPOptions;
import ch.ethz.idsc.amodeus.options.LPOptionsBase;
import ch.ethz.idsc.amodeus.prep.MatsimKMeansVirtualNetworkCreator;
import ch.ethz.idsc.amodeus.traveldata.TravelData;
import ch.ethz.idsc.amodeus.traveldata.TravelDataCreator;
import ch.ethz.idsc.amodeus.virtualnetwork.core.VirtualNetwork;
import ch.ethz.idsc.tensor.Tensor;
import ch.ethz.matsim.av.framework.AVModule;

public class LIMAAmodeusModule extends AbstractModule {
	private final int numberOfVirtualNodes;
	private final int numberOfVehicles;
	private final int travelDataInterval;

	// We may need to parametrize this later?
	private final boolean completeGraph = true;
	private final String lpSolver = "timeInvariant";

	public LIMAAmodeusModule(int numberOfVirtualNodes, int numberOfVehicles, int travelDataInterval) {
		this.numberOfVirtualNodes = numberOfVirtualNodes;
		this.numberOfVehicles = numberOfVehicles;
		this.travelDataInterval = travelDataInterval;
	}

	@Override
	public void install() {

	}

	@Provides
	@Singleton
	public VirtualNetwork<Link> provideVirtualNetwork(Population population, @Named(AVModule.AV_MODE) Network network) {
		return MatsimKMeansVirtualNetworkCreator.createVirtualNetwork(population, network, numberOfVirtualNodes,
				completeGraph);
	}

	@Provides
	@Singleton
	public TravelData provideTravelData(VirtualNetwork<Link> virtualNetwork, @Named(AVModule.AV_MODE) Network network,
			Population population, Config config) throws Exception {
		int endTime = (int) config.qsim().getEndTime();

		Properties properties = new Properties();
		properties.setProperty(LPOptionsBase.LPSOLVER, lpSolver);

		// We cannot easily construct a LPOptions object without a file, but there is a
		// private consturctor that accepts a Properties object. Here, we invoke it via
		// Reflection API.
		Constructor<LPOptions> constructor = LPOptions.class.getDeclaredConstructor(Properties.class);
		constructor.setAccessible(true);
		LPOptions lpOptions = constructor.newInstance(properties);

		// We recover the content from TravelDataCreator.create with our custom
		// lpOptions. Unfortunately, this method internally
		// calls TravelDataCreator.getLambdaAbsolute, which is private. Therefore, we
		// have to use Reflection once more here.
		Method getLambdaAbsolute = TravelDataCreator.class.getDeclaredMethod("getLambdaAbsolute", Network.class,
				VirtualNetwork.class, Population.class, int.class, int.class);
		getLambdaAbsolute.setAccessible(true);
		Tensor lambdaAbsolute = (Tensor) getLambdaAbsolute.invoke(null, network, virtualNetwork, population,
				travelDataInterval, endTime);

		LPCreator lpCreator = lpOptions.getLPSolver();
		LPSolver solver = lpCreator.create(virtualNetwork, network, lpOptions, lambdaAbsolute, numberOfVehicles,
				endTime);

		solver.initiateLP();
		solver.solveLP(false);

		String lpName = solver.getClass().getSimpleName();
		Tensor alphaAbsolute = solver.getAlphaAbsolute_ij();
		Tensor v0_i = solver.getV0_i();
		Tensor fAbsolute = solver.getFAbsolute_ij();

		return new TravelData(virtualNetwork.getvNetworkID(), lambdaAbsolute, alphaAbsolute, fAbsolute, v0_i, lpName,
				endTime);
	}

	@Provides
	@Singleton
	public MatsimAmodeusDatabase provideMATSimAmodeusDatabase(@Named(AVModule.AV_MODE) Network network) {
		ReferenceFrame referenceFrame = new SandboxReferenceFrame();
		return MatsimAmodeusDatabase.initialize(network, referenceFrame);
	}
}
