package ch.ethz.matsim.lima_poc3.av.components.dispatcher.unblocking;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.matsim.api.core.v01.network.Link;

import ch.ethz.matsim.av.data.AVVehicle;

public class SpatialLink {
	final private boolean DEBUG = true;

	final private Link link;

	private List<AVVehicle> idleVehicles = new LinkedList<>();
	private List<AVVehicle> approachingVehicles = new LinkedList<>();

	public SpatialLink(Link link) {
		this.link = link;
	}

	public Link getLink() {
		return link;
	}

	public Collection<AVVehicle> getExcessVehicles(long numberOfSpots) {
		if (approachingVehicles.size() + idleVehicles.size() > numberOfSpots) {
			long excessVehicles = approachingVehicles.size() + idleVehicles.size() - numberOfSpots;
			long dispatchableCount = Math.min(idleVehicles.size(), excessVehicles);
			return idleVehicles.subList(0, (int) dispatchableCount);
		} else {
			return Collections.emptyList();
		}
	}

	public long getExcessSpots(long numberOfSpots) {
		if (approachingVehicles.size() + idleVehicles.size() < numberOfSpots) {
			return numberOfSpots - approachingVehicles.size() - idleVehicles.size();
		} else {
			return 0;
		}
	}

	public void addIdleVehicle(AVVehicle vehicle) {
		if (DEBUG && idleVehicles.contains(vehicle)) {
			throw new IllegalStateException();
		}

		idleVehicles.add(vehicle);
	}

	public void removeIdleVehicle(AVVehicle vehicle) {
		if (DEBUG && !idleVehicles.contains(vehicle)) {
			throw new IllegalStateException();
		}

		idleVehicles.remove(vehicle);
	}

	public void addApproachingVehicle(AVVehicle vehicle) {
		if (DEBUG && approachingVehicles.contains(vehicle)) {
			throw new IllegalStateException();
		}

		approachingVehicles.add(vehicle);
	}

	public void removeApproachingVehicle(AVVehicle vehicle) {
		if (DEBUG && !approachingVehicles.contains(vehicle)) {
			throw new IllegalStateException();
		}

		approachingVehicles.remove(vehicle);
	}
}
