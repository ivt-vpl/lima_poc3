package ch.ethz.matsim.lima_poc3.av.pricing;

public class DefaultAVPricing implements AVPricing {
	@Override
	public boolean useCostCalculator() {
		return false;
	}

	@Override
	public double computeCost(double distance_km, double priceFactor) {
		throw new IllegalStateException();
	}
}
