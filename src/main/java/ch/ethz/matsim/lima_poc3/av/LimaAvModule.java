package ch.ethz.matsim.lima_poc3.av;

import java.util.Collections;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.NetworkCleaner;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVUtils;
import ch.ethz.matsim.lima_poc3.av.analysis.AVNetworkListener;
import ch.ethz.matsim.lima_poc3.av.components.SpatiallyConstrainedPopulationDensityGenerator;
import ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking.ParkingSingleHeuristicDispatcher;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.NearestInteractionLinkData;
import ch.ethz.matsim.lima_poc3.av.space.VehicleInteractionHandler;

public class LimaAvModule extends AbstractModule {
	private final Logger logger = Logger.getLogger(LimaAvModule.class);

	@Override
	public void install() {
		LimaAvConfigGroup limaAvConfig = (LimaAvConfigGroup) getConfig().getModules().get(LimaAvConfigGroup.GROUP_NAME);
		VehicleInteractionHandler.INSTANCE.enableLogger();

		bind(NearestInteractionLinkData.class).asEagerSingleton();

		AVUtils.registerGeneratorFactory(binder(), "SpatiallyConstrainedPopulationDensity",
				SpatiallyConstrainedPopulationDensityGenerator.Factory.class);

		AVUtils.registerDispatcherFactory(binder(), "ParkingDispatcher",
				ParkingSingleHeuristicDispatcher.Factory.class);

		AVConfig avConfig = new AVConfig();
		AVOperatorConfig operator = avConfig.createOperatorConfig(AVModule.AV_MODE);
		operator.createDispatcherConfig("ParkingDispatcher");
		operator.createGeneratorConfig("SpatiallyConstrainedPopulationDensity")
				.setNumberOfVehicles(limaAvConfig.getFleetSize());
		operator.createPriceStructureConfig();
		avConfig.getTimingParameters().setPickupDurationPerStop(60.0);

		bind(AVConfig.class).toInstance(avConfig);
		bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
				.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();

		addControlerListenerBinding().to(AVNetworkListener.class);
	}

	private int countLinks(Network network) {
		int count = 0;

		for (Link link : network.getLinks().values()) {
			if (link.getAllowedModes().contains("av")) {
				count++;
			}
		}

		return count;
	}

	@Provides
	@Singleton
	@Named(AVModule.AV_MODE)
	public Network provideAvNetwork(Network fullNetwork) {
		Network avNetwork = NetworkUtils.createNetwork();

		new TransportModeNetworkFilter(fullNetwork).filter(avNetwork, Collections.singleton("av"));
		new NetworkCleaner().run(avNetwork);

		int initialCount = countLinks(fullNetwork);
		int finalCount = countLinks(avNetwork);

		if (initialCount != finalCount) {
			logger.warn(String.format("Had to clean AV network! Number of AV links has been decreased from %d to %d!",
					initialCount, finalCount));
		}

		return avNetwork;
	}
}
