package ch.ethz.matsim.lima_poc3.av.components;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.Population;
import org.matsim.contrib.dvrp.data.Vehicle;
import org.matsim.core.gbl.MatsimRandom;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.config.AVGeneratorConfig;
import ch.ethz.matsim.av.data.AVVehicle;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.generator.AVGenerator;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.NearestInteractionLinkData;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;

public class SpatiallyConstrainedPopulationDensityGenerator implements AVGenerator {
	private final List<Id<Link>> linkIds;
	private final List<Double> weights;
	private List<Double> cdf;
	private final List<Long> maximumNumberOfVehicles;
	private final List<Long> numberOfVehicles;

	private final int fleetSize;
	private final Random random;
	private final Network network;
	
	private int count = 0;
	
	public SpatiallyConstrainedPopulationDensityGenerator(List<Id<Link>> linkIds, List<Double> weights,
			List<Long> maximumNumberOfVehicles, int fleetSize, Random random, Network network) {
		this.linkIds = linkIds;
		this.weights = weights;
		this.fleetSize = fleetSize;
		this.random = random;
		this.network = network;
		this.maximumNumberOfVehicles = maximumNumberOfVehicles;
		this.numberOfVehicles = new LinkedList<>(Collections.nCopies(maximumNumberOfVehicles.size(), 0L));
		
		updateCdf();
	}
	
	private void updateCdf() {
		cdf = new ArrayList<>(linkIds.size());
		
		for (int i = 0; i < linkIds.size(); i++) {
			if (i == 0) {
				cdf.add(weights.get(0));
			} else {
				cdf.add(cdf.get(i - 1) + weights.get(i));
			}
		}

		for (int i = 0; i < linkIds.size(); i++) {
			cdf.set(i, cdf.get(i) / cdf.get(linkIds.size() - 1));
		}

		if (cdf.size() == 0) {
			throw new IllegalStateException("Did not find any links that have any spatial capacity for parking ");
		}
	}

	@Override
	public boolean hasNext() {
		return count < fleetSize;
	}

	@Override
	public AVVehicle next() {
		count++;

		// Select link
		double selector = random.nextDouble();
		int selectorIndex = 0;

		while (cdf.get(selectorIndex) < selector) {
			selectorIndex++;
		}

		// Track space consumption
		long currentNumberOfVehicles = numberOfVehicles.get(selectorIndex) + 1;
		numberOfVehicles.set(selectorIndex, currentNumberOfVehicles);

		// Reweight sampling if necessary
		if (currentNumberOfVehicles == maximumNumberOfVehicles.get(selectorIndex)) {
			numberOfVehicles.remove(selectorIndex);
			maximumNumberOfVehicles.remove(selectorIndex);
			
			linkIds.remove(selectorIndex);
			weights.remove(selectorIndex);
			
			updateCdf();
		}

		if (linkIds.size() == 0) {
			throw new IllegalStateException("Trying to distribute more vehicles than there is space in the network.");
		}

		// Generate vehicle
		Id<Vehicle> vehicleId = Id.create(String.format("av_%d", count), Vehicle.class);

		Id<Link> startLinkId = linkIds.get(selectorIndex);
		Link startLink = network.getLinks().get(startLinkId);
		
		return new LIMAVehicle(vehicleId, startLink, 1.0, 0.0, Double.POSITIVE_INFINITY);
	}

	@Singleton
	public static class Factory implements AVGeneratorFactory {
		private final List<Id<Link>> linkIds;
		private final List<Double> weights;
		private final List<Long> maximumNumberOfVehicles;
		private final Network network;

		@Inject
		public Factory(Population population, @Named(AVModule.AV_MODE) Network network,
				NearestInteractionLinkData nearestLinkData, @Named("parking") SpatialCapacity spatialCapacity) {
			this.linkIds = new ArrayList<>(network.getLinks().size());
			this.maximumNumberOfVehicles = new ArrayList<>(network.getLinks().size());
			
			this.network = network;

			for (Link link : network.getLinks().values()) {
				if (!nearestLinkData.isInteractionAllowed(link.getId())) {
					//continue; I think we should not filter by that criterion ...
				}

				if (spatialCapacity.hasAnySpatialCapacity(link) && spatialCapacity.getSpatialCapacity(link, 0.0) > 0) {
					linkIds.add(link.getId());
					maximumNumberOfVehicles.add(spatialCapacity.getSpatialCapacity(link, 0.0));
				}
			}

			this.weights = new ArrayList<>(Collections.nCopies(linkIds.size(), 0.0));

			for (Person person : population.getPersons().values()) {
				Plan plan = person.getSelectedPlan();

				if (plan.getPlanElements().size() > 0) {
					Activity firstActivity = (Activity) plan.getPlanElements().get(0);
					int linkIndex = linkIds.indexOf(firstActivity.getLinkId());

					if (linkIndex > -1) {
						weights.set(linkIndex, weights.get(linkIndex) + 1);
					}
				}
			}
		}

		@Override
		public AVGenerator createGenerator(AVGeneratorConfig generatorConfig) {
			Random random = MatsimRandom.getLocalInstance();

			return new SpatiallyConstrainedPopulationDensityGenerator(linkIds, weights, maximumNumberOfVehicles,
					(int) generatorConfig.getNumberOfVehicles(), random, network);
		}

	}
}
