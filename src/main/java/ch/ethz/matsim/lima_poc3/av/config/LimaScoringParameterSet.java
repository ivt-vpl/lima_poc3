package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class LimaScoringParameterSet extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "scoring";

	public final static String SUBPOPUALTION = "subpopulation";

	public final static String MARGINAL_UTILITY_OF_FEEDER_WAITING_TIME = "marginalUtilityOfFeederWaitingTime";
	public final static String MARGINAL_UTILITY_OF_DIRECT_WAITING_TIME = "marginalUtilityOfDirectWaitingTime";

	public final static String DIRECT_PRICE_FACTOR = "directPriceFactor";
	public final static String FEEDER_PRICE_FACTOR = "feederPriceFactor";

	private String subpopulation = null;

	private double marignalUtilityOfFeederWaitingTime;
	private double marginalUtilityOfDirectWaitingTime;

	private double directPriceFactor;
	private double feederPriceFactor;

	public LimaScoringParameterSet() {
		super(GROUP_NAME);
	}

	@StringSetter(SUBPOPUALTION)
	public void setSubpopulation(String subpopulation) {
		this.subpopulation = subpopulation;
	}

	@StringGetter(SUBPOPUALTION)
	public String getSubpopulation() {
		return subpopulation;
	}

	@StringSetter(MARGINAL_UTILITY_OF_DIRECT_WAITING_TIME)
	public void setMarginalUtilityOfDirectWaitingTime(double marginalUtilityOfDirectWaitingTime) {
		this.marginalUtilityOfDirectWaitingTime = marginalUtilityOfDirectWaitingTime;
	}

	@StringGetter(MARGINAL_UTILITY_OF_DIRECT_WAITING_TIME)
	public double getMarginalUtilityOfDirectWaitingTime() {
		return marginalUtilityOfDirectWaitingTime;
	}

	@StringSetter(MARGINAL_UTILITY_OF_FEEDER_WAITING_TIME)
	public void setMarginalUtilityOfFeederWaitingTime(double marignalUtilityOfFeederWaitingTime) {
		this.marignalUtilityOfFeederWaitingTime = marignalUtilityOfFeederWaitingTime;
	}

	@StringGetter(MARGINAL_UTILITY_OF_FEEDER_WAITING_TIME)
	public double getMarginalUtilityOfFeederWaitingTime() {
		return marignalUtilityOfFeederWaitingTime;
	}

	@StringSetter(DIRECT_PRICE_FACTOR)
	public void setDirectPriceFactor(double directPriceFactor) {
		this.directPriceFactor = directPriceFactor;
	}

	@StringGetter(DIRECT_PRICE_FACTOR)
	public double getDirectPriceFactor() {
		return directPriceFactor;
	}

	@StringSetter(FEEDER_PRICE_FACTOR)
	public void setFeederPriceFactor(double feederPriceFactor) {
		this.feederPriceFactor = feederPriceFactor;
	}

	@StringGetter(FEEDER_PRICE_FACTOR)
	public double getFeederPriceFactor() {
		return feederPriceFactor;
	}
}
