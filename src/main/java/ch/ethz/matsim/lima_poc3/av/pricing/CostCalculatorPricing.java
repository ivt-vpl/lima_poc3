package ch.ethz.matsim.lima_poc3.av.pricing;

public class CostCalculatorPricing implements AVPricing {
	private final AVPricingListener listener;

	public CostCalculatorPricing(AVPricingListener listener) {
		this.listener = listener;
	}

	@Override
	public boolean useCostCalculator() {
		return true;
	}

	@Override
	public double computeCost(double distance_km, double priceFactor) {
		return listener.getPricePerKm() * distance_km * priceFactor;
	}
}
