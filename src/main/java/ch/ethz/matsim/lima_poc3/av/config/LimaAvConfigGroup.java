package ch.ethz.matsim.lima_poc3.av.config;

import java.util.HashMap;
import java.util.Map;

import org.matsim.core.config.ConfigGroup;
import org.matsim.core.config.ReflectiveConfigGroup;

public class LimaAvConfigGroup extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "lima_av";

	public enum InteractionTimeEstimator {
		Constant, Lima
	};

	public enum SpatialCapacity {
		Constant, Lima
	}

	public enum InteractionPointFinder {
		Nearest, CapacityAwareWithinRadius
	}

	public final static String INTERACTION_TIME_ESTIMATOR = "interactionTimeEstimator";
	public final static String SPATIAL_CAPACITY = "spatialCapacity";
	public final static String FLEET_SIZE = "fleetSize";
	public final static String USE_AV = "useAv";
	public final static String INTERACTION_LINK_ATTRIBUTE = "interactionLinkAttribute";
	public final static String FEEDER_LINKS_STOP_ATTRIBUTE = "feederLinksStopAttribute";
	public final static String INTERACTION_POINT_FINDER = "interactionPointFinder";
	public final static String FILTER_FEEDER_STATIONS_BY_LINE = "filterFeederStationsByLine";
	public final static String ROUTING_TRANSFER_UTILITY_BIKE = "routingTransferUtilityBike";
	public final static String ROUTING_TRANSFER_UTILITY_FEEDER = "routingTransferUtilityFeeder";
	public final static String ACCESS_EGRESS_SIGMA = "accessEgressSigma";

	private InteractionTimeEstimator waitingTimeType = InteractionTimeEstimator.Constant;
	private SpatialCapacity spatialCapacity = SpatialCapacity.Constant;
	private InteractionPointFinder interactionPointFinder = InteractionPointFinder.CapacityAwareWithinRadius;

	private long fleetSize = 1000;
	private boolean useAv = false;
	private boolean filterFeederStationsByLine = false;

	private String interactionLinkAttribute = "isAvailableForAVInteraction";
	private String feederLinksStopAttribute = "avFeederLinkIds";

	private double routingTransferUtilityBike = 0.0;
	private double routingTransferUtilityFeeder = 0.0;

	public double accessEgressSigma = 0.0;

	public LimaAvConfigGroup() {
		super(GROUP_NAME);

		// addParameterSet(createParameterSet(ScoringParameterSet.GROUP_NAME));
		addParameterSet(createParameterSet(ConstantInteractionTimeParameterSet.GROUP_NAME));
		addParameterSet(createParameterSet(LimaInteractionTimeParameterSet.GROUP_NAME));
		addParameterSet(createParameterSet(ConstantSpatialCapacityParameterSet.GROUP_NAME));
		addParameterSet(createParameterSet(LimaSpatialCapacityParameterSet.GROUP_NAME));
		addParameterSet(createParameterSet(RadiusCapacityInteractionFinderParameterSet.GROUP_NAME));
		addParameterSet(createParameterSet(PricingParameterSet.GROUP_NAME));
	}

	@StringGetter(INTERACTION_POINT_FINDER)
	public InteractionPointFinder getInteractionPointFinder() {
		return interactionPointFinder;
	}

	@StringSetter(INTERACTION_POINT_FINDER)
	public void setInteractionPointFinder(InteractionPointFinder interactionPointFinder) {
		this.interactionPointFinder = interactionPointFinder;
	}

	@StringGetter(INTERACTION_TIME_ESTIMATOR)
	public InteractionTimeEstimator getWaitingTimeType() {
		return waitingTimeType;
	}

	@StringSetter(INTERACTION_TIME_ESTIMATOR)
	public void setWaitingTimeType(InteractionTimeEstimator waitingTimeType) {
		this.waitingTimeType = waitingTimeType;
	}

	@StringGetter(SPATIAL_CAPACITY)
	public SpatialCapacity getSpatialCapacity() {
		return spatialCapacity;
	}

	@StringSetter(SPATIAL_CAPACITY)
	public void setSpatialCapacity(SpatialCapacity spatialCapacity) {
		this.spatialCapacity = spatialCapacity;
	}

	@StringGetter(FLEET_SIZE)
	public long getFleetSize() {
		return fleetSize;
	}

	@StringSetter(FLEET_SIZE)
	public void setFleetSize(long fleetSize) {
		this.fleetSize = fleetSize;
	}

	@StringGetter(USE_AV)
	public boolean isUseAv() {
		return useAv;
	}

	@StringSetter(USE_AV)
	public void setUseAv(boolean useAv) {
		this.useAv = useAv;
	}

	@StringGetter(FILTER_FEEDER_STATIONS_BY_LINE)
	public boolean getFilterFeederStationsByLine() {
		return filterFeederStationsByLine;
	}

	@StringSetter(FILTER_FEEDER_STATIONS_BY_LINE)
	public void setFilterFeederStationsByLine(boolean filterFeederStationsByLine) {
		this.filterFeederStationsByLine = filterFeederStationsByLine;
	}

	@StringGetter(INTERACTION_LINK_ATTRIBUTE)
	public String getInteractionLinkAttribute() {
		return interactionLinkAttribute;
	}

	@StringSetter(INTERACTION_LINK_ATTRIBUTE)
	public void setInteractionLinkAttribute(String interactionLinkAttribute) {
		this.interactionLinkAttribute = interactionLinkAttribute;
	}

	@StringGetter(FEEDER_LINKS_STOP_ATTRIBUTE)
	public String getFeederLinksStopAttribute() {
		return feederLinksStopAttribute;
	}

	@StringSetter(FEEDER_LINKS_STOP_ATTRIBUTE)
	public void setFeederLinksStopAttribute(String feederLinksStopAttribute) {
		this.feederLinksStopAttribute = feederLinksStopAttribute;
	}

	@Override
	public ConfigGroup createParameterSet(final String type) {
		if (type.equals(LimaScoringParameterSet.GROUP_NAME)) {
			return new LimaScoringParameterSet();
		} else if (type.equals(ConstantInteractionTimeParameterSet.GROUP_NAME)) {
			return new ConstantInteractionTimeParameterSet();
		} else if (type.equals(LimaInteractionTimeParameterSet.GROUP_NAME)) {
			return new LimaInteractionTimeParameterSet();
		} else if (type.equals(ConstantSpatialCapacityParameterSet.GROUP_NAME)) {
			return new ConstantSpatialCapacityParameterSet();
		} else if (type.equals(LimaSpatialCapacityParameterSet.GROUP_NAME)) {
			return new LimaSpatialCapacityParameterSet();
		} else if (type.equals(RadiusCapacityInteractionFinderParameterSet.GROUP_NAME)) {
			return new RadiusCapacityInteractionFinderParameterSet();
		} else if (type.equals(PricingParameterSet.GROUP_NAME)) {
			return new PricingParameterSet();
		} else {
			throw new IllegalStateException("Unknown parameter set: " + type);
		}
	}

	public void addScoringParameterForSubpopulation(LimaScoringParameterSet parameterSet) {
		addParameterSet(parameterSet);
	}

	public LimaScoringParameterSet getScoringParametersForSubpopulation(String subpopulation) {
		for (ConfigGroup set : this.getParameterSets(LimaScoringParameterSet.GROUP_NAME)) {
			LimaScoringParameterSet scoringSet = (LimaScoringParameterSet) set;

			if ((subpopulation == null || subpopulation.equals("default")) && scoringSet.getSubpopulation() == null) {
				return scoringSet;
			} else if (subpopulation.equals(scoringSet.getSubpopulation())) {
				return scoringSet;
			}
		}

		throw new IllegalStateException("No AV scoring parameters for subpopulation: " + subpopulation);
	}

	public Map<String, LimaScoringParameterSet> getScoringParametersBySubpopulation() {
		Map<String, LimaScoringParameterSet> map = new HashMap<>();

		for (ConfigGroup set : this.getParameterSets(LimaScoringParameterSet.GROUP_NAME)) {
			LimaScoringParameterSet scoringSet = (LimaScoringParameterSet) set;
			map.put(scoringSet.getSubpopulation(), scoringSet);
		}

		return map;
	}

	public LimaInteractionTimeParameterSet getLimaInteractionTimeConfig() {
		return (LimaInteractionTimeParameterSet) getParameterSets().get(LimaInteractionTimeParameterSet.GROUP_NAME)
				.iterator().next();
	}

	public ConstantInteractionTimeParameterSet getConstantInteractionTimeConfig() {
		return (ConstantInteractionTimeParameterSet) getParameterSets()
				.get(ConstantInteractionTimeParameterSet.GROUP_NAME).iterator().next();
	}

	public LimaSpatialCapacityParameterSet getLimaSpatialCapacityConfig() {
		return (LimaSpatialCapacityParameterSet) getParameterSets().get(LimaSpatialCapacityParameterSet.GROUP_NAME)
				.iterator().next();
	}

	public ConstantSpatialCapacityParameterSet getConstantSpatialCapacityConfig() {
		return (ConstantSpatialCapacityParameterSet) getParameterSets()
				.get(ConstantSpatialCapacityParameterSet.GROUP_NAME).iterator().next();
	}

	public RadiusCapacityInteractionFinderParameterSet getRadiusCapacityInteractionFinderConfig() {
		return (RadiusCapacityInteractionFinderParameterSet) getParameterSets()
				.get(RadiusCapacityInteractionFinderParameterSet.GROUP_NAME).iterator().next();
	}

	public PricingParameterSet getPricingConfig() {
		return (PricingParameterSet) getParameterSets().get(PricingParameterSet.GROUP_NAME).iterator().next();
	}

	@StringGetter(ROUTING_TRANSFER_UTILITY_BIKE)
	public double getRoutingTransferUtilityBike() {
		return routingTransferUtilityBike;
	}

	@StringSetter(ROUTING_TRANSFER_UTILITY_BIKE)
	public void setRoutingTransferUtilityBike(double routingTransferUtilityBike) {
		this.routingTransferUtilityBike = routingTransferUtilityBike;
	}

	@StringGetter(ROUTING_TRANSFER_UTILITY_FEEDER)
	public double getRoutingTransferUtilityFeeder() {
		return routingTransferUtilityFeeder;
	}

	@StringSetter(ROUTING_TRANSFER_UTILITY_FEEDER)
	public void setRoutingTransferUtilityFeeder(double routingTransferUtilityFeeder) {
		this.routingTransferUtilityFeeder = routingTransferUtilityFeeder;
	}

	@StringGetter(ACCESS_EGRESS_SIGMA)
	public double getAccessEgressSigma() {
		return accessEgressSigma;
	}

	@StringSetter(ACCESS_EGRESS_SIGMA)
	public void setAccessEgressSigma(double accessEgressSigma) {
		this.accessEgressSigma = accessEgressSigma;
	}
}
