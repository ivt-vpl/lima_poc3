package ch.ethz.matsim.lima_poc3.av.routing;

import java.util.List;

import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.facilities.Facility;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.replanning.AVOperatorChoiceStrategy;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.IntermodalInteractionFacilityFinder;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.AVInteractionTime;

public class AVFeederRoutingModule implements RoutingModule {
	public static final String AV_FEEDER_MODE = "av_feeder";

	private final ExtendedAVRoutingModule delegate;

	@Inject
	public AVFeederRoutingModule(AVOperatorChoiceStrategy choiceStrategy, AVRouteFactory routeFactory,
			@Named("car_av") RoutingModule carRoutingModule,
			@Named(TransportMode.walk) RoutingModule walkRoutingModule, AVInteractionTime interactionTime,
			IntermodalInteractionFacilityFinder facilityFinder) {
		this.delegate = new ExtendedAVRoutingModule(choiceStrategy, routeFactory, carRoutingModule, walkRoutingModule,
				interactionTime, facilityFinder);
	}

	@Override
	public List<? extends PlanElement> calcRoute(Facility<?> fromFacility, Facility<?> toFacility, double departureTime,
			Person person) {
		return delegate.calcRoute(fromFacility, toFacility, departureTime, person, AV_FEEDER_MODE);
	}

	@Override
	public StageActivityTypes getStageActivityTypes() {
		return delegate.getStageActivityTypes();
	}
}
