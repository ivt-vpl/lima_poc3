package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class ConstantSpatialCapacityParameterSet extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "SpatialCapacity:Constant";

	public static final String PARKING_CAPACITY = "parkingCapacity";
	public static final String PICKUP_DROPOFF_CAPACITY = "pickupDropoffCapacity";

	private long parkingCapacity = 1000;
	private long pickupDropoffCapacity = 1000;

	public ConstantSpatialCapacityParameterSet() {
		super(GROUP_NAME);
	}

	@StringGetter(PARKING_CAPACITY)
	public long getParkingCapacity() {
		return parkingCapacity;
	}

	@StringSetter(PARKING_CAPACITY)
	public void setParkingCapacity(long parkingCapacity) {
		this.parkingCapacity = parkingCapacity;
	}

	@StringGetter(PICKUP_DROPOFF_CAPACITY)
	public long getPickupDropoffCapacity() {
		return pickupDropoffCapacity;
	}

	@StringSetter(PICKUP_DROPOFF_CAPACITY)
	public void setPickupDropoffCapacity(long pickupDropoffCapacity) {
		this.pickupDropoffCapacity = pickupDropoffCapacity;
	}
}
