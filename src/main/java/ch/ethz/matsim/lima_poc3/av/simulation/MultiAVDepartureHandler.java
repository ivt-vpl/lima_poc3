package ch.ethz.matsim.lima_poc3.av.simulation;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.contrib.dvrp.passenger.PassengerEngine;
import org.matsim.core.mobsim.framework.MobsimAgent;
import org.matsim.core.mobsim.qsim.interfaces.DepartureHandler;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MultiAVDepartureHandler implements DepartureHandler {
	static private final String DIRECT_AV_MODE = "av_direct";
	static private final String FEEDER_AV_MODE = "av_feeder";

	private final PassengerEngine passengerEngine;

	@Inject
	public MultiAVDepartureHandler(PassengerEngine passengerEngine) {
		this.passengerEngine = passengerEngine;
	}

	@Override
	public boolean handleDeparture(double now, MobsimAgent agent, Id<Link> linkId) {
		String mode = agent.getMode();

		switch (mode) {
		case DIRECT_AV_MODE:
		case FEEDER_AV_MODE:
			MultiAVAgentDecorator decoratedAgent = new MultiAVAgentDecorator(agent);
			decoratedAgent.pretendAV();

			return passengerEngine.handleDeparture(now, decoratedAgent, linkId);
		}

		return false;
	}
}
