package ch.ethz.matsim.lima_poc3.av.routing;

import java.util.LinkedList;
import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.population.Activity;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.population.PopulationUtils;
import org.matsim.core.router.RoutingModule;
import org.matsim.core.router.StageActivityTypes;
import org.matsim.core.router.StageActivityTypesImpl;
import org.matsim.core.utils.geometry.CoordUtils;
import org.matsim.facilities.Facility;

import ch.ethz.matsim.av.data.AVOperator;
import ch.ethz.matsim.av.replanning.AVOperatorChoiceStrategy;
import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.AVInteractionFacilityFinder;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.AVInteractionTime;

/**
 * Attention, this is not 100% clean:
 * <ul>
 * <li>We use the car router here, which may be wrong</li>
 * <li>We should even use operator-specific routers</li>
 * <li>In that case we need to check how the facilities here relate to the
 * actual pickup and dropoff links.</li>
 * </ul>
 */
class ExtendedAVRoutingModule {
	public static final String AV_INTERACTION_ACTIVITY_TYPE = "av interaction";

	private final AVOperatorChoiceStrategy choiceStrategy;
	private final AVRouteFactory routeFactory;

	private final RoutingModule carRoutingModule;
	private final RoutingModule walkRoutingModule;

	private final AVInteractionTime interactionTime;
	private final AVInteractionFacilityFinder facilityFinder;

	ExtendedAVRoutingModule(AVOperatorChoiceStrategy choiceStrategy, AVRouteFactory routeFactory,
			RoutingModule carRoutingModule, RoutingModule walkRoutingModule, AVInteractionTime interactionTime,
			AVInteractionFacilityFinder facilityFinder) {
		this.choiceStrategy = choiceStrategy;
		this.routeFactory = routeFactory;
		this.carRoutingModule = carRoutingModule;
		this.walkRoutingModule = walkRoutingModule;
		this.interactionTime = interactionTime;
		this.facilityFinder = facilityFinder;
	}

	protected List<? extends PlanElement> calcRoute(Facility<?> fromFacility, Facility<?> toFacility,
			Facility<?> pickupFacility, Facility<?> dropoffFacility, double departureTime, Person person, String mode) {
		List<PlanElement> elements = new LinkedList<>();
		double initialDepartureTime = departureTime;

		if (!fromFacility.getLinkId().equals(pickupFacility.getLinkId())) {
			Leg walkLeg = (Leg) walkRoutingModule.calcRoute(fromFacility, pickupFacility, departureTime, person).get(0);
			walkLeg.setMode(TransportMode.access_walk);
			elements.add(walkLeg);

			Activity interactionActivity = PopulationUtils.getFactory()
					.createActivityFromLinkId(AV_INTERACTION_ACTIVITY_TYPE, pickupFacility.getLinkId());
			interactionActivity.setMaximumDuration(0.0);
			elements.add(interactionActivity);

			departureTime = walkLeg.getDepartureTime() + walkLeg.getTravelTime();
		}

		Leg carLeg = (Leg) carRoutingModule.calcRoute(pickupFacility, dropoffFacility, departureTime, person).get(0);

		Id<AVOperator> operator = choiceStrategy.chooseRandomOperator();
		AVRoute avRoute = routeFactory.createRoute(pickupFacility.getLinkId(), dropoffFacility.getLinkId(), operator);
		avRoute.setDistance(carLeg.getRoute().getDistance());
		avRoute.setTravelTime(carLeg.getTravelTime());

		ExtendedAVRoute extendedAvRoute = new ExtendedAVRoute(avRoute);

		double pickupTime = interactionTime.getPickupTime(pickupFacility, departureTime,
				departureTime - initialDepartureTime);
		extendedAvRoute.setPickupTime(pickupTime);

		double dropoffTime = interactionTime.getDropoffTime(dropoffFacility,
				departureTime + pickupTime + carLeg.getTravelTime());
		extendedAvRoute.setDropoffTime(dropoffTime);

		Leg leg = PopulationUtils.createLeg(mode);
		leg.setDepartureTime(departureTime);
		leg.setTravelTime(extendedAvRoute.getJourneyTime());
		leg.setRoute(extendedAvRoute);
		elements.add(leg);

		departureTime = leg.getDepartureTime() + leg.getTravelTime();

		if (!toFacility.getLinkId().equals(dropoffFacility.getLinkId())) {
			Activity interactionActivity = PopulationUtils.getFactory()
					.createActivityFromLinkId(AV_INTERACTION_ACTIVITY_TYPE, dropoffFacility.getLinkId());
			interactionActivity.setMaximumDuration(0.0);
			elements.add(interactionActivity);

			Leg walkLeg = (Leg) walkRoutingModule.calcRoute(dropoffFacility, toFacility, departureTime, person).get(0);
			walkLeg.setMode(TransportMode.egress_walk);
			elements.add(walkLeg);

			departureTime += walkLeg.getTravelTime();
		}

		return elements;
	}

	public StageActivityTypes getStageActivityTypes() {
		return new StageActivityTypesImpl(AV_INTERACTION_ACTIVITY_TYPE);
	}

	public List<? extends PlanElement> calcRoute(Facility<?> fromFacility, Facility<?> toFacility, double departureTime,
			Person person, String mode) {
		Facility<?> pickupFacility = facilityFinder.findPickupFacility(fromFacility, departureTime);
		Facility<?> dropoffFacility = facilityFinder.findDropoffFacility(toFacility, departureTime);

		return calcRoute(fromFacility, toFacility, pickupFacility, dropoffFacility, departureTime, person, mode);
	}
}
