package ch.ethz.matsim.lima_poc3.av.routing;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Route;
import org.matsim.core.population.routes.RouteFactory;

import ch.ethz.matsim.av.routing.AVRoute;
import ch.ethz.matsim.av.routing.AVRouteFactory;

public class ExtendedAVRouteFactory implements RouteFactory {
	private final AVRouteFactory delegate = new AVRouteFactory();
	
	@Override
	public Route createRoute(Id<Link> startLinkId, Id<Link> endLinkId) {
		return new ExtendedAVRoute(startLinkId, endLinkId, null);
	}

	@Override
	public String getCreatedRouteType() {
		return "av";
	}
}
