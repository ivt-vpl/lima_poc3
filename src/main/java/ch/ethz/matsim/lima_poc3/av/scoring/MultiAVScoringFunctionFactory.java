package ch.ethz.matsim.lima_poc3.av.scoring;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.scoring.ScoringFunction;
import org.matsim.core.scoring.ScoringFunctionFactory;
import org.matsim.core.scoring.SumScoringFunction;
import org.matsim.core.scoring.functions.CharyparNagelScoringFunctionFactory;
import org.matsim.core.scoring.functions.ScoringParametersForPerson;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import ch.ethz.matsim.lima_poc3.av.pricing.AVPricing;

@Singleton
public class MultiAVScoringFunctionFactory implements ScoringFunctionFactory {
	final private ScoringFunctionFactory standardFactory;
	final private ScoringParametersForPerson params;
	final private LIMAScoringParameterForPerson limaParams;
	final private AVPricing pricing;

	@Inject
	public MultiAVScoringFunctionFactory(Scenario scenario, ScoringParametersForPerson scoringParametersForPerson,
			LIMAScoringParameterForPerson limaParams, AVPricing pricing) {
		this.params = scoringParametersForPerson;
		this.limaParams = limaParams;
		this.pricing = pricing;

		standardFactory = new CharyparNagelScoringFunctionFactory(scenario);
	}

	@Override
	public ScoringFunction createNewScoringFunction(Person person) {
		SumScoringFunction sf = (SumScoringFunction) standardFactory.createNewScoringFunction(person);
		sf.addScoringFunction(new MultiAVScoring(params.getScoringParameters(person),
				limaParams.getScoringParameters(person), pricing));

		return sf;
	}
}
