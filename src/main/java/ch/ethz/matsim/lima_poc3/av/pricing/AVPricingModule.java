package ch.ethz.matsim.lima_poc3.av.pricing;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.OutputDirectoryHierarchy;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av_cost_calculator.CostCalculator;
import ch.ethz.matsim.av_cost_calculator.CostCalculatorExecutor;
import ch.ethz.matsim.av_cost_calculator.run.AVValidator;
import ch.ethz.matsim.av_cost_calculator.run.IdAVValidator;
import ch.ethz.matsim.av_cost_calculator.run.PricingAnalysisHandler;
import ch.ethz.matsim.av_cost_calculator.run.SingleOccupancyAnalysisHandler;
import ch.ethz.matsim.lima_poc3.av.config.PricingParameterSet;
import ch.ethz.matsim.lima_poc3.av.config.PricingParameterSet.CostCalculatorMode;

public class AVPricingModule extends AbstractModule {
	private final PricingParameterSet pricingConfig;

	public AVPricingModule(PricingParameterSet pricingConfig) {
		this.pricingConfig = pricingConfig;
	}

	@Override
	public void install() {
		CostCalculatorMode mode = pricingConfig.getCostCalculatorMode();

		if (mode.equals(CostCalculatorMode.ADAPTIVE)) {
			bind(AVPricing.class).to(CostCalculatorPricing.class);
		} else {
			bind(AVPricing.class).to(DefaultAVPricing.class);
		}

		if (!mode.equals(CostCalculatorMode.INACTIVE)) {
			addEventHandlerBinding().to(PricingAnalysisHandler.class);
			addControlerListenerBinding().to(AVPricingListener.class);
		}
	}

	@Provides
	@Singleton
	public AVPricingListener providePricingListener(PricingAnalysisHandler handler, CostCalculatorExecutor executor,
			OutputDirectoryHierarchy output) throws IOException {
		if (!Double.isFinite(pricingConfig.getScenarioScalingFactor())) {
			throw new IllegalStateException("Scenario scaling factor is not finite.");
		}

		return new AVPricingListener("Solo", pricingConfig.getScenarioScalingFactor(), pricingConfig.getHorizon(),
				pricingConfig.getInitialPricePerKm(), handler, executor, output);
	}

	@Provides
	@Singleton
	public PricingAnalysisHandler providePricingAnalysisHandler(@Named(AVModule.AV_MODE) Network network) {
		double totalTime = 24.0 * 3600.0;
		AVValidator validator = new IdAVValidator("av");
		return new SingleOccupancyAnalysisHandler(network, validator, totalTime);
	}

	@Provides
	@Singleton
	public CostCalculatorExecutor provideCostCalculatorExecutor(OutputDirectoryHierarchy outputHierarchy) {
		URL sourceURL = CostCalculator.class.getClassLoader().getResource("ch/ethz/matsim/av_cost_calculator/");

		File workingDirectory = new File(outputHierarchy.getTempPath() + "/av_cost_calculator");
		workingDirectory.mkdirs();

		return new CostCalculatorExecutor(workingDirectory, sourceURL);
	}

	@Provides
	@Singleton
	public CostCalculatorPricing provideCostCalculatorPricing(AVPricingListener listener) {
		return new CostCalculatorPricing(listener);
	}

	@Provides
	@Singleton
	public DefaultAVPricing provideDefaultAVPricing() {
		return new DefaultAVPricing();
	}
}
