package ch.ethz.matsim.lima_poc3.av.routing.spatial;

import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.facilities.Facility;

import com.google.inject.Inject;

public class NearestInteractionFacilityFinder implements AVInteractionFacilityFinder {
	private final NearestInteractionLinkData data;

	@Inject
	public NearestInteractionFacilityFinder(NearestInteractionLinkData data) {
		this.data = data;
	}

	private Facility<?> findFacility(Facility<?> initialFacility) {
		if (data.isInteractionAllowed(initialFacility.getLinkId())) {
			return initialFacility;
		}

		return new LinkWrapperFacility(data.getClosestInteractionLink(initialFacility.getCoord()));
	}

	@Override
	public Facility<?> findPickupFacility(Facility<?> originFacility, double time) {
		return findFacility(originFacility);
	}

	@Override
	public Facility<?> findDropoffFacility(Facility<?> destinationFacility, double time) {
		return findFacility(destinationFacility);
	}
}
