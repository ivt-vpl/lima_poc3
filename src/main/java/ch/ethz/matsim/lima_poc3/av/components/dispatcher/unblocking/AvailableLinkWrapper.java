package ch.ethz.matsim.lima_poc3.av.components.dispatcher.unblocking;

import org.matsim.api.core.v01.network.Link;

public class AvailableLinkWrapper {
	final Link link;
	long numberOfAvailableSpots;

	public AvailableLinkWrapper(Link link, long numberOfAvailableSpots) {
		this.numberOfAvailableSpots = numberOfAvailableSpots;
		this.link = link;
	}
}