package ch.ethz.matsim.lima_poc3.av.pricing;

public interface AVPricing {
	public boolean useCostCalculator();
	public double computeCost(double distance_km, double priceFactor);
}
