package ch.ethz.matsim.lima_poc3.av.routing.spatial;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.gbl.MatsimRandom;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.facilities.Facility;

import com.google.inject.Inject;
import com.google.inject.name.Named;

import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;

public class CapacityAwareRadiusInteractionFacilityFinder implements AVInteractionFacilityFinder {
	private final NearestInteractionLinkData data;
	private final SpatialCapacity capacity;
	private final Random random;
	private final double radius;

	@Inject
	public CapacityAwareRadiusInteractionFacilityFinder(NearestInteractionLinkData data,
			@Named("pickupDropoff") SpatialCapacity capacity, LimaAvConfigGroup limaConfig) {
		this.data = data;
		this.capacity = capacity;
		this.radius = limaConfig.getRadiusCapacityInteractionFinderConfig().getSearchRadius();
		this.random = MatsimRandom.getLocalInstance();
	}

	private Link findLink(Coord coord, double time) {
		List<Link> candidates = new ArrayList<>(data.getInteractionLinks(coord, radius));

		if (candidates.size() == 0) {
			return data.getClosestInteractionLink(coord);
		}

		List<Double> weights = candidates.stream()
				.map(link -> (double) Math.max(1, capacity.getSpatialCapacity(link, time)))
				.collect(Collectors.toList());

		List<Double> cumulative = new ArrayList<>(weights.size());
		cumulative.add(weights.get(0));

		for (int i = 1; i < weights.size(); i++) {
			cumulative.add(cumulative.get(i - 1) + weights.get(i));
		}

		double maximum = cumulative.get(cumulative.size() - 1);

		for (int i = 0; i < cumulative.size(); i++) {
			cumulative.set(i, cumulative.get(i) / maximum);
		}

		double r = random.nextDouble();

		for (int i = 0; i < cumulative.size(); i++) {
			if (r <= cumulative.get(i)) {
				return candidates.get(i);
			}
		}

		throw new IllegalStateException();
	}

	private Facility<?> findFacility(Facility<?> initialFacility, double time) {
		return new LinkWrapperFacility(findLink(initialFacility.getCoord(), time));
	}

	@Override
	public Facility<?> findPickupFacility(Facility<?> originFacility, double time) {
		return findFacility(originFacility, time);
	}

	@Override
	public Facility<?> findDropoffFacility(Facility<?> destinationFacility, double time) {
		return findFacility(destinationFacility, time);
	}
}
