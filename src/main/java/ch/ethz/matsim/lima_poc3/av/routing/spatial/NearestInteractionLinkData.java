package ch.ethz.matsim.lima_poc3.av.routing.spatial;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.utils.collections.QuadTree;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;

@Singleton
public class NearestInteractionLinkData {
	private final Set<Id<Link>> allowedLinkIds = new HashSet<>();
	private final QuadTree<Link> allowedLinkIndex;

	@Inject
	public NearestInteractionLinkData(@Named(AVModule.AV_MODE) Network network, LimaAvConfigGroup limaConfig,
			@Named("pickupDropoff") SpatialCapacity capacity) {
		String attributeName = limaConfig.getInteractionLinkAttribute();

		double[] dimensions = NetworkUtils.getBoundingBox(network.getNodes().values());
		this.allowedLinkIndex = new QuadTree<>(dimensions[0], dimensions[1], dimensions[2], dimensions[3]);

		for (Link link : network.getLinks().values()) {
			Boolean attribute = (Boolean) link.getAttributes().getAttribute(attributeName);

			if (/* attribute == null || */ attribute != null && attribute == true
					&& capacity.hasAnySpatialCapacity(link)) {
				allowedLinkIndex.put(link.getCoord().getX(), link.getCoord().getY(), link);
				allowedLinkIds.add(link.getId());

				if (!link.getAllowedModes().contains(AVModule.AV_MODE)) {
					throw new IllegalStateException("Link " + link.getId().toString()
							+ " is avaialble for AV interaction, but does not have AV mode.");
				}
			}
		}

		if (allowedLinkIds.size() == 0) {
			throw new IllegalStateException("No links found that have " + attributeName + "==true .");
		}
	}

	public boolean isInteractionAllowed(Id<Link> linkId) {
		return allowedLinkIds.contains(linkId);
	}

	public Link getClosestInteractionLink(Coord coord) {
		return allowedLinkIndex.getClosest(coord.getX(), coord.getY());
	}

	public Collection<Link> getInteractionLinks(Coord coord, double radius) {
		return allowedLinkIndex.getDisk(coord.getX(), coord.getY(), radius);
	}
}
