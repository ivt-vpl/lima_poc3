package ch.ethz.matsim.lima_poc3.av.space;

import java.util.Collection;
import java.util.Collections;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.config.Config;
import org.matsim.core.mobsim.framework.listeners.MobsimListener;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

public class LIMAOccupancyQSimPlugin extends AbstractQSimPlugin {
	public LIMAOccupancyQSimPlugin(Config config) {
		super(config);
	}

	public Collection<? extends Module> modules() {
		return Collections.singleton(new AbstractModule() {
			@Override
			protected void configure() {
			}

			@Provides
			@Singleton
			LIMAOccupancyHandler provideHandler(@Named("parking") SpatialCapacity parkingCapacity,
					@Named("pickupDropoff") SpatialCapacity pickupDropoffCapacity, Network network,
					EventsManager eventsManager) {
				return new LIMAOccupancyHandler(parkingCapacity, pickupDropoffCapacity, network, eventsManager);
			}
		});
	}

	public Collection<Class<? extends MobsimListener>> listeners() {
		return Collections.singleton(LIMAOccupancyHandler.class);
	}
}
