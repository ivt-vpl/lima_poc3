package ch.ethz.matsim.lima_poc3.av.analysis.service;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.vehicles.Vehicle;

import ch.ethz.matsim.av.data.AVOperator;

public class AVServiceItem {
	Id<Person> personId;
	int tripIndex;

	Link originLink;
	Link destinationLink;
	double departureTime;

	double waitingTime;
	double inVehicleTime;

	double distance = 0.0;
	double chargedDistance = 0.0;

	Id<AVOperator> operatorId;
	Id<Vehicle> vehicleId;
	
	String mode;
}
