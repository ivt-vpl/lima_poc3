package ch.ethz.matsim.lima_poc3.av.routing.spatial;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;

@Singleton
public class AccessEgressLinkData {
	private final static Logger logger = Logger.getLogger(AccessEgressLinkData.class);
	private final Map<TransitStopFacility, List<Link>> accessEgressLinks = new HashMap<>();

	@Inject
	public AccessEgressLinkData(Network network, TransitSchedule schedule, NearestInteractionLinkData nearestLinkData,
			LimaAvConfigGroup limaConfig) {
		String attributeName = limaConfig.getFeederLinksStopAttribute();

		for (TransitStopFacility facility : schedule.getFacilities().values()) {
			String rawLinkIds = (String) facility.getAttributes().getAttribute(attributeName);

			if (rawLinkIds != null && rawLinkIds.trim().length() > 0) {
				List<Link> links = Arrays.asList(rawLinkIds.split(",")).stream().map(String::trim)
						.map(linkId -> network.getLinks().get(Id.createLinkId(linkId))).collect(Collectors.toList());

				Iterator<Link> iterator = links.iterator();

				while (iterator.hasNext()) {
					Link link = iterator.next();

					if (!link.getAllowedModes().contains(AVModule.AV_MODE)) {
						logger.error("Link " + link.getId().toString() + " is defined as access/egress for station "
								+ facility.getId().toString()
								+ ", but is not accessible for AVs (mode 'av' in the network). Ignoring access link.");
						iterator.remove();
					}
				}

				accessEgressLinks.put(facility, links);
			}
		}
	}

	public List<Link> getAccessEgressLinks(TransitStopFacility facility) {
		if (accessEgressLinks.containsKey(facility)) {
			return accessEgressLinks.get(facility);
		} else {
			return Collections.emptyList();
		}
	}
}
