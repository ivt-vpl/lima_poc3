package ch.ethz.matsim.lima_poc3.av.space;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.mobsim.framework.events.MobsimAfterSimStepEvent;
import org.matsim.core.mobsim.framework.events.MobsimBeforeSimStepEvent;
import org.matsim.core.mobsim.framework.listeners.MobsimAfterSimStepListener;
import org.matsim.core.mobsim.framework.listeners.MobsimBeforeSimStepListener;
import org.matsim.core.mobsim.qsim.qnetsimengine.QVehicle;
import org.matsim.core.utils.misc.Time;

public class LinkOccupancyHandler
		implements VehicleInteractionHandler, MobsimBeforeSimStepListener, MobsimAfterSimStepListener {
	private double time = Time.getUndefinedTime();
	private final EventsManager eventsManager;

	private final String occupancyType;
	private final SpatialCapacity capacity;
	private final Map<Link, Long> occupancy = new HashMap<>();
	private final Set<Link> changed = new HashSet<>();

	private boolean isBeforeStart = true;

	public LinkOccupancyHandler(String occupancyType, SpatialCapacity capacity, Network network,
			EventsManager eventsManager) {
		this.occupancyType = occupancyType;
		this.capacity = capacity;
		this.eventsManager = eventsManager;

		for (Link link : network.getLinks().values()) {
			occupancy.put(link, 0L);
		}
	}

	@Override
	public void registerParkedVehicle(QVehicle vehicle, Link link) {
		if (isBeforeStart) {
			occupancy.put(link, occupancy.get(link) + 1);
			changed.add(link);
		}
	}

	private boolean isSaturated(Link link) {
		return getAvailableSpots(link) <= 0;
	}

	private long getAvailableSpots(Link link) {
		return capacity.getSpatialCapacity(link, time) - occupancy.get(link);
	}

	@Override
	public boolean handleArrival(QVehicle vehicle, Link link) {
		if (isSaturated(link)) {
			return false;
		} else {
			changed.add(link);
			occupancy.put(link, occupancy.get(link) + 1);
			return true;
		}
	}

	@Override
	public void handleDeparture(QVehicle vehicle, Link link) {
		long updatedOccupancy = occupancy.get(link) - 1;

		if (updatedOccupancy < 0) {
			throw new IllegalStateException("Occupancy dropped below zero: " + link.getId().toString());
		}

		occupancy.put(link, updatedOccupancy);
		changed.add(link);
	}

	@Override
	public void notifyMobsimBeforeSimStep(@SuppressWarnings("rawtypes") MobsimBeforeSimStepEvent e) {
		isBeforeStart = false;
		this.time = e.getSimulationTime();
	}

	@Override
	public void notifyMobsimAfterSimStep(@SuppressWarnings("rawtypes") MobsimAfterSimStepEvent e) {
		for (Link link : changed) {
			eventsManager.processEvent(
					new SpaceAvailabilityEvent(time, occupancyType, link.getId(), getAvailableSpots(link)));
		}

		changed.clear();
	}
}
