package ch.ethz.matsim.lima_poc3.av.scoring;

import java.util.Map;

public class LIMAScoringParameters {
	public final double marginalUtilityOfDirectWaitingTime_s;
	public final double marginalUtilityOfFeederWaitingTime_s;

	public final double directPriceFactor;
	public final double feederPriceFactor;

	public final Map<String, Double> routingTransferUtilities;

	public LIMAScoringParameters(double marginalUtilityOfDirectWaitingTime_s,
			double marginalUtilityOfFeederWaitingTime_s, double directPriceFactor, double feederPriceFactor,
			Map<String, Double> routingTransferUtilities) {
		this.marginalUtilityOfDirectWaitingTime_s = marginalUtilityOfDirectWaitingTime_s;
		this.marginalUtilityOfFeederWaitingTime_s = marginalUtilityOfFeederWaitingTime_s;
		this.directPriceFactor = directPriceFactor;
		this.feederPriceFactor = feederPriceFactor;
		this.routingTransferUtilities = routingTransferUtilities;
	}
}
