package ch.ethz.matsim.lima_poc3.av.routing.spatial;

import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.matsim.api.core.v01.network.Link;
import org.matsim.core.gbl.MatsimRandom;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.facilities.Facility;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import com.google.inject.Inject;

public class IntermodalInteractionFacilityFinder implements AVInteractionFacilityFinder {
	private final AVInteractionFacilityFinder delegate;
	private final AccessEgressLinkData accessEgressLinkData;

	private final Random random;

	@Inject
	public IntermodalInteractionFacilityFinder(AccessEgressLinkData accessEgressLinkData,
			AVInteractionFacilityFinder delegate) {
		this.accessEgressLinkData = accessEgressLinkData;
		this.delegate = delegate;
		this.random = MatsimRandom.getLocalInstance();
	}

	private Link selectAccessEgressLink(List<Link> candidates) {
		return candidates.get(random.nextInt(candidates.size()));
	}

	private Optional<Facility<?>> findInteractionFacility(Facility<?> initialFacility) {
		if (initialFacility instanceof TransitStopFacility) {
			List<Link> accessEgressLinks = accessEgressLinkData
					.getAccessEgressLinks((TransitStopFacility) initialFacility);

			if (accessEgressLinks.size() > 0) {
				return Optional.of(new LinkWrapperFacility(selectAccessEgressLink(accessEgressLinks)));
			}
		}

		return Optional.empty();
	}

	@Override
	public Facility<?> findPickupFacility(Facility<?> originFacility, double time) {
		return findInteractionFacility(originFacility)
				.orElseGet(() -> delegate.findPickupFacility(originFacility, time));
	}

	@Override
	public Facility<?> findDropoffFacility(Facility<?> destinationFacility, double time) {
		return findInteractionFacility(destinationFacility)
				.orElseGet(() -> delegate.findDropoffFacility(destinationFacility, time));
	}
}
