package ch.ethz.matsim.lima_poc3.av.space;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.network.LinkImpl;

public class LIMASpatialCapacity implements SpatialCapacity {
	private final static Logger logger = Logger.getLogger(LIMASpatialCapacity.class);

	private final boolean isParking;

	private final List<Double> thresholds;
	private final Map<Id<Link>, List<Long>> capacity;
	private final Double interval;

	public LIMASpatialCapacity(List<Double> thresholds, Map<Id<Link>, List<Long>> capacity, boolean isParking) {
		this.isParking = isParking;
		this.thresholds = thresholds;
		this.capacity = capacity;

		List<Double> differences = new ArrayList<>(thresholds.size() - 1);

		for (int i = 0; i < thresholds.size() - 1; i++) {
			differences.add(thresholds.get(i + 1) - thresholds.get(i));
		}

		double firstDifference = thresholds.get(0);
		boolean hasConstantDifference = true;

		for (int i = 0; i < differences.size(); i++) {
			if (firstDifference != differences.get(i)) {
				hasConstantDifference = false;
			}
		}

		if (hasConstantDifference) {
			logger.info("Found constant difference: " + firstDifference);
		} else {
			logger.info("Did not find constant difference");
		}

		interval = hasConstantDifference ? firstDifference : null;
	}
	
	@Override
	public long getSpatialCapacity(Link link, double time) {
		if (isParking) {
			if (((LinkImpl) link).parkingCapacity == null) {
				((LinkImpl) link).parkingCapacity = capacity.get(link.getId());

				if (((LinkImpl) link).parkingCapacity == null) {
					((LinkImpl) link).parkingCapacity = new ArrayList<>(0);
				}
			}
		} else {
			if (((LinkImpl) link).pickupCapacity == null) {
				((LinkImpl) link).pickupCapacity = capacity.get(link.getId());

				if (((LinkImpl) link).pickupCapacity == null) {
					((LinkImpl) link).pickupCapacity = new ArrayList<>(0);
				}
			}
		}

		List<Long> capacities = null;

		if (isParking) {
			capacities = ((LinkImpl) link).parkingCapacity;
		} else {
			capacities = ((LinkImpl) link).pickupCapacity;
		}

		if (capacities.size() == 0) {
			return 0;
		}

		if (interval != null) {
			int index = Math.max(Math.min((int) Math.floor(time / interval), capacities.size() - 1), 0);
			return capacities.get(index);
		} else {
			int index = 0;

			while (time > thresholds.get(index) && index < capacities.size() - 1) {
				index++;
			}

			return capacities.get(index);
		}
	}

	@Override
	public boolean hasAnySpatialCapacity(Link link) {
		return capacity.get(link.getId()) != null;
	}

	static public LIMASpatialCapacity load(URL url, boolean isParking) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
		logger.info("Reading spatial capacity from " + url.toString());

		List<String> header = Arrays.asList(reader.readLine().split(";"));
		List<Double> thresholds = new ArrayList<>(header.size() - 1);

		for (int i = 1; i < header.size(); i++) {
			thresholds.add(Double.parseDouble(header.get(i)));
		}

		logger.info(String.format("  Found %d thresholds", thresholds.size()));

		String raw = null;
		List<String> row = null;

		Map<Id<Link>, List<Long>> capacities = new HashMap<>();

		while ((raw = reader.readLine()) != null) {
			row = Arrays.asList(raw.split(";"));

			if (row.size() != header.size()) {
				logger.error("  Invalid row: " + raw);
				continue;
			}

			Id<Link> linkId = Id.createLinkId(row.get(0));
			List<Long> capacity = new ArrayList<>(header.size() - 1);
			double sum = 0.0;

			for (int i = 1; i < header.size(); i++) {
				capacity.add(Long.parseLong(row.get(i)));
				sum += Long.parseLong(row.get(i));
			}

			if (sum > 0.0) {
				capacities.put(linkId, capacity);
			}
		}

		reader.close();

		logger.info(String.format("  Read %d links", capacities.size()));

		return new LIMASpatialCapacity(thresholds, capacities, isParking);
	}
}
