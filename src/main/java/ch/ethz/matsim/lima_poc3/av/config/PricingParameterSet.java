package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class PricingParameterSet extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "pricing";

	public final static String COST_CALCULATOR_MODE = "costCalculatorMode";
	public final static String SCENARIO_SCALING_FACTOR = "scenarioScalingFactor";
	public final static String HORIZON = "horizon";
	public final static String INITIAL_PRICE_PER_KM = "initialPricePerKm";

	private CostCalculatorMode costCalculatorMode = CostCalculatorMode.INACTIVE;
	private double scenarioScalingFactor = Double.NaN;
	private int horizon = 10;
	private double initialPricePerKm = 0.2;

	public enum CostCalculatorMode {
		INACTIVE, // If you don't want to run R
		ACTIVE, // If you want to calculate the price but NOT use it for scoring
		ADAPTIVE // If you want to calculate the price and USE it for scoring
	}

	public PricingParameterSet() {
		super(GROUP_NAME);
	}

	@StringGetter(COST_CALCULATOR_MODE)
	public CostCalculatorMode getCostCalculatorMode() {
		return costCalculatorMode;
	}

	@StringSetter(COST_CALCULATOR_MODE)
	public void setCostCalculatorMode(CostCalculatorMode costCalculatorMode) {
		this.costCalculatorMode = costCalculatorMode;
	}

	@StringGetter(SCENARIO_SCALING_FACTOR)
	public double getScenarioScalingFactor() {
		return scenarioScalingFactor;
	}

	@StringSetter(SCENARIO_SCALING_FACTOR)
	public void setScenarioScalingFactor(double scenarioScalingFactor) {
		this.scenarioScalingFactor = scenarioScalingFactor;
	}

	@StringGetter(HORIZON)
	public int getHorizon() {
		return horizon;
	}

	@StringSetter(HORIZON)
	public void setHorizon(int horizon) {
		this.horizon = horizon;
	}

	@StringGetter(INITIAL_PRICE_PER_KM)
	public double getInitialPricePerKm() {
		return initialPricePerKm;
	}

	@StringSetter(INITIAL_PRICE_PER_KM)
	public void setInitialPricePerKm(double initialPricePerKm) {
		this.initialPricePerKm = initialPricePerKm;
	}
}
