package ch.ethz.matsim.lima_poc3.av;

import java.util.List;

import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Route;
import org.matsim.core.gbl.MatsimRandom;
import org.matsim.core.utils.misc.Time;

import ch.ethz.matsim.lima_poc3.av.pricing.AVPricing;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.ExtendedAVRoute;
import ch.ethz.matsim.lima_poc3.srr.LimaRaptorParameters;
import ch.sbb.matsim.routing.pt.raptor.RaptorIntermodalAccessEgress;
import ch.sbb.matsim.routing.pt.raptor.RaptorParameters;

public class AVIntermodalAccessEgress implements RaptorIntermodalAccessEgress {
	private final AVPricing pricing;
	private final double sigma;

	public AVIntermodalAccessEgress(AVPricing pricing, double sigma) {
		this.pricing = pricing;
		this.sigma = sigma;
	}

	private double getPriceFactor(LimaRaptorParameters limaParams, String ongoingMode) {
		switch (ongoingMode) {
		case AVDirectRoutingModule.AV_DIRECT_MODE:
			return limaParams.getDirectPriceFactor();
		case AVFeederRoutingModule.AV_FEEDER_MODE:
			return limaParams.getFeederPriceFactor();
		default:
			throw new IllegalStateException();
		}
	}

	@Override
	public RIntermodalAccessEgress calcIntermodalAccessEgress(List<? extends PlanElement> legs,
			RaptorParameters params) {
		// This is mainly a copy & paste from DefaultRaptorIntermodalAccessEgress
		LimaRaptorParameters limaParams = (LimaRaptorParameters) params;

		double disutility = 0.0;
		double tTime = 0.0;

		for (PlanElement pe : legs) {
			if (pe instanceof Leg) {
				String mode = ((Leg) pe).getMode();
				Route route = ((Leg) pe).getRoute();

				double travelTime = ((Leg) pe).getTravelTime();

				if (Time.getUndefinedTime() != travelTime) {
					tTime += travelTime;
					disutility += travelTime * -params.getMarginalUtilityOfTravelTime_utl_s(mode);
				} else {
					throw new IllegalStateException();
				}

				// Added for AVs

				disutility -= limaParams.getTransferUtility_utl(mode);

				if (route instanceof ExtendedAVRoute) {
					double waitingTime = ((ExtendedAVRoute) route).getPickupTime();

					if (Time.getUndefinedTime() != waitingTime) {
						disutility += waitingTime * -limaParams.getMarginalUtilityOfFeederWaitingTime_s();
					} else {
						throw new IllegalStateException();
					}

					if (pricing.useCostCalculator()) {
						double cost = pricing.computeCost(route.getDistance() * 1e-3, getPriceFactor(limaParams, mode));
						disutility += cost * -limaParams.getMarginalUtilityOfMoney();
					} else {
						disutility += -limaParams.getMarginalUtilityOfDistanceFeeder() * route.getDistance();
					}
				}
			}
		}

		if (sigma > 0.0) {
			disutility += sigma * MatsimRandom.getLocalInstance().nextGaussian();
		}

		return new RIntermodalAccessEgress(legs, disutility, tTime);
	}
}
