package ch.ethz.matsim.lima_poc3.av.analysis;

import org.matsim.api.core.v01.network.Network;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.StartupEvent;
import org.matsim.core.controler.listener.StartupListener;
import org.matsim.core.network.io.NetworkWriter;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import ch.ethz.matsim.av.framework.AVModule;

@Singleton
public class AVNetworkListener implements StartupListener {
	private final Network network;
	private final OutputDirectoryHierarchy outputDirectoryHierarchy;

	@Inject
	public AVNetworkListener(@Named(AVModule.AV_MODE) Network network,
			OutputDirectoryHierarchy outputDirectoryHierarchy) {
		this.network = network;
		this.outputDirectoryHierarchy = outputDirectoryHierarchy;

		if (network.getLinks().size() == 0) {
			throw new IllegalStateException("AV network is empty! Did you forget to assign 'av' mode to links?");
		}
	}

	@Override
	public void notifyStartup(StartupEvent event) {
		new NetworkWriter(network).write(outputDirectoryHierarchy.getOutputFilename("av_network.xml.gz"));
	}
}
