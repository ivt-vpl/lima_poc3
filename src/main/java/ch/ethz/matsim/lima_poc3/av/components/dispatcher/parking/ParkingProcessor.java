package ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.path.VrpPathWithTravelData;
import org.matsim.contrib.dvrp.path.VrpPathWithTravelDataImpl;
import org.matsim.contrib.dvrp.schedule.Schedule;
import org.matsim.contrib.dvrp.schedule.Schedules;
import org.matsim.contrib.dvrp.schedule.Task;
import org.matsim.contrib.dvrp.tracker.OnlineDriveTaskTracker;
import org.matsim.contrib.dvrp.util.LinkTimePair;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.core.utils.collections.Tuple;

import ch.ethz.matsim.av.data.AVVehicle;
import ch.ethz.matsim.av.plcpc.ParallelLeastCostPathCalculator;
import ch.ethz.matsim.av.schedule.AVDriveTask;
import ch.ethz.matsim.av.schedule.AVStayTask;
import ch.ethz.matsim.lima_poc3.av.components.LIMAVehicle;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;

public class ParkingProcessor {
	private final ParallelLeastCostPathCalculator router;
	private final SpatialCapacity capacity;
	private final TravelTime travelTime;

	private final Set<LIMAVehicle> parkingRequests = new HashSet<>();
	private final List<ParkingAppendTask> appendTasks = new LinkedList<>();

	private final Map<Link, ParkingLinkWrapper> parkingLinks = new HashMap<>();
	private final List<ParkingLinkWrapper> occupiedLinks = new LinkedList<>();
	private final QuadTree<ParkingLinkWrapper> availableParkings;

	private final Collection<Tuple<LIMAVehicle, AVDriveTask>> activeVehicles = new LinkedList<>();

	public ParkingProcessor(Network network, SpatialCapacity capacity, ParallelLeastCostPathCalculator router,
			TravelTime travelTime) {
		double[] bounds = NetworkUtils.getBoundingBox(network.getNodes().values()); // minx, miny, maxx, maxy
		availableParkings = new QuadTree<>(bounds[0], bounds[1], bounds[2], bounds[3]);

		this.capacity = capacity;
		this.router = router;
		this.travelTime = travelTime;

		for (Link link : network.getLinks().values()) {
			if (capacity.hasAnySpatialCapacity(link)) {
				ParkingLinkWrapper wrapper = new ParkingLinkWrapper(link, 0);
				parkingLinks.put(link, wrapper);
				occupiedLinks.add(wrapper);
			}
		}
	}

	// Called at the start
	public void registerInitially(LIMAVehicle vehicle, Link link) {
		ParkingLinkWrapper parkingLink = parkingLinks.get(link);
		parkingLink.numberOfVehicles++;
	}

	// Called when a vehicle needs to park
	public void register(LIMAVehicle vehicle) {
		parkingRequests.add(vehicle);
	}

	// Called when a vehicle is dispatched
	public void unregister(LIMAVehicle vehicle) {
		Link link = ((AVStayTask) vehicle.getSchedule().getCurrentTask()).getLink();
		ParkingLinkWrapper parkingLink = parkingLinks.get(link);
		parkingLink.numberOfVehicles--;
	}

	public void update(double now) {
		try {
			// I) Process pending routing tasks
			processAppendTasks(now);

			// II) Move occupied parkings to the available index if something got free
			Iterator<ParkingLinkWrapper> iterator = occupiedLinks.iterator();

			while (iterator.hasNext()) {
				ParkingLinkWrapper parkingLink = iterator.next();
				long currentCapacity = capacity.getSpatialCapacity(parkingLink.link, now);

				if (parkingLink.numberOfVehicles < currentCapacity) {
					iterator.remove();
					availableParkings.put(parkingLink.link.getCoord().getX(), parkingLink.link.getCoord().getY(),
							parkingLink);
				}
			}

			// III) Go through the parking requests
			for (LIMAVehicle vehicle : parkingRequests) {
				Link originLink = ((AVStayTask) vehicle.getSchedule().getCurrentTask()).getLink();
				ParkingLinkWrapper parking = occupyParking(originLink, now);

				// Add routing task to be processed next time step
				Future<Path> pathFuture = router.calcLeastCostPath(originLink.getToNode(), parking.link.getFromNode(),
						now, null, null);
				appendTasks.add(new ParkingAppendTask(vehicle, originLink, parking.link, pathFuture));
			}

			parkingRequests.clear();

			processActive(now);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}

		/*long sum = 0;

		for (ParkingLinkWrapper wrapper : parkingLinks.values()) {
			sum += wrapper.numberOfVehicles;
		}

		System.err.println("Taking spots: " + sum);*/
	}

	private void removeAvailableParking(ParkingLinkWrapper parkingLink) {
		availableParkings.remove(parkingLink.link.getCoord().getX(), parkingLink.link.getCoord().getY(), parkingLink);
	}

	private ParkingLinkWrapper occupyParking(Link originLink, double now) {
		while (true) {
			if (availableParkings.size() == 0) {
				throw new IllegalStateException("Tried to find a parking spot, but whole system is occupied.");
			}

			ParkingLinkWrapper parkingLink = availableParkings.getClosest(originLink.getCoord().getX(),
					originLink.getCoord().getY());
			long currentCapacity = capacity.getSpatialCapacity(parkingLink.link, now);

			if (parkingLink.numberOfVehicles >= currentCapacity) {
				removeAvailableParking(parkingLink);
				occupiedLinks.add(parkingLink);
			} else {
				parkingLink.numberOfVehicles++;
				return parkingLink;
			}
		}
	}

	private void processAppendTasks(double now) throws InterruptedException, ExecutionException {
		for (ParkingAppendTask task : appendTasks) {
			AVVehicle vehicle = task.vehicle;

			Schedule schedule = vehicle.getSchedule();
			AVStayTask stayTask = (AVStayTask) Schedules.getLastTask(schedule);

			double startTime = 0.0;
			double scheduleEndTime = schedule.getEndTime();

			if (stayTask.getStatus() == Task.TaskStatus.STARTED) {
				startTime = now;
			} else {
				startTime = stayTask.getBeginTime();
			}

			Path path = task.pathFuture.get();

			VrpPathWithTravelData vrpPath = createPathWithoutZeroLength(task.originLink, task.destinationLink,
					startTime, path, travelTime);
			AVDriveTask driveTask = new AVDriveTask(vrpPath);
			activeVehicles.add(new Tuple<>((LIMAVehicle) vehicle, driveTask));

			if (stayTask.getStatus() == Task.TaskStatus.STARTED) {
				stayTask.setEndTime(startTime);
			} else {
				schedule.removeLastTask();
			}

			schedule.addTask(driveTask);

			if (driveTask.getEndTime() < scheduleEndTime) {
				schedule.addTask(new AVStayTask(driveTask.getEndTime(), scheduleEndTime, task.destinationLink));
			}
		}

		appendTasks.clear();
	}

	private final Logger logger = Logger.getLogger(ParkingProcessor.class);

	private void processActive(double now) throws InterruptedException, ExecutionException {
		Iterator<Tuple<LIMAVehicle, AVDriveTask>> iterator = activeVehicles.iterator();

		while (iterator.hasNext()) {
			Tuple<LIMAVehicle, AVDriveTask> item = iterator.next();

			AVDriveTask task = item.getSecond();
			AVVehicle vehicle = item.getFirst();

			if (task.getStatus().equals(Task.TaskStatus.PERFORMED)) {
				iterator.remove(); // Apparently, we arrived wihtout a problem
			} else if (task.getStatus().equals(Task.TaskStatus.STARTED)) {
				ParkingLinkWrapper parking = parkingLinks.get(task.getPath().getToLink());
				OnlineDriveTaskTracker tracker = (OnlineDriveTaskTracker) task.getTaskTracker();

				long currentCapacity = capacity.getSpatialCapacity(parking.link, tracker.predictEndTime());

				if (parking.numberOfVehicles > currentCapacity) {
					// Here we see that the link is occupied now!

					LinkTimePair diversionPoint = tracker.getDiversionPoint();

					if (diversionPoint == null) {
						logger.error("Problem, we don't have a diversion point.");
					} else {
						// Unregsiter vehicle
						parking.numberOfVehicles--;

						// Find new spot
						parking = occupyParking(diversionPoint.link, diversionPoint.time);

						// Divert path to new spot
						Path path = router.calcLeastCostPath(diversionPoint.link.getToNode(),
								parking.link.getFromNode(), now, null, null).get();
						VrpPathWithTravelData vrpPath = createPathWithoutZeroLength(diversionPoint.link, parking.link,
								diversionPoint.time, path, travelTime);

						tracker.divertPath(vrpPath);

						// Adjust schedule with new destination
						Schedule schedule = vehicle.getSchedule();
						double scheduleEndTime = schedule.getEndTime();

						schedule.removeLastTask();
						schedule.addTask(new AVStayTask(vrpPath.getArrivalTime(), scheduleEndTime, parking.link));
					}
				}
			}
		}
	}

	/*
	 * This function is copied from DVRP, but we explicitly do *not* want to generat
	 * zero-length paths.
	 */
	public static VrpPathWithTravelData createPathWithoutZeroLength(Link fromLink, Link toLink, double departureTime,
			Path path, TravelTime travelTime) {
		/*
		 * if (fromLink == toLink) { return createZeroLengthPath(fromLink,
		 * departureTime); }
		 */

		int count = path.links.size();
		if (count > 0) {
			if (fromLink.getToNode() != path.links.get(0).getFromNode()) {
				throw new IllegalArgumentException("fromLink and path are not connected; fromLink: " + fromLink
						+ "\n path beg" + path.links.get(0));
			}
			if (path.links.get(count - 1).getToNode() != toLink.getFromNode()) {
				throw new IllegalArgumentException("path and toLink are not connected; path end:"
						+ path.links.get(count - 1).toString() + "\n toLink: " + toLink.toString());
			}
		}

		Link[] links = new Link[count + 2];
		double[] linkTTs = new double[count + 2];

		// we start at the end of fromLink
		// actually, in QSim, it usually takes 1 second to move over the first node
		// (when INSERTING_WAITING_VEHICLES_BEFORE_DRIVING_VEHICLES is ON;
		// otherwise it can take much longer)
		double currentTime = departureTime;
		links[0] = fromLink;
		double linkTT = FIRST_LINK_TT;
		linkTTs[0] = linkTT;
		currentTime += linkTT;

		for (int i = 1; i <= count; i++) {
			Link link = path.links.get(i - 1);
			links[i] = link;
			linkTT = travelTime.getLinkTravelTime(link, currentTime, null, null);
			linkTTs[i] = linkTT;
			currentTime += linkTT;
		}

		// there is no extra time spent on queuing at the end of the last link
		links[count + 1] = toLink;
		linkTT = getLastLinkTT(toLink, currentTime);// as long as we cannot divert from the last link this is okay
		linkTTs[count + 1] = linkTT;
		double totalTT = FIRST_LINK_TT + path.travelTime + linkTT;

		return new VrpPathWithTravelDataImpl(departureTime, totalTT, links, linkTTs);
	}

	static final double FIRST_LINK_TT = 1;

	static double getLastLinkTT(Link lastLink, double time) {
		// XXX imprecise if qsimCfg.timeStepSize != 1
		return Math.floor(lastLink.getLength() / lastLink.getFreespeed(time));
	}
}
