package ch.ethz.matsim.lima_poc3.av.scoring;

import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.utils.objectattributes.ObjectAttributes;

import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.config.LimaScoringParameterSet;

public class LIMAScoringParameterForPerson {
	private final String subpopulationAttributeName;
	private final ObjectAttributes personAttributes;
	private final LimaAvConfigGroup limaConfig;
	private final Map<String, LIMAScoringParameters> scoringParameters = new HashMap<>();

	public LIMAScoringParameterForPerson(LimaAvConfigGroup limaConfig, Scenario scenario) {
		this.subpopulationAttributeName = scenario.getConfig().plans().getSubpopulationAttributeName();
		this.personAttributes = scenario.getPopulation().getPersonAttributes();
		this.limaConfig = limaConfig;
	}

	public LIMAScoringParameters getScoringParameters(Person person) {
		String subpopulation = (String) personAttributes.getAttribute(person.getId().toString(),
				subpopulationAttributeName);

		if (subpopulation == null) {
			subpopulation = "default";
		}

		LIMAScoringParameters personParameters = scoringParameters.get(subpopulation);

		if (personParameters == null) {
			LimaScoringParameterSet scoringConfig = limaConfig.getScoringParametersForSubpopulation(subpopulation);

			double marginalUtilityOfDirectWaitingTime_s = scoringConfig.getMarginalUtilityOfDirectWaitingTime()
					/ 3600.0;
			double marginalUtilityOfFeederWaitingTime_s = scoringConfig.getMarginalUtilityOfFeederWaitingTime()
					/ 3600.0;

			Map<String, Double> routingTransferUtilities = new HashMap<>();
			routingTransferUtilities.put("bike", limaConfig.getRoutingTransferUtilityBike());
			routingTransferUtilities.put("av_feeder", limaConfig.getRoutingTransferUtilityFeeder());

			personParameters = new LIMAScoringParameters(marginalUtilityOfDirectWaitingTime_s,
					marginalUtilityOfFeederWaitingTime_s, scoringConfig.getDirectPriceFactor(),
					scoringConfig.getFeederPriceFactor(), routingTransferUtilities);

			scoringParameters.put(subpopulation, personParameters);
		}

		return personParameters;
	}
}
