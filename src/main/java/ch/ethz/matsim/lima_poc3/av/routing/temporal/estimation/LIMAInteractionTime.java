package ch.ethz.matsim.lima_poc3.av.routing.temporal.estimation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.events.handler.PersonEntersVehicleEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Person;
import org.matsim.facilities.Facility;

import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.AVInteractionTime;

public class LIMAInteractionTime
		implements AVInteractionTime, PersonDepartureEventHandler, PersonEntersVehicleEventHandler {
	private final Logger logger = Logger.getLogger(LIMAInteractionTime.class);

	private final double estimationStartTime;
	private final double estimationEndTime;
	private final double estimationInterval;
	private final int numberOfEstimationBins;
	private final double fixedPickupTime;
	private final double fixedDropoffTime;

	private final Map<Id<Link>, Integer> groupIndices;
	private final IndexEstimator estimator;

	private final String groupAttribute;

	public LIMAInteractionTime(double estimationStartTime, double estimationEndTime, double estimationInterval,
			int horizon, double defaultWaitingTime, double fixedPickupTime, double fixedDropoffTime, Network network,
			String groupAttribute, String defaultWaitingTimeAttribute) {
		this.estimationStartTime = estimationStartTime;
		this.estimationEndTime = estimationEndTime;
		this.estimationInterval = estimationInterval;
		this.numberOfEstimationBins = (int) Math.ceil((estimationEndTime - estimationStartTime) / estimationInterval);
		this.fixedPickupTime = fixedPickupTime;
		this.fixedDropoffTime = fixedDropoffTime;
		this.groupAttribute = groupAttribute;
		this.groupIndices = createGroupIndices(network);

		List<Double> defaultWaitingTimes = new ArrayList<>(groupIndices.size());

		for (int i = 0; i < groupIndices.size(); i++) {
			defaultWaitingTimes.add(defaultWaitingTime);
		}

		for (Map.Entry<Id<Link>, Integer> entry : groupIndices.entrySet()) {
			int index = entry.getValue();

			if (defaultWaitingTimes.get(index) == defaultWaitingTime) {
				Link link = network.getLinks().get(entry.getKey());
				Double linkWaitingTime = (Double) link.getAttributes().getAttribute(defaultWaitingTimeAttribute);

				if (linkWaitingTime != null) {
					defaultWaitingTimes.set(index, linkWaitingTime);
				}
			}
		}

		this.estimator = new MovingAverageIndexEstimator(groupIndices.size(), numberOfEstimationBins, horizon,
				defaultWaitingTimes);
	}

	private Map<Id<Link>, Integer> createGroupIndices(Network network) {
		List<String> names = new LinkedList<>();
		List<Integer> counts = new LinkedList<>();

		Map<Id<Link>, Integer> indices = new HashMap<>();

		for (Link link : network.getLinks().values()) {
			String groupName = (String) link.getAttributes().getAttribute(groupAttribute);

			if (groupName == null) {
				groupName = "__default__";
			}

			if (!names.contains(groupName)) {
				names.add(groupName);
				counts.add(0);
			}

			int index = names.indexOf(groupName);
			indices.put(link.getId(), index);
			counts.set(index, counts.get(index) + 1);
		}

		logger.info(String.format("Found %d estimation groups:", names.size()));
		for (int k = 0; k < names.size(); k++) {
			logger.info(String.format("%s: %d obs", names.get(k), counts.get(k)));
		}

		return indices;
	}

	@Override
	public double getPickupTime(Facility<?> facility, double time, double approachTime) {
		double result = Math.max(0,
				estimator.estimate(getGroupIndex(facility.getLinkId()), getTimeIndex(time)) - approachTime)
				+ fixedPickupTime;

		return result;
	}

	@Override
	public double getDropoffTime(Facility<?> facility, double time) {
		return fixedDropoffTime;
	}

	private int getGroupIndex(Id<Link> linkId) {
		return groupIndices.get(linkId);
	}

	private int getTimeIndex(double time) {
		if (time < estimationStartTime) {
			return 0;
		} else if (time >= estimationEndTime) {
			return numberOfEstimationBins - 1;
		} else {
			return (int) Math.floor((time - estimationStartTime) / estimationInterval);
		}
	}

	private final Map<Id<Person>, PersonDepartureEvent> departureEvents = new HashMap<>();

	private void recordPickupTime(double pickupTime, double time, Id<Link> linkId) {
		estimator.record(getGroupIndex(linkId), getTimeIndex(time), pickupTime);
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (event.getLegMode().equals(AVDirectRoutingModule.AV_DIRECT_MODE)
				|| event.getLegMode().equals(AVFeederRoutingModule.AV_FEEDER_MODE)) {
			departureEvents.put(event.getPersonId(), event);
		}
	}

	@Override
	public void handleEvent(PersonEntersVehicleEvent event) {
		PersonDepartureEvent departureEvent = departureEvents.remove(event.getPersonId());

		if (departureEvent != null) {
			recordPickupTime(event.getTime() - departureEvent.getTime(), departureEvent.getTime(),
					departureEvent.getLinkId());
		}
	}

	@Override
	public void reset(int iteration) {
		departureEvents.clear();
		estimator.finish();
	}
}
