package ch.ethz.matsim.lima_poc3.av.routing;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.utils.misc.Time;

import ch.ethz.matsim.av.data.AVOperator;
import ch.ethz.matsim.av.routing.AVRoute;

public class ExtendedAVRoute extends AVRoute {
	private double pickupTime = Time.getUndefinedTime();
	private double dropoffTime = Time.getUndefinedTime();

	public ExtendedAVRoute(AVRoute route) {
		super(route);
	}
	
    public ExtendedAVRoute(Id<Link> startLinkId, Id<Link> endLinkId, Id<AVOperator> operatorId) {
        super(startLinkId, endLinkId, operatorId);
    }

	public double getPickupTime() {
		return pickupTime;
	}

	public void setPickupTime(double pickupTime) {
		this.pickupTime = pickupTime;
	}

	public double getDropoffTime() {
		return dropoffTime;
	}

	public void setDropoffTime(double dropoffTime) {
		this.dropoffTime = dropoffTime;
	}

	public double getJourneyTime() {
		return getTravelTime() + pickupTime + dropoffTime;
	}
}
