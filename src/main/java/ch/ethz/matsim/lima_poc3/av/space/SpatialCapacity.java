package ch.ethz.matsim.lima_poc3.av.space;

import org.matsim.api.core.v01.network.Link;

public interface SpatialCapacity {
	long getSpatialCapacity(Link link, double time);
	boolean hasAnySpatialCapacity(Link link);
}
