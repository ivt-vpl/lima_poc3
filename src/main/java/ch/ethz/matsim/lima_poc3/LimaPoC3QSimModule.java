package ch.ethz.matsim.lima_poc3;

import java.util.Collection;

import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.mobsim.qsim.pt.TransitEnginePlugin;

import com.google.inject.Provides;
import com.google.inject.Singleton;

import ch.ethz.idsc.amodeus.matsim.mod.AmodeusQSimPlugin;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVQSimPlugin;
import ch.ethz.matsim.lima_poc3.av.simulation.MultiAVQSimPlugin;
import ch.ethz.matsim.lima_poc3.av.space.LIMAOccupancyQSimPlugin;
import ch.sbb.matsim.mobsim.qsim.pt.SBBTransitEnginePlugin;

public class LimaPoC3QSimModule extends AbstractModule {
	private final boolean useAmodeus = false;
	private final boolean useSBB;

	public LimaPoC3QSimModule(boolean useAv, boolean useSBB) {
		this.useSBB = useSBB;
	}

	@Override
	public void install() {
	}

	@Provides
	@Singleton
	public Collection<AbstractQSimPlugin> provideQSimPlugins(Config config) {
		Collection<AbstractQSimPlugin> plugins = new AVModule().provideQSimPlugins(config);
		
		plugins.add(new MultiAVQSimPlugin(config));
		plugins.add(new LIMAOccupancyQSimPlugin(config));

		if (useAmodeus) {
			plugins.removeIf(p -> p instanceof AVQSimPlugin);
			plugins.add(new AmodeusQSimPlugin(config));
		}
		
		if (useSBB) {
			if (config.transit().isUseTransit()) {
				plugins.removeIf(p -> p instanceof TransitEnginePlugin);
				plugins.add(new SBBTransitEnginePlugin(config));
			}
		}

		return plugins;
	}

}
