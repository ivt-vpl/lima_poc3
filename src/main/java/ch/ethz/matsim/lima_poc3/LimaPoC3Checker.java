package ch.ethz.matsim.lima_poc3;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.apache.log4j.Logger;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ScoringParameterSet;
import org.matsim.core.config.groups.SubtourModeChoiceConfigGroup;

import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.config.PricingParameterSet;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup.IntermodalAccessEgressParameterSet;

public class LimaPoC3Checker {
	private static final Logger logger = Logger.getLogger(LimaPoC3Checker.class);

	static public void checkFlowEfficiency(Config config) {
		checkCode();
	}

	static public void checkNoAv(Config config) {
		SwissRailRaptorConfigGroup srrConfig = (SwissRailRaptorConfigGroup) config.getModules()
				.get(SwissRailRaptorConfigGroup.GROUP);

		if (srrConfig != null) {
			if (srrConfig.isUseIntermodalAccessEgress()) {
				for (IntermodalAccessEgressParameterSet set : srrConfig.getIntermodalAccessEgressParameterSets()) {
					if (set.getMode().equals(AVFeederRoutingModule.AV_FEEDER_MODE)) {
						throw new IllegalStateException(
								"The av_feeder mode is registered as access/egress for SwissRailRaptor although AVs are not enabled in the PoC3 module.");
					}
				}
			}
		}

		SubtourModeChoiceConfigGroup smcConfig = config.subtourModeChoice();

		if (Arrays.asList(smcConfig.getModes()).contains(AVDirectRoutingModule.AV_DIRECT_MODE)) {
			throw new IllegalStateException(
					"The mode av_direct cannot be used in SubtourModeChoice if AVs are not enabled in the PoC3 module.");
		}
	}

	static public void checkAv(Config config) {
		// CHECK SwissRailRaptor

		if (!config.getModules().containsKey(SwissRailRaptorConfigGroup.GROUP)) {
			throw new IllegalStateException("SwissRailRaptor config group should be activated.");
		} else {
			SwissRailRaptorConfigGroup srrConfig = (SwissRailRaptorConfigGroup) config.getModules()
					.get(SwissRailRaptorConfigGroup.GROUP);

			if (!srrConfig.isUseIntermodalAccessEgress()) {
				logger.warn(
						"SwissRailRaptor is not configured to use intermodalAccessEgress. Did you forget to set it up?");
			}

			boolean foundFeeder = false;

			for (IntermodalAccessEgressParameterSet set : srrConfig.getIntermodalAccessEgressParameterSets()) {
				if (set.getMode().equals(AVFeederRoutingModule.AV_FEEDER_MODE)) {
					foundFeeder = true;
				}
			}

			if (!foundFeeder) {
				logger.warn("SwissRailRaptor does not contain " + AVFeederRoutingModule.AV_FEEDER_MODE
						+ " as a access/egress mode. Did you forget to set it up?");
			}
		}

		// CHECK Scoring

		PlanCalcScoreConfigGroup scoringConfig = config.planCalcScore();
		Set<String> subpopulations = scoringConfig.getScoringParametersPerSubpopulation().keySet();
		Collection<String> avModes = Arrays.asList(AVFeederRoutingModule.AV_FEEDER_MODE,
				AVDirectRoutingModule.AV_DIRECT_MODE);

		for (String subpopulation : subpopulations) {
			ScoringParameterSet params = scoringConfig.getScoringParametersPerSubpopulation().get(subpopulation);

			for (String avMode : avModes) {
				if (!params.getModes().containsKey(avMode)) {
					throw new IllegalStateException("Scoring parameters for subpopulation " + subpopulation
							+ " do not contain mode parameters for " + avMode);
				}
			}

			if (!params.getActivityParamsPerType().containsKey("av interaction")) {
				throw new IllegalStateException("Scoring parameters for subpopulation " + subpopulation
						+ " do not contain activity parameters for 'av interaction'");
			}
		} 

		// Mode choice
		SubtourModeChoiceConfigGroup smcConfig = config.subtourModeChoice();

		if (!Arrays.asList(smcConfig.getModes()).contains(AVDirectRoutingModule.AV_DIRECT_MODE)) {
			logger.warn("SubtourModeChoice does not contain " + AVDirectRoutingModule.AV_DIRECT_MODE
					+ ". Did you forget to set it up?");
		}

		if (Arrays.asList(smcConfig.getModes()).contains(AVFeederRoutingModule.AV_FEEDER_MODE)) {
			throw new IllegalStateException("The mode av_feeder should not be registered in SubtourModeChoice");
		}

		if (!config.getModules().containsKey(LimaAvConfigGroup.GROUP_NAME)) {
			throw new IllegalStateException(LimaAvConfigGroup.GROUP_NAME + " config group should be activated.");
		} else {
			LimaAvConfigGroup limaAvConfig = (LimaAvConfigGroup) config.getModules().get(LimaAvConfigGroup.GROUP_NAME);

			for (String subpopulation : subpopulations) {
				if (!limaAvConfig.getScoringParametersBySubpopulation().containsKey(subpopulation)) {
					throw new IllegalStateException(
							"No av waiting time scoring parameter set given for subpopulation " + subpopulation);
				}
			}
			
			// CHECK Pricing
			PricingParameterSet pricingConfig = limaAvConfig.getPricingConfig();
			
			switch (pricingConfig.getCostCalculatorMode()) {
			case ACTIVE:
			case ADAPTIVE:
				if (!Double.isFinite(pricingConfig.getScenarioScalingFactor())) {
					throw new IllegalStateException("Pricing scenario scaling factor is not finite");
				}
				break;
			default:
				break;
			}
		}

		// CHECK DVRP

		if (!config.getModules().containsKey(DvrpConfigGroup.GROUP_NAME)) {
			throw new IllegalStateException(DvrpConfigGroup.GROUP_NAME + " config group should be activated.");
		} else {
			DvrpConfigGroup dvrpConfig = (DvrpConfigGroup) config.getModules().get(DvrpConfigGroup.GROUP_NAME);

			if (!dvrpConfig.getNetworkMode().equals("av")) {
				throw new IllegalStateException("DVRP network mode should be 'av'");
			}
		}

		checkCode();
	}

	static public void checkCode() {
		try {
			LimaPoC3Checker.class.getClassLoader().loadClass("org.matsim.core.mobsim.qsim.qnetsimengine.AbstractQLink")
					.getDeclaredField("LIMA_TAG");
			LimaPoC3Checker.class.getClassLoader()
					.loadClass("org.matsim.core.mobsim.qsim.qnetsimengine.QueueWithBuffer")
					.getDeclaredField("LIMA_TAG");
			LimaPoC3Checker.class.getClassLoader()
					.loadClass("org.matsim.core.network.algorithms.TransportModeNetworkFilter")
					.getDeclaredField("LIMA_TAG");
			LimaPoC3Checker.class.getClassLoader().loadClass("ch.sbb.matsim.routing.pt.raptor.SwissRailRaptor")
					.getDeclaredField("LIMA_TAG");
		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
			throw new IllegalStateException(
					"The MATSim classes overridden by PoC3 do not seem to be present. Check pom configuration!");
		}
	}
}
