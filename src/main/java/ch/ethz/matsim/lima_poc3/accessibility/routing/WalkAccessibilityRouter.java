package ch.ethz.matsim.lima_poc3.accessibility.routing;

import org.matsim.api.core.v01.Coord;
import org.matsim.core.utils.geometry.CoordUtils;

public class WalkAccessibilityRouter implements AccessibilityRouter {
	private final double beelineDistanceFactorAndSpeed;

	private WalkAccessibilityRouter(double beelineDistanceFactorAndSpeed) {
		this.beelineDistanceFactorAndSpeed = beelineDistanceFactorAndSpeed;
	}

	@Override
	public double computeTravelTime(Coord fromCoord, Coord toCoord) {
		return CoordUtils.calcEuclideanDistance(fromCoord, toCoord) * beelineDistanceFactorAndSpeed;
	}

	public static class Factory implements AccessibilityRouter.Factory {
		private final double beelineDistanceFactor;
		private final double walkSpeed;

		public Factory(double beelineDistanceFactor, double walkSpeed) {
			this.beelineDistanceFactor = beelineDistanceFactor;
			this.walkSpeed = walkSpeed;
		}

		@Override
		public WalkAccessibilityRouter create() {
			return new WalkAccessibilityRouter(beelineDistanceFactor / walkSpeed);
		}
	}
}
