package ch.ethz.matsim.lima_poc3.accessibility.utils;

import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZone;

public interface AccessibilityFunction {
	double computeAccessibility(AccessibilityZone originZone, AccessibilityZone destinationZone, double travelTime);
}
