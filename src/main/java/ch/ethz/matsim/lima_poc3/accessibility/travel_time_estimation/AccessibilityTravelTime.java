package ch.ethz.matsim.lima_poc3.accessibility.travel_time_estimation;

import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.router.util.TravelTime;
import org.matsim.vehicles.Vehicle;

public class AccessibilityTravelTime implements TravelTime {
	private final Map<Id<Link>, Double> estimatedTravelTimes;
	private final double freespeedFactor;

	public AccessibilityTravelTime(Map<Id<Link>, Double> estimatedTravelTimes, double freespeedFactor) {
		this.estimatedTravelTimes = estimatedTravelTimes;
		this.freespeedFactor = freespeedFactor;
	}

	@Override
	public double getLinkTravelTime(Link link, double time, Person person, Vehicle vehicle) {
		if (estimatedTravelTimes.containsKey(link.getId())) {
			return estimatedTravelTimes.get(link.getId());
		} else {
			return link.getLength() / (freespeedFactor * link.getFreespeed());
		}
	}
}
