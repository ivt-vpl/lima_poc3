package ch.ethz.matsim.lima_poc3.accessibility.routing;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.TransportMode;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.facilities.Facility;
import org.matsim.pt.transitSchedule.api.TransitSchedule;

import ch.ethz.matsim.lima_poc3.accessibility.routing.PTAccessibilityRouter.Factory.CustomFacility;
import ch.sbb.matsim.routing.pt.raptor.DefaultRaptorIntermodalAccessEgress;
import ch.sbb.matsim.routing.pt.raptor.DefaultRaptorParametersForPerson;
import ch.sbb.matsim.routing.pt.raptor.LeastCostRaptorRouteSelector;
import ch.sbb.matsim.routing.pt.raptor.RaptorIntermodalAccessEgress;
import ch.sbb.matsim.routing.pt.raptor.RaptorParametersForPerson;
import ch.sbb.matsim.routing.pt.raptor.RaptorRouteSelector;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptor;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorFactory;

public class PTAccessibilityRouter implements AccessibilityRouter {
	private final SwissRailRaptor raptor;
	private final double departureTime;

	public PTAccessibilityRouter(SwissRailRaptor raptor, double departureTime) {
		this.raptor = raptor;
		this.departureTime = departureTime;
	}

	@Override
	public double computeTravelTime(Coord fromCoord, Coord toCoord) {
		List<Leg> legs = raptor.calcRoute(new CustomFacility(fromCoord), new CustomFacility(toCoord), departureTime,
				null);
		Leg lastLeg = legs.get(legs.size() - 1);

		double arrivalTime = lastLeg.getDepartureTime() + lastLeg.getTravelTime();
		double travelTime = arrivalTime - departureTime;

		return travelTime;
	}

	public static class Factory implements AccessibilityRouter.Factory {
		private final Network network;
		private final TransitSchedule schedule;
		private final double departureTime;

		private final double beelineFactor;
		private final double walkSpeed;

		public Factory(TransitSchedule schedule, double departureTime, Network network, double beelineFactor,
				double walkSpeed) {
			this.schedule = schedule;
			this.departureTime = departureTime;
			this.network = network;
			this.beelineFactor = beelineFactor;
			this.walkSpeed = walkSpeed;
		}

		@Override
		public AccessibilityRouter create() {
			Config dummyConfig = ConfigUtils.createConfig();
			Scenario dummyScenario = ScenarioUtils.createScenario(dummyConfig);

			// We want minimal travel time!
			dummyConfig.planCalcScore().getModes().get(TransportMode.walk).setMarginalUtilityOfTraveling(-1.0);
			dummyConfig.planCalcScore().getModes().get(TransportMode.walk).setConstant(0.0);
			dummyConfig.planCalcScore().getModes().get(TransportMode.walk).setMarginalUtilityOfDistance(0.0);
			dummyConfig.planCalcScore().getModes().get(TransportMode.walk).setMonetaryDistanceRate(0.0);

			dummyConfig.planCalcScore().getModes().get(TransportMode.pt).setMarginalUtilityOfTraveling(-1.0);
			dummyConfig.planCalcScore().getModes().get(TransportMode.pt).setConstant(0.0);
			dummyConfig.planCalcScore().getModes().get(TransportMode.pt).setMarginalUtilityOfDistance(0.0);
			dummyConfig.planCalcScore().getModes().get(TransportMode.pt).setMonetaryDistanceRate(0.0);

			dummyConfig.planCalcScore().setPerforming_utils_hr(0.0);
			dummyConfig.planCalcScore().setMarginalUtlOfWaiting_utils_hr(-1.0);
			dummyConfig.planCalcScore().setMarginalUtlOfWaitingPt_utils_hr(-1.0);
			dummyConfig.planCalcScore().setUtilityOfLineSwitch(0.0);

			dummyConfig.transitRouter().setAdditionalTransferTime(0.0);
			dummyConfig.plansCalcRoute().getModeRoutingParams().get(TransportMode.walk)
					.setBeelineDistanceFactor(beelineFactor);
			dummyConfig.plansCalcRoute().getModeRoutingParams().get(TransportMode.walk)
					.setTeleportedModeSpeed(walkSpeed);

			RaptorParametersForPerson parametersForPerson = new DefaultRaptorParametersForPerson(dummyConfig);
			RaptorRouteSelector routeSelector = new LeastCostRaptorRouteSelector();
			RaptorIntermodalAccessEgress intermodalAccessEgress = new DefaultRaptorIntermodalAccessEgress();

			SwissRailRaptor raptor = new SwissRailRaptorFactory(schedule, dummyConfig, network, parametersForPerson,
					routeSelector, intermodalAccessEgress, dummyConfig.plans(), dummyScenario.getPopulation(),
					Collections.emptyMap()).get();

			return new PTAccessibilityRouter(raptor, departureTime);
		}

		static class CustomFacility implements Facility {
			private final Coord coord;

			public CustomFacility(Coord coord) {
				this.coord = coord;
			}

			@Override
			public Coord getCoord() {
				return coord;
			}

			@Override
			public Id getId() {
				return null;
			}

			@Override
			public Map<String, Object> getCustomAttributes() {
				return Collections.emptyMap();
			}

			@Override
			public Id getLinkId() {
				return null;
			}
		}
	}
}
