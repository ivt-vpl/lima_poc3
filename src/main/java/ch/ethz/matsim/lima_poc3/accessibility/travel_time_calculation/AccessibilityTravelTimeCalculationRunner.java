package ch.ethz.matsim.lima_poc3.accessibility.travel_time_calculation;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.matsim.lima_poc3.accessibility.routing.AccessibilityRouter;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityProgress;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityTravelTimeMatrix;

public class AccessibilityTravelTimeCalculationRunner implements Runnable {
	private final AccessibilityRouter.Factory routerFactory;
	private final AccessibilityTravelTimeMatrix matrix;
	private final AccessibilityProgress progress;
	private final AccessibilityTravelTimeCalculationTaskGenerator generator;
	private final int chunkSize;

	public AccessibilityTravelTimeCalculationRunner(AccessibilityTravelTimeMatrix matrix,
			AccessibilityRouter.Factory routerFactory, AccessibilityTravelTimeCalculationTaskGenerator generator,
			int chunkSize, AccessibilityProgress progress) {
		this.generator = generator;
		this.chunkSize = chunkSize;
		this.routerFactory = routerFactory;
		this.matrix = matrix;
		this.progress = progress;
	}

	@Override
	public void run() {
		AccessibilityRouter router = routerFactory.create();

		while (true) {
			List<AccessibilityTravelTimeCalculationTask> tasks = new ArrayList<>(chunkSize);

			synchronized (generator) {
				while (generator.hasNext() && tasks.size() < chunkSize) {
					tasks.add(generator.next());
				}
			}

			if (tasks.size() > 0) {
				for (AccessibilityTravelTimeCalculationTask task : tasks) {
					double travelTime = router.computeTravelTime(task.fromZone.coordinate, task.toZone.coordinate);
					matrix.set(task.fromZone.index, task.toZone.index, travelTime);
					progress.update();
				}
			} else {
				return;
			}
		}
	}
}
