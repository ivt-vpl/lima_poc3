package ch.ethz.matsim.lima_poc3.accessibility.utils;

public class AccessibilityProgress implements Runnable {
	private final long interval = 1000;

	private final String description;
	private final int total;

	private int current = 0;

	public AccessibilityProgress(int total, String description) {
		this.total = total;
		this.description = description;
	}

	@Override
	public void run() {
		try {
			int previousCurrent = 0;

			while (current < total) {
				double progress = 100.0 * current / total;

				if (current > previousCurrent) {
					System.out.println(String.format("%s %d/%d (%.2f%%)", description, current, total, progress));
					previousCurrent = current;
				}

				Thread.sleep(interval);
			}
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
	}

	public synchronized void update() {
		current++;
	}
}
