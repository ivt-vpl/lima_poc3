package ch.ethz.matsim.lima_poc3.accessibility.travel_time_estimation;

import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.LinkEnterEvent;
import org.matsim.api.core.v01.events.LinkLeaveEvent;
import org.matsim.api.core.v01.events.VehicleEntersTrafficEvent;
import org.matsim.api.core.v01.events.VehicleLeavesTrafficEvent;
import org.matsim.api.core.v01.events.handler.LinkEnterEventHandler;
import org.matsim.api.core.v01.events.handler.LinkLeaveEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleEntersTrafficEventHandler;
import org.matsim.api.core.v01.events.handler.VehicleLeavesTrafficEventHandler;
import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.Vehicle;

public class AccessibilityTravelTimeListener implements LinkEnterEventHandler, LinkLeaveEventHandler,
		VehicleLeavesTrafficEventHandler, VehicleEntersTrafficEventHandler {
	private final double averagingStartTime;
	private final double averagingEndTime;

	private final Map<Id<Link>, Double> cumulativeTravelTimes = new HashMap<>();
	private final Map<Id<Link>, Double> traversalCounts = new HashMap<>();
	private final Map<Id<Vehicle>, LinkEnterEvent> enterEvents = new HashMap<>();

	public AccessibilityTravelTimeListener(double averagingStartTime, double averagingEndTime) {
		this.averagingStartTime = averagingStartTime;
		this.averagingEndTime = averagingEndTime;
	}

	private boolean isRelevantStartTime(double startTime) {
		return startTime <= averagingEndTime;
	}

	private boolean isRelevantEndTime(double endTime) {
		return endTime >= averagingStartTime;
	}

	@Override
	public void handleEvent(LinkEnterEvent enterEvent) {
		if (isRelevantStartTime(enterEvent.getTime())) {
			enterEvents.put(enterEvent.getVehicleId(), enterEvent);
		}
	}

	@Override
	public void handleEvent(LinkLeaveEvent leaveEvent) {
		if (isRelevantEndTime(leaveEvent.getTime())) {
			LinkEnterEvent enterEvent = enterEvents.remove(leaveEvent.getVehicleId());

			if (enterEvent != null && enterEvent.getLinkId().equals(leaveEvent.getLinkId())) {
				double traversalTime = leaveEvent.getTime() - enterEvent.getTime();
				registerTraversal(enterEvent.getLinkId(), traversalTime);
			}
		}
	}

	@Override
	public void handleEvent(VehicleEntersTrafficEvent event) {
		enterEvents.remove(event.getVehicleId());
	}

	@Override
	public void handleEvent(VehicleLeavesTrafficEvent event) {
		enterEvents.remove(event.getVehicleId());
	}

	private void registerTraversal(Id<Link> linkId, double traversalTime) {
		if (!cumulativeTravelTimes.containsKey(linkId)) {
			cumulativeTravelTimes.put(linkId, traversalTime);
			traversalCounts.put(linkId, 1.0);
		} else {
			cumulativeTravelTimes.put(linkId, cumulativeTravelTimes.get(linkId) + traversalTime);
			traversalCounts.put(linkId, traversalCounts.get(linkId) + 1.0);
		}
	}

	public Map<Id<Link>, Double> getAverageTravelTimes() {
		Map<Id<Link>, Double> averages = new HashMap<>();

		for (Id<Link> linkId : cumulativeTravelTimes.keySet()) {
			double cumulativeTravelTime = cumulativeTravelTimes.get(linkId);
			double traversalCount = traversalCounts.get(linkId);
			averages.put(linkId, cumulativeTravelTime / traversalCount);
		}

		return averages;
	}

}
