package ch.ethz.matsim.lima_poc3.accessibility.travel_time_calculation;

import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZone;

public class AccessibilityTravelTimeCalculationTask {
	public final AccessibilityZone fromZone;
	public final AccessibilityZone toZone;

	public AccessibilityTravelTimeCalculationTask(AccessibilityZone fromZone, AccessibilityZone toZone) {
		this.fromZone = fromZone;
		this.toZone = toZone;
	}
}
