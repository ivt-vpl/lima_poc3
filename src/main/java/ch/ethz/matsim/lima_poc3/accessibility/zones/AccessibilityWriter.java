package ch.ethz.matsim.lima_poc3.accessibility.zones;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AccessibilityWriter {
	private final String identifierAttribute;
	private final List<AccessibilityZone> zones;
	private final Map<String, double[]> accessibilities;

	public AccessibilityWriter(String identifierAttribute, List<AccessibilityZone> zones,
			Map<String, double[]> accessibilities) {
		this.zones = zones;
		this.accessibilities = accessibilities;
		this.identifierAttribute = identifierAttribute;
	}

	public void write(File outputFile) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile)));

		List<String> modes = new ArrayList<>(accessibilities.keySet());

		List<String> header = new LinkedList<>(Arrays.asList(identifierAttribute));
		header.addAll(modes);
		writer.write(String.join(";", header) + "\n");

		for (AccessibilityZone zone : zones) {
			List<String> row = new ArrayList<>(1 + modes.size());
			row.add(zone.identifier);

			for (String mode : modes) {
				row.add(String.valueOf(accessibilities.get(mode)[zone.index]));
			}

			writer.write(String.join(";", row) + "\n");
		}

		writer.close();
	}
}
