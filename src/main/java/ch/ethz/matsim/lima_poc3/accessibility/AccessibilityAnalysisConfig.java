package ch.ethz.matsim.lima_poc3.accessibility;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class AccessibilityAnalysisConfig {
	public double accessibilityBeta = -0.2;

	public double walkBeelineDistanceFactor = 1.05;
	public double walkSpeed = 1.389;

	public boolean carUseFreespeed = false;
	public double carTravelTimeOffset = 300.0;
	public double avWaitingTimeOffset = 300.0;
	public double ptReferenceDepartureTime = 8.0 * 3600.0;

	public double roadTravelTimeAveragingStartTime = 7.5 * 3600.0;
	public double roadTravelTimeAveragingEndTime = 8.5 * 3600.0;
	public double roadFreespeedFactor = 1.0;

	public String eventsPath;
	public String networkPath;
	public String schedulePath;

	public String shapefilePath;
	public String opportunitiesAttribute;
	public String identifierAttribute;

	public String outputPath;

	public int numberOfThreads = -1;
	public int routingChunkSize = 1000;

	static public void main(String[] args) throws JsonGenerationException, JsonMappingException, IOException {
		new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).writeValue(new File(args[0]),
				new AccessibilityAnalysisConfig());
	}
}
