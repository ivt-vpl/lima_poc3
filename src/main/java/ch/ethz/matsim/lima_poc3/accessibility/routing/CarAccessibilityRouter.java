package ch.ethz.matsim.lima_poc3.accessibility.routing;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.router.DijkstraFactory;
import org.matsim.core.router.costcalculators.OnlyTimeDependentTravelDisutility;
import org.matsim.core.router.util.LeastCostPathCalculator;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.TravelDisutility;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.collections.QuadTree;

public class CarAccessibilityRouter implements AccessibilityRouter {
	private final LeastCostPathCalculator router;
	private final QuadTree<Link> quadtree;
	private final WalkAccessibilityRouter walkRouter;
	private final double offsetTravelTime;

	public CarAccessibilityRouter(LeastCostPathCalculator router, QuadTree<Link> quadtree,
			WalkAccessibilityRouter walkRouter, double offsetTravelTime) {
		this.router = router;
		this.quadtree = quadtree;
		this.walkRouter = walkRouter;
		this.offsetTravelTime = offsetTravelTime;
	}

	@Override
	public double computeTravelTime(Coord fromCoord, Coord toCoord) {
		Link startLink = quadtree.getClosest(fromCoord.getX(), fromCoord.getY());
		Link endLink = quadtree.getClosest(toCoord.getX(), toCoord.getY());

		Path path = router.calcLeastCostPath(startLink.getToNode(), endLink.getFromNode(), 0.0, null, null);

		double travelTime = path.travelTime;
		travelTime += walkRouter.computeTravelTime(fromCoord, startLink.getToNode().getCoord());
		travelTime += walkRouter.computeTravelTime(endLink.getFromNode().getCoord(), toCoord);

		travelTime += offsetTravelTime;

		return travelTime;
	}

	public static class Factory implements AccessibilityRouter.Factory {
		private final Network roadNetwork;
		private final TravelTime travelTime;
		private final QuadTree<Link> quadtree;
		private final WalkAccessibilityRouter.Factory walkFactory;
		private final double offsetTravelTime;

		public Factory(Network roadNetwork, TravelTime travelTime, QuadTree<Link> quadtree,
				WalkAccessibilityRouter.Factory walkFactory, double offsetTravelTime) {
			this.roadNetwork = roadNetwork;
			this.travelTime = travelTime;
			this.quadtree = quadtree;
			this.walkFactory = walkFactory;
			this.offsetTravelTime = offsetTravelTime;
		}

		@Override
		public CarAccessibilityRouter create() {
			WalkAccessibilityRouter walkRouter = walkFactory.create();

			TravelDisutility travelDisutility = new OnlyTimeDependentTravelDisutility(travelTime);
			LeastCostPathCalculator router = new DijkstraFactory().createPathCalculator(roadNetwork, travelDisutility,
					travelTime);

			return new CarAccessibilityRouter(router, quadtree, walkRouter, offsetTravelTime);
		}
	}
}
