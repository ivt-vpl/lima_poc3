package ch.ethz.matsim.lima_poc3.accessibility;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.network.algorithms.TransportModeNetworkFilter;
import org.matsim.core.network.io.MatsimNetworkReader;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.core.trafficmonitoring.FreeSpeedTravelTime;
import org.matsim.core.utils.collections.QuadTree;
import org.matsim.pt.transitSchedule.api.TransitSchedule;
import org.matsim.pt.transitSchedule.api.TransitScheduleReader;

import com.fasterxml.jackson.databind.ObjectMapper;

import ch.ethz.matsim.lima_poc3.accessibility.computation.AccessibilityComputationGenerator;
import ch.ethz.matsim.lima_poc3.accessibility.computation.AccessibilityComputationRunner;
import ch.ethz.matsim.lima_poc3.accessibility.routing.AVAccessibilityRouter;
import ch.ethz.matsim.lima_poc3.accessibility.routing.AccessibilityRouter;
import ch.ethz.matsim.lima_poc3.accessibility.routing.CarAccessibilityRouter;
import ch.ethz.matsim.lima_poc3.accessibility.routing.PTAccessibilityRouter;
import ch.ethz.matsim.lima_poc3.accessibility.routing.WalkAccessibilityRouter;
import ch.ethz.matsim.lima_poc3.accessibility.travel_time_calculation.AccessibilityTravelTimeCalculationRunner;
import ch.ethz.matsim.lima_poc3.accessibility.travel_time_calculation.AccessibilityTravelTimeCalculationTaskGenerator;
import ch.ethz.matsim.lima_poc3.accessibility.travel_time_estimation.AccessibilityTravelTimeFactory;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityFunction;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityProgress;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityTravelTimeMatrix;
import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityWriter;
import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZone;
import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZoneFactory;

public class AccessibilityAnalysis {
	static public void main(String[] args) throws InterruptedException, MalformedURLException, IOException {
		AccessibilityAnalysisConfig config = new ObjectMapper().readValue(new File(args[0]),
				AccessibilityAnalysisConfig.class);

		// Define accessibility function

		AccessibilityFunction accessibilityFunction = (o, d, t) -> d.opportunities
				* Math.exp(config.accessibilityBeta * t / 60.0);
		Function<List<Double>, Double> aggregationFunction = e -> Math.log(e.stream().mapToDouble(d -> d).sum());

		// Additional setup

		int numberOfThreads = config.numberOfThreads == -1 ? Runtime.getRuntime().availableProcessors()
				: config.numberOfThreads;

		// I) Read in MATSim information

		Network network = NetworkUtils.createNetwork();
		new MatsimNetworkReader(network).readFile(config.networkPath);

		Scenario scheduleScenario = ScenarioUtils.createScenario(ConfigUtils.createConfig());
		new TransitScheduleReader(scheduleScenario).readFile(config.schedulePath);
		TransitSchedule schedule = scheduleScenario.getTransitSchedule();

		// II) Read in zone information

		AccessibilityZoneFactory zoneFactory = new AccessibilityZoneFactory(config.identifierAttribute,
				config.opportunitiesAttribute);
		List<AccessibilityZone> zones = zoneFactory.createZones(new File(config.shapefilePath));
		int numberOfZones = zones.size();

		// III) Estimate travel times

		TravelTime travelTime = new FreeSpeedTravelTime();

		if (!config.carUseFreespeed) {
			AccessibilityTravelTimeFactory travelTimeFactory = new AccessibilityTravelTimeFactory(
					config.roadTravelTimeAveragingStartTime, config.roadTravelTimeAveragingEndTime,
					config.roadFreespeedFactor);
			travelTime = travelTimeFactory.create(config.eventsPath);
		}

		// IV) Prepare road network and index

		Network roadNetwork = NetworkUtils.createNetwork();
		new TransportModeNetworkFilter(network).filter(roadNetwork, Collections.singleton("car"));

		double[] bounds = NetworkUtils.getBoundingBox(roadNetwork.getNodes().values());
		QuadTree<Link> roadQuadTree = new QuadTree<>(bounds[0], bounds[1], bounds[2], bounds[3]);
		roadNetwork.getLinks().values().forEach(l -> roadQuadTree.put(l.getCoord().getX(), l.getCoord().getY(), l));

		// V) Create router factories

		WalkAccessibilityRouter.Factory walkFactory = new WalkAccessibilityRouter.Factory(
				config.walkBeelineDistanceFactor, config.walkSpeed);
		CarAccessibilityRouter.Factory carFactory = new CarAccessibilityRouter.Factory(roadNetwork, travelTime,
				roadQuadTree, walkFactory, config.carTravelTimeOffset);
		AVAccessibilityRouter.Factory avFactory = new AVAccessibilityRouter.Factory(carFactory,
				config.avWaitingTimeOffset);
		PTAccessibilityRouter.Factory ptFactory = new PTAccessibilityRouter.Factory(schedule,
				config.ptReferenceDepartureTime, network, config.walkBeelineDistanceFactor, config.walkSpeed);

		Map<String, AccessibilityRouter.Factory> routerFactories = new HashMap<>();
		routerFactories.put("car", carFactory);
		routerFactories.put("av", avFactory);
		routerFactories.put("pt", ptFactory);

		// VI) Compute travel time matrices (in parallel)

		Map<String, AccessibilityTravelTimeMatrix> matrices = new HashMap<>();
		routerFactories.keySet().forEach(mode -> matrices.put(mode, new AccessibilityTravelTimeMatrix(numberOfZones)));

		for (String mode : routerFactories.keySet()) {
			AccessibilityProgress progress = new AccessibilityProgress(numberOfZones * numberOfZones,
					String.format("Calculating travel time (%s)", mode));

			AccessibilityRouter.Factory routerFactory = routerFactories.get(mode);
			AccessibilityTravelTimeCalculationTaskGenerator generator = new AccessibilityTravelTimeCalculationTaskGenerator(
					zones);

			AccessibilityTravelTimeMatrix matrix = new AccessibilityTravelTimeMatrix(numberOfZones);
			matrices.put(mode, matrix);

			Thread[] threads = new Thread[numberOfThreads];

			for (int i = 0; i < numberOfThreads; i++) {
				threads[i] = new Thread(new AccessibilityTravelTimeCalculationRunner(matrix, routerFactory, generator,
						config.routingChunkSize, progress));
				threads[i].start();
			}

			Thread progressThread = new Thread(progress);
			progressThread.start();

			for (int i = 0; i < numberOfThreads; i++) {
				threads[i].join();
			}

			progressThread.join();
		}

		// VII) Compute accessibility

		Map<String, double[]> accessibilities = new HashMap<>();

		for (String mode : routerFactories.keySet()) {
			AccessibilityProgress progress = new AccessibilityProgress(numberOfZones,
					String.format("Calculating accessibility (%s)", mode));
			AccessibilityComputationGenerator generator = new AccessibilityComputationGenerator(zones);
			AccessibilityTravelTimeMatrix matrix = matrices.get(mode);

			double accessibility[] = new double[numberOfZones];
			accessibilities.put(mode, accessibility);

			Thread[] threads = new Thread[numberOfThreads];

			for (int i = 0; i < numberOfThreads; i++) {
				threads[i] = new Thread(new AccessibilityComputationRunner(accessibility, zones, accessibilityFunction,
						aggregationFunction, matrix, generator, progress));
				threads[i].start();
			}

			Thread progressThread = new Thread(progress);
			progressThread.start();

			for (int i = 0; i < numberOfThreads; i++) {
				threads[i].join();
			}

			progressThread.join();
		}

		// VIII) Write out information

		new AccessibilityWriter(config.identifierAttribute, zones, accessibilities).write(new File(config.outputPath));
	}
}
