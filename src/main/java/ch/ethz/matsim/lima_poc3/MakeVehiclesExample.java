package ch.ethz.matsim.lima_poc3;

import java.util.Random;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigUtils;
import org.matsim.core.population.io.PopulationReader;
import org.matsim.core.scenario.ScenarioUtils;
import org.matsim.vehicles.Vehicle;
import org.matsim.vehicles.VehicleCapacity;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleWriterV1;
import org.matsim.vehicles.Vehicles;

public class MakeVehiclesExample {
	static public void main(String[] args) {
		String populationPath = args[0];
		String vehiclesPath = args[1];
		double automatedProbability = Double.parseDouble(args[2]);

		Config config = ConfigUtils.createConfig();
		Scenario scenario = ScenarioUtils.createScenario(config);
		new PopulationReader(scenario).readFile(populationPath);

		Vehicles vehicles = scenario.getVehicles();

		// Create vehicle types

		// Conventional
		Id<VehicleType> conventionalTypeId = Id.create("conventional", VehicleType.class);
		VehicleType conventionalType = vehicles.getFactory().createVehicleType(conventionalTypeId);
		vehicles.addVehicleType(conventionalType);

		VehicleCapacity conventionalCapacity = vehicles.getFactory().createVehicleCapacity();
		conventionalCapacity.setSeats(4);
		conventionalType.setCapacity(conventionalCapacity);

		// Automated
		Id<VehicleType> automatedTypeId = Id.create("automated", VehicleType.class);
		VehicleType automatedType = vehicles.getFactory().createVehicleType(automatedTypeId);
		vehicles.addVehicleType(automatedType);

		VehicleCapacity automatedCapacity = vehicles.getFactory().createVehicleCapacity();
		automatedCapacity.setSeats(4);
		automatedType.setCapacity(automatedCapacity);

		// Add vehicles

		Random random = new Random(0);

		for (Person person : scenario.getPopulation().getPersons().values()) {
			VehicleType type = conventionalType;

			if (random.nextDouble() < automatedProbability) {
				type = automatedType;
			}

			Vehicle vehicle = vehicles.getFactory().createVehicle(Id.createVehicleId(person.getId()), type);
			vehicles.addVehicle(vehicle);
		}

		new VehicleWriterV1(vehicles).writeFile(vehiclesPath);
	}
}
